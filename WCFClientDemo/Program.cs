﻿using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFClientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Instance.IsWrite2Console = true;
            Log.Instance.IsWrite2File = true;
            Log.Instance.IsWrite2Cloud = false;


            ConsoleKeyInfo cki;
            Console.TreatControlCAsInput = true;

            Console.WriteLine("Please choose which server to connect:");
            Console.WriteLine("*****************************************");
            Console.WriteLine("1. Simio DEV");
            Console.WriteLine("2. Simio QA");
            Console.WriteLine("3. Local");
            Console.WriteLine("4. Enter IP");

            WCFClient.CommunicationManager cm = null;

            do
            {
                cki = Console.ReadKey(true);
                //Console.Write(" --- You pressed ");
                if ((cki.Modifiers & ConsoleModifiers.Alt) != 0)
                    Console.Write("ALT+");
                if ((cki.Modifiers & ConsoleModifiers.Shift) != 0)
                    Console.Write("SHIFT+");
                if ((cki.Modifiers & ConsoleModifiers.Control) != 0)
                    Console.Write("CTL+");
                //Console.WriteLine(cki.Key.ToString());
                switch (cki.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        cm = new WCFClient.CommunicationManager("simio.cloudapp.net:8080/WCFCommunication/WCFCommunication");
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        cm = new WCFClient.CommunicationManager("simio-qa.cloudapp.net:80/WCFCommunication/WCFCommunication");
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        cm = new WeboomerangClient.CommunicationManager("127.0.0.1:8080/WCFCommunication/WCFCommunication");
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        string ip = Console.ReadLine();
                        ip = ip + ":8080/WCFCommunication/WCFCommunication";
                        cm = new WeboomerangClient.CommunicationManager(ip);
                        break;
                    
                    
                }
            } while (cm==null);

           // WCFClient.CommunicationManager cm = new WCFClient.CommunicationManager("simio-qa.cloudapp.net:80/WCFCommunication/WCFCommunication");
           // WCFClient.CommunicationManager cm = new WCFClient.CommunicationManager("webaccess-dev.cloudapp.net:80/WCFCommunication/WCFCommunication");
           // WeboomerangClient.CommunicationManager cm = new WeboomerangClient.CommunicationManager("10.10.10.140:8080/WCFCommunication/Weboomerang");
            if(cm!=null)
                cm.Start();
            Console.ReadLine();
        }
    }
}
