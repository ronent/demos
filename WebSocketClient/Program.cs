﻿using SocketClient.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ClientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            DeviceManager manager = new DeviceManager();
            manager.Start();
            manager.OnMessageLogged += manager_OnMessageLogged;

            Console.ReadLine();
        }

        static void manager_OnMessageLogged(object sender, string message)
        {
            Console.WriteLine(message);
        }
    }
}
