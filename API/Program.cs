﻿using API_UI.Forms;
using Base.Devices;
using Base.Devices.Management;
using System;
using System.Windows.Forms;

namespace API_UI
{
    public enum eAPIRunMode
    {
        Form,
        Console,
    }

    static class Program
    {
        #region Properties
        public static eAPIRunMode RunMode
        {
            get { return eAPIRunMode.Form; }
        }

        public static bool UserMode
        {
            get { return Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User; }
        }

        #endregion
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
          //  Log4Tech.Log.Instance.IsWrite2Console = true;

            switch (RunMode)
            {
                case eAPIRunMode.Form:
                    runForm();
                    break;
                case eAPIRunMode.Console:
                    runConsole();
                    break;
                default:
                    break;
            }
        }

        #region Console
        private static void runConsole()
        {
            SuperDeviceManager.OnDeviceConnected += SuperDeviceManager_OnDeviceConnected;
            SuperDeviceManager.OnDeviceRemoved += SuperDeviceManager_OnDeviceRemoved;
 
            SuperDeviceManager.Start();

            Console.ReadKey();
        }

        private static void SuperDeviceManager_OnDeviceConnected(object sender, Base.Devices.Management.EventsAndExceptions.ConnectionEventArgs<Base.Devices.GenericDevice> e)
        {
            Console.WriteLine(string.Format("{0} :{1}, #{2} Devices Connected", e.State, e.Device, SuperDeviceManager.DevicesCount));
            if (e.Device is GenericLogger)
            {
                GenericLogger logger = (e.Device as GenericLogger);
                logger.Functions.OnDownloadCompleted += Functions_OnDownloadCompleted;
                logger.Functions.Download();
            }
        }

        static void Functions_OnDownloadCompleted(object sender, Base.OpCodes.Helpers.DownloadCompletedArgs args)
        {
            Console.WriteLine("Finish Download");
        }

        private static void SuperDeviceManager_OnDeviceRemoved(object sender, Base.Devices.Management.EventsAndExceptions.ConnectionEventArgs<Base.Devices.GenericDevice> e)
        {
            Console.WriteLine(string.Format("{0} :{1}", e.State, e.Device));
        }

        #endregion
        #region Form
        private static void runForm()
        {
            Log4Tech.Log.Instance.IsWrite2Console = true;
            Log4Tech.Log.Instance.IsWrite2File = true;
            Log4Tech.Log.Instance.IsWrite2Cloud = false;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        #endregion
    }
}
