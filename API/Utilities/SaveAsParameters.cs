﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_UI.Utilities
{
    public class SaveAsParameters
    {
        public string DefaultExt { get; private set; }
        public string Filter { get; private set; }

        public SaveAsParameters(string defaultExt, string filter)
        {
            this.DefaultExt = defaultExt;
            this.Filter = filter;
        }
    }
}
