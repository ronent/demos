﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace API_UI.Utilities
{
    public static class ExtensionMethods
    {
        public static void DoubleBuffered(this DataGridView dgv, bool setting)
        {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }

        public static void AppendText(this RichTextBox box, string text, Color color, bool AddNewLine = true)
        {
            if (AddNewLine)
                text += Environment.NewLine;

            try
            {
                box.SelectionStart = box.TextLength;
            }
            catch
            {
                box.SelectionStart = 0;
            }

            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
