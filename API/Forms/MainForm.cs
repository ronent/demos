﻿using API_UI.Forms.UserControls;
using API_UI.Forms.UserControls.Devices;
using API_UI.Forms.UserControls.Devices.MicroXDevices;
using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace API_UI.Forms
{
    public partial class MainForm : Form
    {
        #region Fields
        private static LogForm log = new LogForm();
        private static string title = "The Best Device API V" + Assembly.GetExecutingAssembly().GetName().Version;

        #endregion
        #region Constructor
        public MainForm()
        {
            InitializeComponent();
            Initialize();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (Base.Settings.Settings.Current.ShowLog)
                log.Show();

            autoOpenDeviceIOToolStripMenuItem.Checked = Properties.Settings.Default.AutoShowDeviceIO;
        }

        private void startAutoTimeDownload()
        {
            Timer timer = new Timer();
            timer.Tick += (sender, e) =>
                {
                    multiDownloadToolStripMenuItem_Click(null, EventArgs.Empty);
                };
            timer.Interval = 10000;
            timer.Start();
        }

        protected override void OnShown(EventArgs e)
        {
            SuperDeviceManager.Start();

            base.OnShown(e);

            //startAutoTimeDownload();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (log != null)
                log.Close();

            SuperDeviceManager.Shutdown();
        }

        private void Initialize()
        {
            updateLabel();

            SuperDeviceManager.OnDeviceConnected += SuperDeviceManager_OnDeviceConnected;
            SuperDeviceManager.OnDeviceRemoved += SuperDeviceManager_OnDeviceRemoved;
        }

        #endregion
        #region Events
        void SuperDeviceManager_OnDeviceRemoved(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            removeTab(e.Device);
            updateLabel();
        }

        void SuperDeviceManager_OnDeviceConnected(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            addNewTab(e.Device);
            updateLabel();
            
        }

        private void updateLabel()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { updateLabel(); }));
            }
            else
            {
                if (SuperDeviceManager.Started)
                    this.Text = string.Format("{0} ({1}\\{2} Devices Connected)", title, SuperDeviceManager.OnlineDevicesCount, SuperDeviceManager.DevicesCount);
            }
        }

        #endregion
        #region Device Connections
        private void addNewTab(GenericDevice device)
        {
            if (tabControl1.InvokeRequired)
            {
                tabControl1.BeginInvoke(new MethodInvoker(delegate() { addNewTab(device); }));
            }
            else
            {
                try
                {
                    var tp = new TabPage();
                    UserControl ucl;
                    if(device is SiMiAPI.Devices.SiMiLogger)
                        ucl = FactoryV2.GetDeviceControl(device);
                    else
                        ucl = Factory.GetDeviceControl(device);
                    
                    ucl.Dock = DockStyle.Fill;
                    tp.Controls.Add(ucl);
                    tp.Text = string.Format("{0} : {1}", device.DeviceTypeName, device.Status.SerialNumber);

                    var debug = device.Status.GetMember("TestMode");
                    if (debug != null)
                        if ((bool)debug)
                            tp.Text += " (debug)";

                    tabControl1.TabPages.Add(tp);
                    //tabControl1.SelectedTab = tp;

                    tabControl1.Visible = true;
                    //foreach (TabPage tab in tabControl1.TabPages)
                    //{
                    //    foreach (var userControl in tab.Controls)
                    //    {
                    //        if (userControl is UserControlLogger)
                    //            (userControl as UserControlLogger).Download();
                    //    }
                    //}
                }
                catch (InvalidOperationException ioe)
                {
                    MessageBox.Show("On addNewTab()\n" + ioe);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("New unsupported device connected: " + device + ex);
                }
            }
        }

        private void removeTab(GenericDevice device)
        {
            if (tabControl1.InvokeRequired)
            {
                tabControl1.BeginInvoke(new MethodInvoker(delegate() { removeTab(device); }));
            }
            else
            {
                foreach (TabPage tab in tabControl1.TabPages)
                {
                    try
                    {
                        foreach (UserControl item in tab.Controls)
                        {

                            if (device is SiMiAPI.Devices.SiMiLogger)
                            {
                                var temp = (SiMiUserControlDevice) item;
                                if (temp.Device.Equals(device))
                                {
                                    tabControl1.TabPages.Remove(tab);
                                    try
                                    {
                                        temp.Close();
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            else
                            {
                                var temp = (UserControlDevice) item;
                                if (temp.Device.Equals(device))
                                {
                                    tabControl1.TabPages.Remove(tab);
                                    try
                                    {
                                        temp.Close();
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            
                        }
                    }
                    catch
                    {
                    }
                }

                if (tabControl1.TabCount == 0)
                    tabControl1.Visible = false;
            }
        }

        #endregion
        #region MenuStrip
        private void multiDownloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TabPage tab in tabControl1.TabPages)
            {
                foreach (var userControl in tab.Controls)
                {
                    if (userControl is UserControlLogger)
                        (userControl as UserControlLogger).Download();
                }
            }
        }

        private void openLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!log.Visible)
                log.Show();
        }

        private void autoOpenDeviceIOToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            Properties.Settings.Default.AutoShowDeviceIO = item.Checked;
            Properties.Settings.Default.Save();
        }

        #endregion
    }
}