﻿using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Types;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace API_UI.Forms
{
    public partial class DefineSensorForm : Form
    {
        #region Members
        private UserDefinedSensor m_Sensor;
        private List<GenericSensor> m_Sensors;

        #endregion       
        #region Constructor
        public DefineSensorForm(GenericSensor i_Sensor, List<GenericSensor> i_Sensors)
            :this(i_Sensors)
        {
            m_Sensor = i_Sensor as UserDefinedSensor;
            initialize();
        }

        public DefineSensorForm(List<GenericSensor> i_Sensors)
        {
            InitializeComponent();

            m_Sensors = i_Sensors;

            foreach (var item in m_Sensors)
            {
                comboBoxBaseSensor.Items.Add(item.Type);
            }

            foreach (var item in Enum.GetNames(typeof(ExternalSensorUnitEnum)))
            {
                if (item != ExternalSensorUnitEnum.Custom.ToString())
                    comboBoxUnit.Items.Add(item);                    
            }

            for (int i = 0; i < 7; i++)
            {
                comboBoxDecimalDigits.Items.Add(i);
            }
        }

        private void initialize()
        {
            if (m_Sensor != null)
            {
                SensorName = m_Sensor.Name;

                if (m_Sensor.Unit is UserDefinedUnit)
                {
                    if (((m_Sensor as UserDefinedSensor).Unit as UserDefinedUnit).Type == ExternalSensorUnitEnum.Custom)
                        Unit = ((m_Sensor as UserDefinedSensor).Unit as UserDefinedUnit).Name;
                    else
                        Unit = (m_Sensor.Unit as UserDefinedUnit).Type.ToString();
                }

                BaseSensorType = m_Sensor.BaseType;
                DecimalDigits = m_Sensor.SignificantFigures;
                
                var references = m_Sensor.UDS.References;
                Ref1 = references[0].Reference;
                Ref2 = references[1].Reference;
            }
        }

        #endregion
        #region Properties
        public MicroLogUDSConfiguration Configuration { get; private set; }

        private byte DecimalDigits 
        {
            get { return Convert.ToByte(comboBoxDecimalDigits.SelectedIndex); }
            set { comboBoxDecimalDigits.SelectedIndex = Convert.ToInt16(value); } 
        }

        private string SensorName
        {
            get { return textBoxName.Text; }
            set { textBoxName.Text = value; }
        }

        private string Unit
        {
            get 
            {
                if (comboBoxUnit.SelectedItem != null)
                    return comboBoxUnit.SelectedItem.ToString();                     
                else
                    return comboBoxUnit.Text;
            }
            set { comboBoxUnit.Text = value; }
        }

        private eSensorType BaseSensorType
        {
            get
            {
                string[] names = Enum.GetNames(typeof(eSensorType));

                return (eSensorType)Enum.Parse(typeof(eSensorType), comboBoxBaseSensor.SelectedItem.ToString());
            }
            set
            {
                if (this.InvokeRequired)
                    this.BeginInvoke(new MethodInvoker(delegate() { baseSensorType(value); }));
                else
                    baseSensorType(value);
            }
        }

        private void baseSensorType(eSensorType value)
        {
            comboBoxBaseSensor.SelectedItem = value;
        }

        private decimal Ref1
        {
            get { return Convert.ToDecimal(textBoxRef1.Text); }
            set { textBoxRef1.Text = value.ToString(); }
        }

        public decimal Logger1
        {
            get { return Convert.ToDecimal(textBoxLogger1.Text); }
            set { textBoxLogger1.Text = value.ToString(); }
        }

        private decimal Ref2
        {
            get { return Convert.ToDecimal(textBoxRef2.Text); }
            set { textBoxRef2.Text = value.ToString(); }
        }

        private decimal Logger2
        {
            get { return Convert.ToDecimal(textBoxLogger2.Text); }
            set { textBoxLogger2.Text = value.ToString(); }
        }

        #endregion
        #region Events
        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (checkValidation())
            {
                Configuration = new MicroLogUDSConfiguration();
                LoggerReference[] pairs = 
                { 
                    new LoggerReference { Logger = Logger1, Reference = Ref1},
                    new LoggerReference { Logger = Logger2, Reference = Ref2},
                };
               
                Configuration.Set(pairs);

                Configuration.BaseType = BaseSensorType;
                Configuration.Name = SensorName;
                Configuration.SignificantFigures = DecimalDigits;
                Configuration.Unit = getUnit();

                if (Configuration.Unit == ExternalSensorUnitEnum.Custom)
                {
                    Configuration.CustomUnit = CustomUnit;
                }

                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            disableChanges();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void comboBoxBaseSensor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Logger1 = Convert.ToDecimal(m_Sensors[(sender as ComboBox).SelectedIndex].Minimum);
            Logger2 = Convert.ToDecimal(m_Sensors[(sender as ComboBox).SelectedIndex].Maximum);
        }
        #endregion
        #region Methods
        private string CustomUnit
        {
            get { return comboBoxUnit.Text; }
        }

        private ExternalSensorUnitEnum getUnit()
        {
            try
            {
                ExternalSensorUnitEnum enumParse = (ExternalSensorUnitEnum)Enum.Parse(typeof(ExternalSensorUnitEnum), Unit);

                if (Enum.IsDefined(typeof(ExternalSensorUnitEnum), enumParse))
                    return enumParse;
            }
            catch { }

            return ExternalSensorUnitEnum.Custom;
        }

        private void disableChanges()
        {
            if (Configuration != null)
            {
                BaseSensorType = Configuration.BaseType;
                SensorName = Configuration.Name;
                DecimalDigits = Configuration.SignificantFigures;
                Unit = GetUnitString(Configuration.Unit, Configuration.CustomUnit);
            }
        }

        public static string GetUnitString(ExternalSensorUnitEnum m_ExternalSensor, string m_CustomString)
        {
            string toReturn;

            if (m_ExternalSensor == ExternalSensorUnitEnum.Custom)
                toReturn = m_CustomString;
            else
                toReturn = m_ExternalSensor.ToString();

            return toReturn;
        }

        private bool checkValidation()
        {
            return true;
        }

        #endregion
    }
}
