﻿using MicroLogAPI.Devices;

namespace API_UI.Forms.UserControls.Setup
{
    public partial class MicroLogSetup : Middle
    {
        public MicroLogSetup(GenericMicroLogLogger device)
            : base(device)
        {

        }

        public MicroLogSetup()
        {
            InitializeComponent();
        }

        protected override void Initialize()
        {
            numericUpDownAveragingPoints.Minimum = 1;
            numericUpDownAveragingPoints.Maximum = (Logger as GenericMicroLogLogger).AllowedAveragePoints + 1;
        }
    }
}
