﻿using Base.Devices.Management;
using Base.Sensors.Management;
using PicoLite.Devices;
using System.Windows.Forms;

namespace API_UI.Forms.UserControls.Setup.PicoLite
{
    public partial class PicoLiteV1Setup : Middle
    {
        public PicoLiteV1Setup(PicoLiteLogger i_Logger)
            :base(i_Logger)
        {
            InitializeComponent();
        }

        public PicoLiteV1Setup()
            : base()
        {

        }

        protected override void Initialize()
        {
            GetUCFixedSensor(eSensorIndex.Temperature).Checkable = false;

            if (!Logger.Status.TestMode)
                labelInterval.Text = "[minutes]";

            labelAveragingPointsType.Visible = false;
            labelAverangingPoints.Visible = false;
            numericUpDownAveragingPoints.Visible = false;

            foreach (var item in tabControlSetup.TabPages)
            {
                if ((item as TabPage).Text == "Display")
                    tabControlSetup.TabPages.Remove(item as TabPage);
            }
        }

        protected override void loadProperties(MicroXBase.DataStructures.MicroXStatusConfiguration i_Status)
        {
            base.loadProperties(i_Status);

            if (!Logger.Status.TestMode)
                Interval = (ushort)i_Status.Interval.TotalMinutes;
        }
    }
}
