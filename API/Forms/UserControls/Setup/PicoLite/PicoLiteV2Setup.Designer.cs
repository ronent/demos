﻿namespace API_UI.Forms.UserControls.Setup.PicoLite
{
    partial class PicoLiteV2Setup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PTRDelay = new System.Windows.Forms.TextBox();
            this.Info = new System.Windows.Forms.TabPage();
            this.dateTimePickerRun = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSetup = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAveragingPoints)).BeginInit();
            this.tabControlSetup.SuspendLayout();
            this.tabPageProperties.SuspendLayout();
            this.Info.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlSetup
            // 
            this.tabControlSetup.Controls.Add(this.Info);
            this.tabControlSetup.Controls.SetChildIndex(this.Info, 0);
            this.tabControlSetup.Controls.SetChildIndex(this.tabPageProperties, 0);
            // 
            // tabPageProperties
            // 
            this.tabPageProperties.Controls.Add(this.PTRDelay);
            this.tabPageProperties.Controls.SetChildIndex(this.checkBoxDeepSleepMode, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.labelInterval, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.checkBoxStopOnSensorDisconnects, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.labelAverangingPoints, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.numericUpDownAveragingPoints, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.labelAveragingPointsType, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.cbDebug, 0);
            this.tabPageProperties.Controls.SetChildIndex(this.PTRDelay, 0);
            // 
            // checkBoxDeepSleepMode
            // 
            this.checkBoxDeepSleepMode.Visible = false;
            // 
            // PTRDelay
            // 
            this.PTRDelay.Location = new System.Drawing.Point(101, 84);
            this.PTRDelay.Name = "PTRDelay";
            this.PTRDelay.Size = new System.Drawing.Size(35, 20);
            this.PTRDelay.TabIndex = 46;
            // 
            // Info
            // 
            this.Info.Controls.Add(this.dateTimePickerRun);
            this.Info.Controls.Add(this.dateTimePickerSetup);
            this.Info.Controls.Add(this.label5);
            this.Info.Controls.Add(this.label2);
            this.Info.Location = new System.Drawing.Point(4, 22);
            this.Info.Name = "Info";
            this.Info.Padding = new System.Windows.Forms.Padding(3);
            this.Info.Size = new System.Drawing.Size(305, 315);
            this.Info.TabIndex = 4;
            this.Info.Text = "More Info";
            this.Info.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerRun
            // 
            this.dateTimePickerRun.CustomFormat = "HH:mm:ss dd/MM/yy";
            this.dateTimePickerRun.Enabled = false;
            this.dateTimePickerRun.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerRun.Location = new System.Drawing.Point(94, 58);
            this.dateTimePickerRun.Name = "dateTimePickerRun";
            this.dateTimePickerRun.Size = new System.Drawing.Size(137, 20);
            this.dateTimePickerRun.TabIndex = 3;
            // 
            // dateTimePickerSetup
            // 
            this.dateTimePickerSetup.CustomFormat = "HH:mm:ss dd/MM/yy";
            this.dateTimePickerSetup.Enabled = false;
            this.dateTimePickerSetup.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSetup.Location = new System.Drawing.Point(94, 19);
            this.dateTimePickerSetup.Name = "dateTimePickerSetup";
            this.dateTimePickerSetup.Size = new System.Drawing.Size(137, 20);
            this.dateTimePickerSetup.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Run Time: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Setup Time: ";
            // 
            // PicoLiteV2Setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "PicoLiteV2Setup";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAveragingPoints)).EndInit();
            this.tabControlSetup.ResumeLayout(false);
            this.tabPageProperties.ResumeLayout(false);
            this.tabPageProperties.PerformLayout();
            this.Info.ResumeLayout(false);
            this.Info.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox PTRDelay;
        private System.Windows.Forms.TabPage Info;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerRun;
        private System.Windows.Forms.DateTimePicker dateTimePickerSetup;
    }
}
