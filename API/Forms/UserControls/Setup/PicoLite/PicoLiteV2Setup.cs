﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PicoLite.Devices;
using PicoLite.DataStructures.V2;

namespace API_UI.Forms.UserControls.Setup.PicoLite
{
    public partial class PicoLiteV2Setup : PicoLiteV1Setup
    {
        public PicoLiteV2Setup(PicoLiteLogger i_Logger)
            :base(i_Logger)
        {
            InitializeComponent();
        }

        public PicoLiteV2Setup()
            : base()
        {

        }

        protected override void loadProperties(MicroXBase.DataStructures.MicroXStatusConfiguration i_Status)
        {
            PushToRunDelay = (i_Status as PicoLiteV2StatusConfiguration).RunDelay;

            base.loadProperties(i_Status);
        }

        protected override void loadStatusConfiguration(MicroXBase.DataStructures.MicroXStatusConfiguration i_Status)
        {
            loadMoreInfo(i_Status as PicoLiteV2StatusConfiguration);

            base.loadStatusConfiguration(i_Status);
        }

        private void loadMoreInfo(PicoLiteV2StatusConfiguration status)
        {
            SetupTime = status.SetupTime;
            RunTime = status.RunTime;
            StopOnKeyPress = status.StopOnKeyPress;
        }

        #region Properties
        public UInt16 PushToRunDelay
        {
            get 
            {
                UInt16 number;
                UInt16.TryParse(PTRDelay.Text, out number);

                return number;
            }
            set
            {
                PTRDelay.Text = value.ToString();
            }
        }

        public DateTime SetupTime
        {
            get { return dateTimePickerSetup.Value; }
            set 
            {
                try
                {
                    dateTimePickerSetup.Value = value;
                }
                catch
                {
                    dateTimePickerSetup.Value = DateTimePicker.MinimumDateTime;
                }
            }
        }

        public DateTime RunTime
        {
            get { return dateTimePickerRun.Value; }
            set
            {
                try
                {
                    dateTimePickerRun.Value = value;
                }
                catch
                {
                    dateTimePickerRun.Value = DateTimePicker.MinimumDateTime;
                }
            }
        }

        #endregion
    }
}
