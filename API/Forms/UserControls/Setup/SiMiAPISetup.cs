﻿using MicroLogAPI.Devices;
using SiMiAPI.Devices;

namespace API_UI.Forms.UserControls.Setup
{
    public partial class SiMiAPISetup : SiMiMiddle
    {
        public SiMiAPISetup(SiMiLogger device)
            : base(device)
        {

        }

        public SiMiAPISetup()
        {
            InitializeComponent();
        }

        protected override void Initialize()
        {
            numericUpDownAveragingPoints.Minimum = 1;
            //numericUpDownAveragingPoints.Maximum = (Logger as GenericSiMiLogLogger).AllowedAveragePoints + 1;
        }
    }
}
