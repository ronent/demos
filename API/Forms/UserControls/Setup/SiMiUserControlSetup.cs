﻿using API_UI.Forms.UserControls.Sensors;
using Auxiliary.MathLib;
using Auxiliary.Tools;
using Base.Devices.Management;
using Base.Misc;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Base.Sensors.Types.Temperature;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using PicoLite.DataStructures.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SiMiAPI.DataStructures;
using SiMiAPI.DataStructures.ESetup;
using SiMiAPI.Devices;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Features;
using SiMiBase.Devices.Types;
using LCDConfigurationEnum = MicroLogAPI.DataStructures.LCDConfigurationEnum;

namespace API_UI.Forms.UserControls.Setup
{
    public abstract partial class SiMiUserControlSetup : UserControl
    {
        #region Members and Consts
        public SiMiLogger Logger { get; set; }
        private int m_BoomerangEmailItemToDelete;

        private static string[] k_Display = new string[] 
        { 
            "Always On", 
            "On for 30s on key press, alarm", 
            "On for 60s on key press, alarm", 
            "On for 120s on key press, alarm" 
        };

        #endregion
        #region Constructor

        protected SiMiUserControlSetup(SiMiLogger i_Logger)
        {
            InitializeComponent();

            Logger = i_Logger;
            initialize();
        }

        protected SiMiUserControlSetup()
        {
            InitializeComponent();
        }

        protected abstract void Initialize();

        private void initialize()
        {
            setUserMode();

            comboBoxDisplay.Items.AddRange(k_Display);
            comboBoxDisplay.SelectedIndex = 0;
            numericUpDownInterval.Minimum = 1;
            labelFWVersion.Text = Logger.Version.Firmware.ToString();
            labelBatteryLevel.Text = Logger.Battery.BatteryLevel.ToString() + " %";

            textBoxSN.MaxLength = MicroXSetupConfiguration.AllowedSNLength;
            textBoxComment.MaxLength = MicroXSetupConfiguration.AllowedCommentLength;

            checkBoxBoomerang_CheckedChanged(checkBoxBoomerang, EventArgs.Empty);

            initializeSensors();
            initializeFeatures();

            Initialize();
        }

        /// <summary>
        /// Disables non user features
        /// </summary>
        private void setUserMode()
        {
            if (Program.UserMode)
            {
                comboBoxMemorySize.Enabled = false;
                textBoxSN.Enabled = false;
            }
        }

        private void initializeFeatures()
        {
            enableAlarmDelay(false);
            enableCyclic(false);
            enablePushToRun(false);
            enableTimerRun(false);
            enableStopOnKeyPress(false);
            enableStopOnDisconnect(false);
            enableTestMode(false);
            enableMemorySize(false);
            enableShowMinMax(false);
            enableDeepSleep(false);
            enableLedOnAlarm(false);

            foreach (var item in Logger.Features)
            {
                switch (item.CurrentFeature)
                {
                    case SiMiFeatureEnum.CYCLIC:
                        enableCyclic(true);
                        break;
                    case SiMiFeatureEnum.PUSH_TO_RUN:
                        enablePushToRun(true);
                        break;
                    case SiMiFeatureEnum.TIMER_RUN:
                        enableTimerRun(true);
                        break;
                    case SiMiFeatureEnum.STOP_ON_KEY_PRESS:
                        enableStopOnKeyPress(true);
                        break;
                    case SiMiFeatureEnum.TEST_MODE:
                        enableTestMode(true);
                        break;
                    case SiMiFeatureEnum.DEEP_SLEEP:
                        enableDeepSleep(true);
                        break;
                }
            }
        }

        private void initializeSensors()
        {
            var fixedSensors = Logger.Sensors.GetAllFixed().ToList();
            var detachableSensors = Logger.Sensors.GetAllDetachables().ToList();
            flowLayoutPanelSensors.Controls.Clear();

            foreach (GenericSensorV2 item in fixedSensors)
            {
                var sensorControl = new SiMiUserControlFixedSensor(item, this);
                flowLayoutPanelSensors.Controls.Add(sensorControl);
            }

            // MicroLite External
           /* if (Logger is MicroLite2E.Devices.MicroLite2ELogger && Program.UserMode)
            {
                // Sensor is NTC
                if (detachableSensors[0] is Base.Sensors.Types.Temperature.ExternalNTC)
                {
                    UserControlDetachableSensors sensorControl = new UserControlDetachableSensors(Logger.Sensors, detachableSensors[0]);
                    flowLayoutPanelSensors.Controls.Add(sensorControl);
                }
                else
                {
                    // Sensor is not temperature
                    UserControlDetachableSensors sensorControl = new UserControlDetachableSensors(Logger.Sensors, detachableSensors, false);
                    flowLayoutPanelSensors.Controls.Add(sensorControl);

                    if (!(detachableSensors[0] is GenericTemperatureSensor))
                        disableTemperatureControls();
                }
            }
            // Logger is not MicroLite External or not in User Mode
            else*/ if (detachableSensors.Count > 0)
            {
                var userControlDetachableSensors = new SiMiUserControlDetachableSensors(Logger.Sensors, detachableSensors, true);
                flowLayoutPanelSensors.Controls.Add(userControlDetachableSensors);

                if (Logger.Sensors.GetAllEnabled().Count() > 0)
                {
                    if (!(Logger.Sensors.GetAllEnabled().ToList()[0] is GenericTemperatureSensor))
                        disableTemperatureControls();
                }
            }
        }

        private void disableTemperatureControls()
        {
            labelTemperatureUnit.Visible = false;
            radioButtonCelsius.Visible = false;
            radioButtonFahrenheit.Visible = false;

            labelBoomerangTemperatureUnit.Visible = false;
            radioButtonBoomerangCelsius.Visible = false;
            radioButtonBoomerangFahrenheit.Visible = false;
        }

        #endregion
        #region Status Loading
        public void LoadFromStatus()
        {
            initializeSensors();
            loadStatusConfiguration(Logger.Status);
        }

        protected virtual void loadStatusConfiguration(SiMiLogStatusConfiguration i_Status)
        {
            loadProperties(i_Status);
            loadSensors(i_Status);
            loadDisplay(i_Status);
            loadBoomerang(i_Status);
        }

        protected virtual void loadProperties(SiMiLogStatusConfiguration i_Status)
        {
            SerialNumber = i_Status.SerialNumber;
            Comment = i_Status.Comment;
            PushToRun = i_Status.PushToRunMode;
            TimerRun = i_Status.TimerRun>0;
            if (TimerRun)
            {
                TimerRunStart = new DateTime(1970,1,1).AddSeconds( i_Status.TimerRun);                
            }

            Cyclic = i_Status.CyclicMode;
            Interval = (ushort)i_Status.Interval.TotalSeconds;

            if (i_Status is SiMiLogStatusConfiguration)
            {
              /*  var status = i_Status as MicroLogStatusConfiguration;

                AveragingPoints = status.AveragePoints;
                MemorySize = status.MemorySize;
                DeepSleepMode = status.DeepSleepMode;
                StopOnKeyPress = status.StopOnKeyPress;
                StopOnSensorDisconnects = status.StopOnDisconnect;*/
            }

//            if (i_Status is PicoLiteV1StatusConfiguration)
//            {
//                var status = i_Status as PicoLiteV1StatusConfiguration;
//
//                DebugMode = status.TestMode;
//            }
        }

        private void loadSensors(SiMiLogStatusConfiguration i_Status)
        {
            Fahrenheit = i_Status.FahrenheitMode;

            if (i_Status is SiMiLogStatusConfiguration)
                foreach (SiMiUserControlFixedSensor item in flowLayoutPanelSensors.Controls)
                {
                    item.LoadFromStatus(i_Status);
                }

//            if (i_Status is PicoLiteV1StatusConfiguration)
//                foreach (UserControlFixedSensor item in flowLayoutPanelSensors.Controls)
//                {
//                    item.LoadFromStatus(i_Status as PicoLiteV1StatusConfiguration);
//                }
        }

        private void loadDisplay(SiMiLogStatusConfiguration i_Status)
        {
            if (i_Status is SiMiLogStatusConfiguration)
            {
                var status = i_Status ;

                //LCDConfiguration = status.LCDConfiguration;
                //ShowMinMax = status.ShowMinMax;
                //ShowMinMax24h = status.ShowPast24HMinMax;
                //DisableLEDOnAlarm = status.EnableLEDOnAlarm;
            }
        }

        private void loadBoomerang(SiMiLogStatusConfiguration i_Status)
        {
            BoomerangEnable = i_Status.BoomerangEnabled;
            BoomerangGeneratedBy = i_Status.Boomerang.Author;
            BoomerangUnit = i_Status.Boomerang.CelsiusMode;
            BoomerangComment = i_Status.Boomerang.Comment;
            BoomerangDisplayAlarm = i_Status.Boomerang.DisplayAlarmLevels;
            BoomerangEmailAdresses = i_Status.Boomerang.Contacts;
        }

        #endregion
        #region Events
        private void checkBoxTimerRun_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerTimerRun.Enabled = (sender as CheckBox).Checked;

            if (checkBoxPushToRun.Checked)
            {
                checkBoxPushToRun.CheckedChanged -= checkBoxPushToRun_CheckedChanged;
                checkBoxPushToRun.Checked = false;
                checkBoxPushToRun.CheckedChanged += checkBoxPushToRun_CheckedChanged;
            }
        }

        private void checkBoxPushToRun_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTimerRun.Checked)
            {
                checkBoxTimerRun.CheckedChanged -= checkBoxTimerRun_CheckedChanged;
                checkBoxTimerRun.Checked = false;
                dateTimePickerTimerRun.Enabled = false;
                checkBoxTimerRun.CheckedChanged += checkBoxTimerRun_CheckedChanged;
            }
        }

        private void checkBoxBoomerang_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (sender as CheckBox);

            buttonAddBoomerangMail.Enabled = cb.Checked;
            radioButtonBoomerangCelsius.Enabled = cb.Checked;
            radioButtonBoomerangFahrenheit.Enabled = cb.Checked;
            textBoxBoomerangComment.Enabled = cb.Checked;
            textBoxBoomerangGeneratedBy.Enabled = cb.Checked;
            checkBoxBoomerangAlarmLevel.Enabled = cb.Checked;
            listBoxBoomerangEmails.Enabled = cb.Checked;
        }

        private void checkBoxShowMinMax_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxShowMinMaxPast24.Enabled = (sender as CheckBox).Checked;
        }

        private void buttonAddBoomerangMail_Click(object sender, EventArgs e)
        {
            if (textBoxBoomerangMailAdress.Text.IsEmailValid())
            {
                listBoxBoomerangEmails.Items.Add(textBoxBoomerangMailAdress.Text);
                textBoxBoomerangMailAdress.Text = null;
            }
            else
            {
                MessageBox.Show("Email address is not valid");
                textBoxBoomerangMailAdress.Text = null;                
            }
        }

        private void listBoxBoomerangEmails_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_BoomerangEmailItemToDelete = this.listBoxBoomerangEmails.IndexFromPoint(e.Location);
                if (m_BoomerangEmailItemToDelete != ListBox.NoMatches)
                {
                    listBoxBoomerangEmails.SelectedIndex = m_BoomerangEmailItemToDelete;
                    ContextMenu contextMenu = new ContextMenu();

                    MenuItem menuItemRun = new MenuItem("Delete");
                    menuItemRun.Click += menuItemRun_Click;
                    contextMenu.MenuItems.Add(menuItemRun);

                    contextMenu.Show(listBoxBoomerangEmails, e.Location, LeftRightAlignment.Right);
                }
            }
        }

        void menuItemRun_Click(object sender, EventArgs e)
        {
            listBoxBoomerangEmails.Items.RemoveAt(m_BoomerangEmailItemToDelete);
        }

        private void radioButtonCelsius_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;

            if (button.Text == "Celsius" && button.Checked)
            {
                updateSensors(true);
            }

            if (button.Text == "Fahrenheit" && button.Checked)
            {
                updateSensors(false);
            }
        }
        #endregion
        #region Properties
        // properties tab
        public DateTime TimerRunStart 
        {
            get { return dateTimePickerTimerRun.Value; } 
            private set
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { timerRunStart(value); }));
                }
                else
                {
                    timerRunStart(value);
                }
            } 
        }

        private void timerRunStart(DateTime value)
        {
            dateTimePickerTimerRun.Value = value;
        }

        public bool DebugMode
        {
            get { return cbDebug.Checked; }
            private set
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { debugMode(value); }));
                }
                else
                {
                    debugMode(value);
                }
            }
        }

        private void debugMode(bool debug)
        {
            cbDebug.Checked = debug;
        }

        public string SerialNumber 
        {
            get { return textBoxSN.Text; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { serialNumber(value); }));
                }
                else
                {
                    serialNumber(value);
                }
            } 
        }

        private void serialNumber(string value)
        {
            textBoxSN.Text = value; 
        }

//        public MemorySizeEnum MemorySize
//        {
//            get
//            {
//                try
//                {
//                    if (comboBoxMemorySize.SelectedItem == null)
//                    {
//                        return (Logger as GenericSiMiLogLogger).Status.MemorySize;
//                    }
//
//                    return (MemorySizeEnum)Enum.Parse(typeof(MemorySizeEnum), comboBoxMemorySize.SelectedItem.ToString());
//                }
//                catch(Exception ex)
//                {
//                    Console.Write(ex);
//                    return MemorySizeEnum.K52;
//                }
//            }
//            private set
//            {
//                comboBoxMemorySize.SelectedItem = value;
//            }
//        }

        public bool DeepSleepMode
        {
            get { return checkBoxDeepSleepMode.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { deepSleepMode(value); }));
                }
                else
                {
                    deepSleepMode(value);
                }
            }
        }

        private void deepSleepMode(bool value)
        {
            checkBoxDeepSleepMode.Checked = value; 
        }

        public byte AveragingPoints
        {
            get { return Convert.ToByte(numericUpDownAveragingPoints.Value - 1); }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { averagingPoints(value); }));
                }
                else
                {
                    averagingPoints(value + 1);
                }
            }
        }

        private void averagingPoints(decimal value)
        {
            numericUpDownAveragingPoints.Value = value; 
        }

        public string Comment
        {
            get { return textBoxComment.Text; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { comment(value); }));
                }
                else
                {
                    comment(value);
                }
            }
        }

        private void comment(string value)
        {
            textBoxComment.Text = value; 

        }

        public bool PushToRun
        {
            get { return checkBoxPushToRun.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { pushToRun(value); }));
                }
                else
                {
                    pushToRun(value);
                }
            }
        }

        private void pushToRun(bool value)
        {
            checkBoxPushToRun.Checked = value; 
        }

        public bool TimerRun
        {
            get { return checkBoxTimerRun.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { timerRun(value); }));
                }
                else
                {
                    timerRun(value);
                }
            }
        }

        private void timerRun(bool value)
        {
            checkBoxTimerRun.Checked = value; 
        }

        public bool Cyclic
        {
            get { return checkBoxCyclic.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { cyclic(value); }));
                }
                else
                {
                    cyclic(value);
                }
            }
        }

        private void cyclic(bool value)
        {
            checkBoxCyclic.Checked = value; 
        }

        public bool StopOnKeyPress
        {
            get { return checkBoxStopOnKeyPress.Checked; }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { stopOnKeyPress(value); }));
                }
                else
                {
                    stopOnKeyPress(value);
                }
            }
        }

        private void stopOnKeyPress(bool value)
        {
            checkBoxStopOnKeyPress.Checked = value; 
        }

        public bool StopOnSensorDisconnects
        {
            get { return checkBoxStopOnSensorDisconnects.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { stopOnSensorDisconnects(value); }));
                }
                else
                {
                    stopOnSensorDisconnects(value);
                }
            }
        }

        private void stopOnSensorDisconnects(bool value)
        {
            checkBoxStopOnSensorDisconnects.Checked = value; 
        }

        public UInt64 Interval
        {
            get
            {
                try
                {
                    return Convert.ToUInt64(numericUpDownInterval.Value);
                }
                catch
                {
                    return 1;
                }
            }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { interval(value); }));
                }
                else
                {
                    interval(value);
                }
            }
        }

        private void interval(UInt64 value)
        {
            if (value <= 0)
            {
                Console.WriteLine(string.Format("Error: Value of Interval is: {0}.", value));
                value = 1;
            }

            numericUpDownInterval.Value = value; 
        }

        // Sensor tab
        public bool TemperatureEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.Temperature).Enable;}
        }

        public bool TemperatureAlarmEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.Temperature).AlarmEnable; }
        }

        public decimal TemperatureAlarmLow
        {
            get { return Convert.ToDecimal(GetUCFixedSensor(eSensorIndex.Temperature).AlarmLow); }
        }

        public decimal TemperatureAlarmHigh
        {
            get { return Convert.ToDecimal(GetUCFixedSensor(eSensorIndex.Temperature).AlarmHigh); }
        }

        public bool HumidityEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.Humidity).Enable; }
        }

        public bool HumidityAlarmEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.Humidity).AlarmEnable; }
        }

        public decimal HumidityAlarmLow
        {
            get { return Convert.ToDecimal(GetUCFixedSensor(eSensorIndex.Humidity).AlarmLow); }
        }

        public decimal HumidityAlarmHigh
        {
            get { return Convert.ToDecimal(GetUCFixedSensor(eSensorIndex.Humidity).AlarmHigh); }
        }

        public bool DewPointEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.DewPoint).Enable; }
        }

        public bool DewPointAlarmEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.DewPoint).AlarmEnable; }
        }

        public decimal DewPointAlarmLow
        {
            get { return Convert.ToDecimal(GetUCFixedSensor(eSensorIndex.DewPoint).AlarmLow); }
        }

        public decimal DewPointAlarmHigh
        {
            get { return Convert.ToDecimal(GetUCFixedSensor(eSensorIndex.DewPoint).AlarmHigh); }
        }

        public bool ExternalEnable
        {
            get { return GetUCFixedSensor(eSensorIndex.External1).Enable; }
        }

        public eSensorType ExternalType
        {
            get 
            {
                var defineSensor = getUCDetachableSensor(eSensorIndex.External1);

                if (defineSensor.DefineSensor)
                {
                    return eSensorType.UserDefined;
                }

                return getUCDetachableSensor(eSensorIndex.External1).Sensor.Type; 
            }
        }

        public SiMiLogUDSConfiguration ExternalUserDefinedSensor
        {
            get
            {
                var userControl = getUCDetachableSensor(eSensorIndex.External1);
                
//                 if (userControl.DefineSensor)
//                {
//                    if (userControl.Configuration == null)
//                    {
//                        var sensor = (Logger.Sensors.GetEnabledDetachable() as UserDefinedSensor);
//                        var config = new SiMiLogUDSConfiguration()
//                        {
//                            BaseType = sensor.UDS.BaseType,
//                            CustomUnit = sensor.UDS.CustomUnit,
//                            Gain = sensor.UDS.Gain,
//                            Name = sensor.UDS.Name,
//                            Offset = sensor.UDS.Offset,
//                            SignificantFigures = sensor.UDS.SignificantFigures,
//                           // Unit = sensor.UDS.Unit,
//                        };
//
//                        config.Set(sensor.UDS.References);
//
//                        return config;
//                    }
//
//                    return userControl.Configuration;
//                }

                return null;
            }
        }

        public bool ExternalAlarmEnable
        {
            get { return getUCDetachableSensor(eSensorIndex.External1).AlarmEnable; }
        }

        public decimal ExternalAlarmLow
        {
            get { return Convert.ToDecimal(getUCDetachableSensor(eSensorIndex.External1).AlarmLow); }
        }

        public decimal ExternalAlarmHigh
        {
            get { return Convert.ToDecimal(getUCDetachableSensor(eSensorIndex.External1).AlarmHigh); }
        }

        public ushort AlarmDelay
        {
            get { return Convert.ToUInt16(numericUpDownAlarmDelay.Value); }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { alarmDelay(value); }));
                }
                else
                {
                    alarmDelay(value);
                }
            }
        }

        private void alarmDelay(ushort value)
        {
            numericUpDownAlarmDelay.Value = value; 
        }

        public bool Fahrenheit
        {
            get { return radioButtonFahrenheit.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { fahrenheit(value); }));
                }
                else
                {
                    fahrenheit(value);
                }
            }
        }

        private void fahrenheit(bool value)
        {
            radioButtonFahrenheit.Checked = value;
            radioButtonCelsius.Checked = !value;
        }

        // Display Tab
        public LCDConfigurationEnum LCDConfiguration
        {
            get 
            {
                string[] names = Enum.GetNames(typeof(LCDConfigurationEnum));

                return (LCDConfigurationEnum)Enum.Parse(typeof(LCDConfigurationEnum), names[comboBoxDisplay.SelectedIndex]);
            }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { lcdConfiguration(value); }));
                }
                else
                {
                    lcdConfiguration(value);
                }
            }
        }

        private void lcdConfiguration(LCDConfigurationEnum value)
        {
            string[] names = Enum.GetNames(typeof(LCDConfigurationEnum));
            string name = Enum.GetName(typeof(LCDConfigurationEnum), value);

            if (name != null)
            {
                comboBoxDisplay.SelectedIndex = names.ToList().IndexOf(name);                
            }
            else
            {
                comboBoxDisplay.SelectedIndex = 0;
            }
        }

        public bool ShowMinMax
        {
            get { return checkBoxShowMinMax.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { showMinMax(value); }));
                }
                else
                {
                    showMinMax(value);
                }
            }
        }

        private void showMinMax(bool value)
        {
            checkBoxShowMinMax.Checked = value; 
        }

        public bool ShowMinMax24h
        {
            get { return checkBoxShowMinMaxPast24.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { showMinMax24h(value); }));
                }
                else
                {
                    showMinMax24h(value);
                }
            }
        }

        private void showMinMax24h(bool value)
        {
            checkBoxShowMinMaxPast24.Checked = value; 
        }

        public bool DisableLEDOnAlarm
        {
            get { return !checkBoxDisableLED.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { disableLEDOnAlarm(value); }));
                }
                else
                {
                    disableLEDOnAlarm(value);
                }
            }
        }

        private void disableLEDOnAlarm(bool value)
        {
            checkBoxDisableLED.Checked = !value; 
        }

        // Boomerang Tab
        public bool BoomerangEnable
        {
            get { return checkBoxBoomerang.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { boomerangEnable(value); }));
                }
                else
                {
                    boomerangEnable(value);
                }
            }
        }

        private void boomerangEnable(bool value)
        {
            checkBoxBoomerang.Checked = value; 
        }

        public bool BoomerangUnit
        {
            get { return radioButtonBoomerangCelsius.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { boomerangUnit(value); }));
                }
                else
                {
                    boomerangUnit(value);
                }
            } 
        }

        private void boomerangUnit(bool value)
        {
            radioButtonBoomerangFahrenheit.Checked = !value;
            radioButtonBoomerangCelsius.Checked = value;
        }

        public string BoomerangGeneratedBy
        {
            get { return textBoxBoomerangGeneratedBy.Text; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { boomerangGeneratedBy(value); }));
                }
                else
                {
                    boomerangGeneratedBy(value);
                }
            }
        }

        private void boomerangGeneratedBy(string value)
        {
            textBoxBoomerangGeneratedBy.Text = value; 

        }

        public string BoomerangComment
        {
            get { return textBoxBoomerangComment.Text; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { boomerangComment(value); }));
                }
                else
                {
                    boomerangComment(value);
                }
            }
        }

        private void boomerangComment(string value)
        {
            textBoxBoomerangComment.Text = value; 
        }

        public bool BoomerangDisplayAlarm
        {
            get { return checkBoxBoomerangAlarmLevel.Checked; }
            private set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { boomerangDisplayAlarm(value); }));
                }
                else
                {
                    boomerangDisplayAlarm(value);
                }
            }
        }

        private void boomerangDisplayAlarm(bool value)
        {
            checkBoxBoomerangAlarmLevel.Checked = value; 
        }

        public List<BasicContact> BoomerangEmailAdresses
        {
            get
            {
                List<BasicContact> toReturn = new List<BasicContact>();

                foreach (var item in listBoxBoomerangEmails.Items)
                {
                    toReturn.Add(new BasicContact() { EMail = item.ToString() });
                }

                return toReturn;
            }
            private set
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { boomerangEmailAdresses(value); }));
                }
                else
                {
                    boomerangEmailAdresses(value);
                }
            }
        }

        private void boomerangEmailAdresses(List<BasicContact> value)
        {
            listBoxBoomerangEmails.Items.Clear();

            List<BasicContact> list = value as List<BasicContact>;
            foreach (var item in list)
            {
                listBoxBoomerangEmails.Items.Add(item.EMail);
            }
        }
        #endregion
        #region Methods
        internal void Close()
        {
            foreach (SiMiUserControlFixedSensor item in flowLayoutPanelSensors.Controls)
            {
                item.Close();
            }
        }

        public SiMiUserControlFixedSensor GetUCFixedSensor(eSensorIndex i_Index)
        {
            foreach (SiMiUserControlFixedSensor item in flowLayoutPanelSensors.Controls)
            {
                if (item.Index == i_Index)
                {
                    return item;
                }
            }

            return null;
        }

        private SiMiUserControlDetachableSensors getUCDetachableSensor(eSensorIndex i_Index)
        {
            foreach (SiMiUserControlFixedSensor item in flowLayoutPanelSensors.Controls)
            {
                if (item is SiMiUserControlDetachableSensors && item.Index == i_Index)
                {
                    return (SiMiUserControlDetachableSensors)item;
                }
            }

            return null;
        }

        public void DisbleAllControls()
        {
            foreach (Control item1 in this.Controls)
            {
                if (item1 is TabControl)
                {
                    foreach (Control item2 in item1.Controls)
                    {
                        item2.Enabled = false;
                    }
                }
                else
                {
                    item1.Enabled = false;                                        
                }
            }
        }

        bool firstTime = true;
        private void updateSensors(bool celsius)
        {
            if (firstTime)
            {
                firstTime = false;
                return;
            }

            foreach (UserControlFixedSensor item in flowLayoutPanelSensors.Controls)
            {
                item.RefreshUnit(celsius);
            }
        }
        #endregion
        #region Features
        private void enableAlarmDelay(bool i_Enable)
        {
            numericUpDownAlarmDelay.Visible = i_Enable;
            labelAlarmDelay.Visible = i_Enable;
            labelAlarmDelaySeconds.Visible = i_Enable;
        }

        private void enableLedOnAlarm(bool i_Enable)
        {
            checkBoxDisableLED.Visible = i_Enable;
        }

        private void enableDeepSleep(bool i_Enable)
        {
            checkBoxDeepSleepMode.Visible = i_Enable;
        }

        private void enableShowMinMax(bool i_Enable)
        {
            checkBoxShowMinMax.Visible = i_Enable;
            checkBoxShowMinMaxPast24.Visible = i_Enable;
        }

        private void enableMemorySize(bool i_Enable)
        {
//            labelMemorySize.Visible = i_Enable;
//            comboBoxMemorySize.Visible = i_Enable;
//
//            if (i_Enable && Logger is GenericMicroLogLogger)
//            {
//                comboBoxMemorySize.Items.Clear();
//
//                foreach (var item in (Logger as GenericMicroLogLogger).AvailableMemorySizes)
//                {
//                    comboBoxMemorySize.Items.Add(item);
//                }
//             }
        }

        protected void enableTestMode(bool i_Enable)
        {
            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation || !i_Enable)
                cbDebug.Visible = i_Enable;
        }

        private void enableStopOnDisconnect(bool i_Enable)
        {
            checkBoxStopOnSensorDisconnects.Visible = i_Enable;
        }

        private void enableStopOnKeyPress(bool i_Enable)
        {
            checkBoxStopOnKeyPress.Visible = i_Enable;
        }

        private void enableTimerRun(bool i_Enable)
        {
            checkBoxTimerRun.Visible = i_Enable;
            dateTimePickerTimerRun.Visible = i_Enable;
        }

        private void enablePushToRun(bool i_Enable)
        {
            checkBoxPushToRun.Visible = i_Enable;
        }

        private void enableCyclic(bool i_Enable)
        {
            checkBoxCyclic.Visible = i_Enable;
        }

        #endregion
    }
}
