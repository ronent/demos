﻿namespace API_UI.Forms.UserControls.Setup
{
    public partial class MicroLite2Setup : MicroLogSetup
    {
        public MicroLite2Setup(MicroLogAPI.Devices.MicroLiteLogger device)
            :base(device)
        {

        }

        public MicroLite2Setup()
        {
            InitializeComponent();
        }

        protected override void Initialize()
        {
            if (this.Logger is MicroLite2E.Devices.MicroLite2ELogger)
            {
                checkBoxStopOnSensorDisconnects.Visible = true;
            }
        }
    }
}
