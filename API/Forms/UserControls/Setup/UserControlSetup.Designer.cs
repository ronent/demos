﻿namespace API_UI.Forms.UserControls.Setup
{
    partial class UserControlSetup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlSetup = new System.Windows.Forms.TabControl();
            this.tabPageProperties = new System.Windows.Forms.TabPage();
            this.cbDebug = new System.Windows.Forms.CheckBox();
            this.labelMemorySize = new System.Windows.Forms.Label();
            this.comboBoxMemorySize = new System.Windows.Forms.ComboBox();
            this.checkBoxTimerRun = new System.Windows.Forms.CheckBox();
            this.checkBoxPushToRun = new System.Windows.Forms.CheckBox();
            this.labelBatteryLevel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelFWVersion = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.checkBoxDeepSleepMode = new System.Windows.Forms.CheckBox();
            this.labelAveragingPointsType = new System.Windows.Forms.Label();
            this.numericUpDownAveragingPoints = new System.Windows.Forms.NumericUpDown();
            this.labelAverangingPoints = new System.Windows.Forms.Label();
            this.checkBoxStopOnSensorDisconnects = new System.Windows.Forms.CheckBox();
            this.labelInterval = new System.Windows.Forms.Label();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.textBoxSN = new System.Windows.Forms.TextBox();
            this.labelText = new System.Windows.Forms.Label();
            this.labelSN = new System.Windows.Forms.Label();
            this.numericUpDownInterval = new System.Windows.Forms.NumericUpDown();
            this.dateTimePickerTimerRun = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxCyclic = new System.Windows.Forms.CheckBox();
            this.checkBoxStopOnKeyPress = new System.Windows.Forms.CheckBox();
            this.tabPageSensors = new System.Windows.Forms.TabPage();
            this.labelTemperatureUnit = new System.Windows.Forms.Label();
            this.flowLayoutPanelSensors = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButtonFahrenheit = new System.Windows.Forms.RadioButton();
            this.radioButtonCelsius = new System.Windows.Forms.RadioButton();
            this.labelAlarmDelaySeconds = new System.Windows.Forms.Label();
            this.numericUpDownAlarmDelay = new System.Windows.Forms.NumericUpDown();
            this.labelAlarmDelay = new System.Windows.Forms.Label();
            this.tabPageDisplay = new System.Windows.Forms.TabPage();
            this.checkBoxDisableLED = new System.Windows.Forms.CheckBox();
            this.checkBoxShowMinMaxPast24 = new System.Windows.Forms.CheckBox();
            this.checkBoxShowMinMax = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDisplay = new System.Windows.Forms.ComboBox();
            this.tabPageBoomerang = new System.Windows.Forms.TabPage();
            this.checkBoxBoomerangAlarmLevel = new System.Windows.Forms.CheckBox();
            this.textBoxBoomerangComment = new System.Windows.Forms.TextBox();
            this.textBoxBoomerangGeneratedBy = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.radioButtonBoomerangFahrenheit = new System.Windows.Forms.RadioButton();
            this.radioButtonBoomerangCelsius = new System.Windows.Forms.RadioButton();
            this.labelBoomerangTemperatureUnit = new System.Windows.Forms.Label();
            this.buttonAddBoomerangMail = new System.Windows.Forms.Button();
            this.textBoxBoomerangMailAdress = new System.Windows.Forms.TextBox();
            this.listBoxBoomerangEmails = new System.Windows.Forms.ListBox();
            this.checkBoxBoomerang = new System.Windows.Forms.CheckBox();
            this.tabControlSetup.SuspendLayout();
            this.tabPageProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAveragingPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).BeginInit();
            this.tabPageSensors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAlarmDelay)).BeginInit();
            this.tabPageDisplay.SuspendLayout();
            this.tabPageBoomerang.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlSetup
            // 
            this.tabControlSetup.Controls.Add(this.tabPageProperties);
            this.tabControlSetup.Controls.Add(this.tabPageSensors);
            this.tabControlSetup.Controls.Add(this.tabPageDisplay);
            this.tabControlSetup.Controls.Add(this.tabPageBoomerang);
            this.tabControlSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSetup.Location = new System.Drawing.Point(0, 0);
            this.tabControlSetup.Name = "tabControlSetup";
            this.tabControlSetup.SelectedIndex = 0;
            this.tabControlSetup.Size = new System.Drawing.Size(313, 341);
            this.tabControlSetup.TabIndex = 13;
            // 
            // tabPageProperties
            // 
            this.tabPageProperties.Controls.Add(this.cbDebug);
            this.tabPageProperties.Controls.Add(this.labelMemorySize);
            this.tabPageProperties.Controls.Add(this.comboBoxMemorySize);
            this.tabPageProperties.Controls.Add(this.checkBoxTimerRun);
            this.tabPageProperties.Controls.Add(this.checkBoxPushToRun);
            this.tabPageProperties.Controls.Add(this.labelBatteryLevel);
            this.tabPageProperties.Controls.Add(this.label16);
            this.tabPageProperties.Controls.Add(this.labelFWVersion);
            this.tabPageProperties.Controls.Add(this.label13);
            this.tabPageProperties.Controls.Add(this.checkBoxDeepSleepMode);
            this.tabPageProperties.Controls.Add(this.labelAveragingPointsType);
            this.tabPageProperties.Controls.Add(this.numericUpDownAveragingPoints);
            this.tabPageProperties.Controls.Add(this.labelAverangingPoints);
            this.tabPageProperties.Controls.Add(this.checkBoxStopOnSensorDisconnects);
            this.tabPageProperties.Controls.Add(this.labelInterval);
            this.tabPageProperties.Controls.Add(this.textBoxComment);
            this.tabPageProperties.Controls.Add(this.textBoxSN);
            this.tabPageProperties.Controls.Add(this.labelText);
            this.tabPageProperties.Controls.Add(this.labelSN);
            this.tabPageProperties.Controls.Add(this.numericUpDownInterval);
            this.tabPageProperties.Controls.Add(this.dateTimePickerTimerRun);
            this.tabPageProperties.Controls.Add(this.label1);
            this.tabPageProperties.Controls.Add(this.checkBoxCyclic);
            this.tabPageProperties.Controls.Add(this.checkBoxStopOnKeyPress);
            this.tabPageProperties.Location = new System.Drawing.Point(4, 22);
            this.tabPageProperties.Name = "tabPageProperties";
            this.tabPageProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProperties.Size = new System.Drawing.Size(305, 315);
            this.tabPageProperties.TabIndex = 0;
            this.tabPageProperties.Text = "Properties";
            this.tabPageProperties.UseVisualStyleBackColor = true;
            // 
            // cbDebug
            // 
            this.cbDebug.AutoSize = true;
            this.cbDebug.Location = new System.Drawing.Point(12, 206);
            this.cbDebug.Name = "cbDebug";
            this.cbDebug.Size = new System.Drawing.Size(58, 17);
            this.cbDebug.TabIndex = 45;
            this.cbDebug.Text = "Debug";
            this.cbDebug.UseVisualStyleBackColor = true;
            // 
            // labelMemorySize
            // 
            this.labelMemorySize.AutoSize = true;
            this.labelMemorySize.Location = new System.Drawing.Point(159, 72);
            this.labelMemorySize.Name = "labelMemorySize";
            this.labelMemorySize.Size = new System.Drawing.Size(67, 13);
            this.labelMemorySize.TabIndex = 43;
            this.labelMemorySize.Text = "Memory Size";
            // 
            // comboBoxMemorySize
            // 
            this.comboBoxMemorySize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMemorySize.FormattingEnabled = true;
            this.comboBoxMemorySize.Location = new System.Drawing.Point(236, 69);
            this.comboBoxMemorySize.Name = "comboBoxMemorySize";
            this.comboBoxMemorySize.Size = new System.Drawing.Size(53, 21);
            this.comboBoxMemorySize.TabIndex = 42;
            // 
            // checkBoxTimerRun
            // 
            this.checkBoxTimerRun.AutoSize = true;
            this.checkBoxTimerRun.Location = new System.Drawing.Point(12, 117);
            this.checkBoxTimerRun.Name = "checkBoxTimerRun";
            this.checkBoxTimerRun.Size = new System.Drawing.Size(70, 17);
            this.checkBoxTimerRun.TabIndex = 41;
            this.checkBoxTimerRun.Text = "Timer run";
            this.checkBoxTimerRun.UseVisualStyleBackColor = true;
            this.checkBoxTimerRun.CheckedChanged += new System.EventHandler(this.checkBoxTimerRun_CheckedChanged);
            // 
            // checkBoxPushToRun
            // 
            this.checkBoxPushToRun.AutoSize = true;
            this.checkBoxPushToRun.Location = new System.Drawing.Point(12, 87);
            this.checkBoxPushToRun.Name = "checkBoxPushToRun";
            this.checkBoxPushToRun.Size = new System.Drawing.Size(80, 17);
            this.checkBoxPushToRun.TabIndex = 40;
            this.checkBoxPushToRun.Text = "Push to run";
            this.checkBoxPushToRun.UseVisualStyleBackColor = true;
            this.checkBoxPushToRun.CheckedChanged += new System.EventHandler(this.checkBoxPushToRun_CheckedChanged);
            // 
            // labelBatteryLevel
            // 
            this.labelBatteryLevel.AutoSize = true;
            this.labelBatteryLevel.Location = new System.Drawing.Point(256, 36);
            this.labelBatteryLevel.Name = "labelBatteryLevel";
            this.labelBatteryLevel.Size = new System.Drawing.Size(33, 13);
            this.labelBatteryLevel.TabIndex = 39;
            this.labelBatteryLevel.Text = "Level";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(189, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 38;
            this.label16.Text = "Battery level:";
            // 
            // labelFWVersion
            // 
            this.labelFWVersion.AutoSize = true;
            this.labelFWVersion.Location = new System.Drawing.Point(256, 15);
            this.labelFWVersion.Name = "labelFWVersion";
            this.labelFWVersion.Size = new System.Drawing.Size(42, 13);
            this.labelFWVersion.TabIndex = 37;
            this.labelFWVersion.Text = "Version";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(189, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "Firmware:";
            // 
            // checkBoxDeepSleepMode
            // 
            this.checkBoxDeepSleepMode.AutoSize = true;
            this.checkBoxDeepSleepMode.Location = new System.Drawing.Point(144, 147);
            this.checkBoxDeepSleepMode.Name = "checkBoxDeepSleepMode";
            this.checkBoxDeepSleepMode.Size = new System.Drawing.Size(109, 17);
            this.checkBoxDeepSleepMode.TabIndex = 35;
            this.checkBoxDeepSleepMode.Text = "Deep sleep mode";
            this.checkBoxDeepSleepMode.UseVisualStyleBackColor = true;
            // 
            // labelAveragingPointsType
            // 
            this.labelAveragingPointsType.AutoSize = true;
            this.labelAveragingPointsType.Location = new System.Drawing.Point(159, 271);
            this.labelAveragingPointsType.Name = "labelAveragingPointsType";
            this.labelAveragingPointsType.Size = new System.Drawing.Size(51, 13);
            this.labelAveragingPointsType.TabIndex = 34;
            this.labelAveragingPointsType.Text = "[samples]";
            // 
            // numericUpDownAveragingPoints
            // 
            this.numericUpDownAveragingPoints.Location = new System.Drawing.Point(100, 269);
            this.numericUpDownAveragingPoints.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownAveragingPoints.Name = "numericUpDownAveragingPoints";
            this.numericUpDownAveragingPoints.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownAveragingPoints.TabIndex = 33;
            this.numericUpDownAveragingPoints.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelAverangingPoints
            // 
            this.labelAverangingPoints.AutoSize = true;
            this.labelAverangingPoints.Location = new System.Drawing.Point(9, 271);
            this.labelAverangingPoints.Name = "labelAverangingPoints";
            this.labelAverangingPoints.Size = new System.Drawing.Size(86, 13);
            this.labelAverangingPoints.TabIndex = 32;
            this.labelAverangingPoints.Text = "Averaging points";
            // 
            // checkBoxStopOnSensorDisconnects
            // 
            this.checkBoxStopOnSensorDisconnects.AutoSize = true;
            this.checkBoxStopOnSensorDisconnects.Location = new System.Drawing.Point(144, 177);
            this.checkBoxStopOnSensorDisconnects.Name = "checkBoxStopOnSensorDisconnects";
            this.checkBoxStopOnSensorDisconnects.Size = new System.Drawing.Size(157, 17);
            this.checkBoxStopOnSensorDisconnects.TabIndex = 31;
            this.checkBoxStopOnSensorDisconnects.Text = "Stop on sensor disconnects";
            this.checkBoxStopOnSensorDisconnects.UseVisualStyleBackColor = true;
            // 
            // labelInterval
            // 
            this.labelInterval.AutoSize = true;
            this.labelInterval.Location = new System.Drawing.Point(116, 241);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(53, 13);
            this.labelInterval.TabIndex = 30;
            this.labelInterval.Text = "[seconds]";
            // 
            // textBoxComment
            // 
            this.textBoxComment.Location = new System.Drawing.Point(70, 41);
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(100, 20);
            this.textBoxComment.TabIndex = 29;
            // 
            // textBoxSN
            // 
            this.textBoxSN.Location = new System.Drawing.Point(70, 12);
            this.textBoxSN.Name = "textBoxSN";
            this.textBoxSN.Size = new System.Drawing.Size(100, 20);
            this.textBoxSN.TabIndex = 28;
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(9, 44);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(54, 13);
            this.labelText.TabIndex = 27;
            this.labelText.Text = "Comment:";
            // 
            // labelSN
            // 
            this.labelSN.AutoSize = true;
            this.labelSN.Location = new System.Drawing.Point(9, 15);
            this.labelSN.Name = "labelSN";
            this.labelSN.Size = new System.Drawing.Size(28, 13);
            this.labelSN.TabIndex = 26;
            this.labelSN.Text = "SN: ";
            // 
            // numericUpDownInterval
            // 
            this.numericUpDownInterval.Location = new System.Drawing.Point(57, 239);
            this.numericUpDownInterval.Maximum = new decimal(new int[] {
            66600,
            0,
            0,
            0});
            this.numericUpDownInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownInterval.Name = "numericUpDownInterval";
            this.numericUpDownInterval.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownInterval.TabIndex = 25;
            this.numericUpDownInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dateTimePickerTimerRun
            // 
            this.dateTimePickerTimerRun.CustomFormat = "HH:mm:ss dd/MM/yy";
            this.dateTimePickerTimerRun.Enabled = false;
            this.dateTimePickerTimerRun.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTimerRun.Location = new System.Drawing.Point(101, 112);
            this.dateTimePickerTimerRun.Name = "dateTimePickerTimerRun";
            this.dateTimePickerTimerRun.Size = new System.Drawing.Size(135, 20);
            this.dateTimePickerTimerRun.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Interval";
            // 
            // checkBoxCyclic
            // 
            this.checkBoxCyclic.AutoSize = true;
            this.checkBoxCyclic.Location = new System.Drawing.Point(12, 147);
            this.checkBoxCyclic.Name = "checkBoxCyclic";
            this.checkBoxCyclic.Size = new System.Drawing.Size(72, 17);
            this.checkBoxCyclic.TabIndex = 18;
            this.checkBoxCyclic.Text = "Cyclic run";
            this.checkBoxCyclic.UseVisualStyleBackColor = true;
            // 
            // checkBoxStopOnKeyPress
            // 
            this.checkBoxStopOnKeyPress.AutoSize = true;
            this.checkBoxStopOnKeyPress.Location = new System.Drawing.Point(12, 177);
            this.checkBoxStopOnKeyPress.Name = "checkBoxStopOnKeyPress";
            this.checkBoxStopOnKeyPress.Size = new System.Drawing.Size(111, 17);
            this.checkBoxStopOnKeyPress.TabIndex = 19;
            this.checkBoxStopOnKeyPress.Text = "Stop on key press";
            this.checkBoxStopOnKeyPress.UseVisualStyleBackColor = true;
            // 
            // tabPageSensors
            // 
            this.tabPageSensors.Controls.Add(this.labelTemperatureUnit);
            this.tabPageSensors.Controls.Add(this.flowLayoutPanelSensors);
            this.tabPageSensors.Controls.Add(this.radioButtonFahrenheit);
            this.tabPageSensors.Controls.Add(this.radioButtonCelsius);
            this.tabPageSensors.Controls.Add(this.labelAlarmDelaySeconds);
            this.tabPageSensors.Controls.Add(this.numericUpDownAlarmDelay);
            this.tabPageSensors.Controls.Add(this.labelAlarmDelay);
            this.tabPageSensors.Location = new System.Drawing.Point(4, 22);
            this.tabPageSensors.Name = "tabPageSensors";
            this.tabPageSensors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSensors.Size = new System.Drawing.Size(305, 315);
            this.tabPageSensors.TabIndex = 1;
            this.tabPageSensors.Text = "Sensors";
            this.tabPageSensors.UseVisualStyleBackColor = true;
            // 
            // labelTemperatureUnit
            // 
            this.labelTemperatureUnit.AutoSize = true;
            this.labelTemperatureUnit.Location = new System.Drawing.Point(7, 12);
            this.labelTemperatureUnit.Name = "labelTemperatureUnit";
            this.labelTemperatureUnit.Size = new System.Drawing.Size(87, 13);
            this.labelTemperatureUnit.TabIndex = 37;
            this.labelTemperatureUnit.Text = "Temperature unit";
            // 
            // flowLayoutPanelSensors
            // 
            this.flowLayoutPanelSensors.Location = new System.Drawing.Point(6, 65);
            this.flowLayoutPanelSensors.Name = "flowLayoutPanelSensors";
            this.flowLayoutPanelSensors.Size = new System.Drawing.Size(290, 244);
            this.flowLayoutPanelSensors.TabIndex = 36;
            // 
            // radioButtonFahrenheit
            // 
            this.radioButtonFahrenheit.AutoSize = true;
            this.radioButtonFahrenheit.Location = new System.Drawing.Point(167, 10);
            this.radioButtonFahrenheit.Name = "radioButtonFahrenheit";
            this.radioButtonFahrenheit.Size = new System.Drawing.Size(75, 17);
            this.radioButtonFahrenheit.TabIndex = 35;
            this.radioButtonFahrenheit.Text = "Fahrenheit";
            this.radioButtonFahrenheit.UseVisualStyleBackColor = true;
            this.radioButtonFahrenheit.CheckedChanged += new System.EventHandler(this.radioButtonCelsius_CheckedChanged);
            // 
            // radioButtonCelsius
            // 
            this.radioButtonCelsius.AutoSize = true;
            this.radioButtonCelsius.Location = new System.Drawing.Point(103, 10);
            this.radioButtonCelsius.Name = "radioButtonCelsius";
            this.radioButtonCelsius.Size = new System.Drawing.Size(58, 17);
            this.radioButtonCelsius.TabIndex = 34;
            this.radioButtonCelsius.Text = "Celsius";
            this.radioButtonCelsius.UseVisualStyleBackColor = true;
            this.radioButtonCelsius.CheckedChanged += new System.EventHandler(this.radioButtonCelsius_CheckedChanged);
            // 
            // labelAlarmDelaySeconds
            // 
            this.labelAlarmDelaySeconds.AutoSize = true;
            this.labelAlarmDelaySeconds.Location = new System.Drawing.Point(136, 41);
            this.labelAlarmDelaySeconds.Name = "labelAlarmDelaySeconds";
            this.labelAlarmDelaySeconds.Size = new System.Drawing.Size(53, 13);
            this.labelAlarmDelaySeconds.TabIndex = 33;
            this.labelAlarmDelaySeconds.Text = "[seconds]";
            // 
            // numericUpDownAlarmDelay
            // 
            this.numericUpDownAlarmDelay.Location = new System.Drawing.Point(77, 39);
            this.numericUpDownAlarmDelay.Name = "numericUpDownAlarmDelay";
            this.numericUpDownAlarmDelay.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownAlarmDelay.TabIndex = 32;
            this.numericUpDownAlarmDelay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelAlarmDelay
            // 
            this.labelAlarmDelay.AutoSize = true;
            this.labelAlarmDelay.Location = new System.Drawing.Point(7, 41);
            this.labelAlarmDelay.Name = "labelAlarmDelay";
            this.labelAlarmDelay.Size = new System.Drawing.Size(61, 13);
            this.labelAlarmDelay.TabIndex = 31;
            this.labelAlarmDelay.Text = "Alarm delay";
            // 
            // tabPageDisplay
            // 
            this.tabPageDisplay.Controls.Add(this.checkBoxDisableLED);
            this.tabPageDisplay.Controls.Add(this.checkBoxShowMinMaxPast24);
            this.tabPageDisplay.Controls.Add(this.checkBoxShowMinMax);
            this.tabPageDisplay.Controls.Add(this.label4);
            this.tabPageDisplay.Controls.Add(this.label3);
            this.tabPageDisplay.Controls.Add(this.comboBoxDisplay);
            this.tabPageDisplay.Location = new System.Drawing.Point(4, 22);
            this.tabPageDisplay.Name = "tabPageDisplay";
            this.tabPageDisplay.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDisplay.Size = new System.Drawing.Size(305, 315);
            this.tabPageDisplay.TabIndex = 2;
            this.tabPageDisplay.Text = "Display";
            this.tabPageDisplay.UseVisualStyleBackColor = true;
            // 
            // checkBoxDisableLED
            // 
            this.checkBoxDisableLED.AutoSize = true;
            this.checkBoxDisableLED.Location = new System.Drawing.Point(11, 180);
            this.checkBoxDisableLED.Name = "checkBoxDisableLED";
            this.checkBoxDisableLED.Size = new System.Drawing.Size(276, 17);
            this.checkBoxDisableLED.TabIndex = 5;
            this.checkBoxDisableLED.Text = "Disable LED alarm indications while device is running";
            this.checkBoxDisableLED.UseVisualStyleBackColor = true;
            // 
            // checkBoxShowMinMaxPast24
            // 
            this.checkBoxShowMinMaxPast24.AutoSize = true;
            this.checkBoxShowMinMaxPast24.Enabled = false;
            this.checkBoxShowMinMaxPast24.Location = new System.Drawing.Point(35, 139);
            this.checkBoxShowMinMaxPast24.Name = "checkBoxShowMinMaxPast24";
            this.checkBoxShowMinMaxPast24.Size = new System.Drawing.Size(93, 17);
            this.checkBoxShowMinMaxPast24.TabIndex = 4;
            this.checkBoxShowMinMaxPast24.Text = "Only pass 24h";
            this.checkBoxShowMinMaxPast24.UseVisualStyleBackColor = true;
            // 
            // checkBoxShowMinMax
            // 
            this.checkBoxShowMinMax.AutoSize = true;
            this.checkBoxShowMinMax.Location = new System.Drawing.Point(11, 116);
            this.checkBoxShowMinMax.Name = "checkBoxShowMinMax";
            this.checkBoxShowMinMax.Size = new System.Drawing.Size(98, 17);
            this.checkBoxShowMinMax.TabIndex = 3;
            this.checkBoxShowMinMax.Text = "Show Min/Max";
            this.checkBoxShowMinMax.UseVisualStyleBackColor = true;
            this.checkBoxShowMinMax.CheckedChanged += new System.EventHandler(this.checkBoxShowMinMax_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = " for the LCD to stay on:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Please select the apprproate time length";
            // 
            // comboBoxDisplay
            // 
            this.comboBoxDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDisplay.FormattingEnabled = true;
            this.comboBoxDisplay.Location = new System.Drawing.Point(11, 67);
            this.comboBoxDisplay.Name = "comboBoxDisplay";
            this.comboBoxDisplay.Size = new System.Drawing.Size(252, 21);
            this.comboBoxDisplay.TabIndex = 0;
            // 
            // tabPageBoomerang
            // 
            this.tabPageBoomerang.Controls.Add(this.checkBoxBoomerangAlarmLevel);
            this.tabPageBoomerang.Controls.Add(this.textBoxBoomerangComment);
            this.tabPageBoomerang.Controls.Add(this.textBoxBoomerangGeneratedBy);
            this.tabPageBoomerang.Controls.Add(this.label9);
            this.tabPageBoomerang.Controls.Add(this.label8);
            this.tabPageBoomerang.Controls.Add(this.radioButtonBoomerangFahrenheit);
            this.tabPageBoomerang.Controls.Add(this.radioButtonBoomerangCelsius);
            this.tabPageBoomerang.Controls.Add(this.labelBoomerangTemperatureUnit);
            this.tabPageBoomerang.Controls.Add(this.buttonAddBoomerangMail);
            this.tabPageBoomerang.Controls.Add(this.textBoxBoomerangMailAdress);
            this.tabPageBoomerang.Controls.Add(this.listBoxBoomerangEmails);
            this.tabPageBoomerang.Controls.Add(this.checkBoxBoomerang);
            this.tabPageBoomerang.Location = new System.Drawing.Point(4, 22);
            this.tabPageBoomerang.Name = "tabPageBoomerang";
            this.tabPageBoomerang.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBoomerang.Size = new System.Drawing.Size(305, 315);
            this.tabPageBoomerang.TabIndex = 3;
            this.tabPageBoomerang.Text = "Boomerang";
            this.tabPageBoomerang.UseVisualStyleBackColor = true;
            // 
            // checkBoxBoomerangAlarmLevel
            // 
            this.checkBoxBoomerangAlarmLevel.AutoSize = true;
            this.checkBoxBoomerangAlarmLevel.Location = new System.Drawing.Point(11, 119);
            this.checkBoxBoomerangAlarmLevel.Name = "checkBoxBoomerangAlarmLevel";
            this.checkBoxBoomerangAlarmLevel.Size = new System.Drawing.Size(158, 17);
            this.checkBoxBoomerangAlarmLevel.TabIndex = 42;
            this.checkBoxBoomerangAlarmLevel.Text = "Display alarm level on graph";
            this.checkBoxBoomerangAlarmLevel.UseVisualStyleBackColor = true;
            // 
            // textBoxBoomerangComment
            // 
            this.textBoxBoomerangComment.Location = new System.Drawing.Point(92, 84);
            this.textBoxBoomerangComment.Name = "textBoxBoomerangComment";
            this.textBoxBoomerangComment.Size = new System.Drawing.Size(158, 20);
            this.textBoxBoomerangComment.TabIndex = 41;
            // 
            // textBoxBoomerangGeneratedBy
            // 
            this.textBoxBoomerangGeneratedBy.Location = new System.Drawing.Point(92, 58);
            this.textBoxBoomerangGeneratedBy.Name = "textBoxBoomerangGeneratedBy";
            this.textBoxBoomerangGeneratedBy.Size = new System.Drawing.Size(158, 20);
            this.textBoxBoomerangGeneratedBy.TabIndex = 40;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "Generated by";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Comment";
            // 
            // radioButtonBoomerangFahrenheit
            // 
            this.radioButtonBoomerangFahrenheit.AutoSize = true;
            this.radioButtonBoomerangFahrenheit.Location = new System.Drawing.Point(175, 32);
            this.radioButtonBoomerangFahrenheit.Name = "radioButtonBoomerangFahrenheit";
            this.radioButtonBoomerangFahrenheit.Size = new System.Drawing.Size(75, 17);
            this.radioButtonBoomerangFahrenheit.TabIndex = 37;
            this.radioButtonBoomerangFahrenheit.Text = "Fahrenheit";
            this.radioButtonBoomerangFahrenheit.UseVisualStyleBackColor = true;
            // 
            // radioButtonBoomerangCelsius
            // 
            this.radioButtonBoomerangCelsius.AutoSize = true;
            this.radioButtonBoomerangCelsius.Checked = true;
            this.radioButtonBoomerangCelsius.Location = new System.Drawing.Point(111, 32);
            this.radioButtonBoomerangCelsius.Name = "radioButtonBoomerangCelsius";
            this.radioButtonBoomerangCelsius.Size = new System.Drawing.Size(58, 17);
            this.radioButtonBoomerangCelsius.TabIndex = 36;
            this.radioButtonBoomerangCelsius.TabStop = true;
            this.radioButtonBoomerangCelsius.Text = "Celsius";
            this.radioButtonBoomerangCelsius.UseVisualStyleBackColor = true;
            // 
            // labelBoomerangTemperatureUnit
            // 
            this.labelBoomerangTemperatureUnit.AutoSize = true;
            this.labelBoomerangTemperatureUnit.Location = new System.Drawing.Point(8, 36);
            this.labelBoomerangTemperatureUnit.Name = "labelBoomerangTemperatureUnit";
            this.labelBoomerangTemperatureUnit.Size = new System.Drawing.Size(87, 13);
            this.labelBoomerangTemperatureUnit.TabIndex = 4;
            this.labelBoomerangTemperatureUnit.Text = "Temperature unit";
            // 
            // buttonAddBoomerangMail
            // 
            this.buttonAddBoomerangMail.Location = new System.Drawing.Point(221, 146);
            this.buttonAddBoomerangMail.Name = "buttonAddBoomerangMail";
            this.buttonAddBoomerangMail.Size = new System.Drawing.Size(75, 23);
            this.buttonAddBoomerangMail.TabIndex = 3;
            this.buttonAddBoomerangMail.Text = "Add";
            this.buttonAddBoomerangMail.UseVisualStyleBackColor = true;
            this.buttonAddBoomerangMail.Click += new System.EventHandler(this.buttonAddBoomerangMail_Click);
            // 
            // textBoxBoomerangMailAdress
            // 
            this.textBoxBoomerangMailAdress.Location = new System.Drawing.Point(11, 149);
            this.textBoxBoomerangMailAdress.Name = "textBoxBoomerangMailAdress";
            this.textBoxBoomerangMailAdress.Size = new System.Drawing.Size(204, 20);
            this.textBoxBoomerangMailAdress.TabIndex = 2;
            // 
            // listBoxBoomerangEmails
            // 
            this.listBoxBoomerangEmails.FormattingEnabled = true;
            this.listBoxBoomerangEmails.Location = new System.Drawing.Point(11, 175);
            this.listBoxBoomerangEmails.Name = "listBoxBoomerangEmails";
            this.listBoxBoomerangEmails.Size = new System.Drawing.Size(285, 134);
            this.listBoxBoomerangEmails.TabIndex = 1;
            this.listBoxBoomerangEmails.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBoxBoomerangEmails_MouseUp);
            // 
            // checkBoxBoomerang
            // 
            this.checkBoxBoomerang.AutoSize = true;
            this.checkBoxBoomerang.Location = new System.Drawing.Point(11, 13);
            this.checkBoxBoomerang.Name = "checkBoxBoomerang";
            this.checkBoxBoomerang.Size = new System.Drawing.Size(148, 17);
            this.checkBoxBoomerang.TabIndex = 0;
            this.checkBoxBoomerang.Text = "Include Boomerang report";
            this.checkBoxBoomerang.UseVisualStyleBackColor = true;
            this.checkBoxBoomerang.CheckedChanged += new System.EventHandler(this.checkBoxBoomerang_CheckedChanged);
            // 
            // UserControlSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlSetup);
            this.Name = "UserControlSetup";
            this.Size = new System.Drawing.Size(313, 341);
            this.tabControlSetup.ResumeLayout(false);
            this.tabPageProperties.ResumeLayout(false);
            this.tabPageProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAveragingPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).EndInit();
            this.tabPageSensors.ResumeLayout(false);
            this.tabPageSensors.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAlarmDelay)).EndInit();
            this.tabPageDisplay.ResumeLayout(false);
            this.tabPageDisplay.PerformLayout();
            this.tabPageBoomerang.ResumeLayout(false);
            this.tabPageBoomerang.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxComment;
        private System.Windows.Forms.TextBox textBoxSN;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Label labelSN;
        private System.Windows.Forms.NumericUpDown numericUpDownInterval;
        private System.Windows.Forms.DateTimePicker dateTimePickerTimerRun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxCyclic;
        private System.Windows.Forms.CheckBox checkBoxStopOnKeyPress;
        private System.Windows.Forms.TabPage tabPageSensors;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelSensors;
        private System.Windows.Forms.RadioButton radioButtonFahrenheit;
        private System.Windows.Forms.RadioButton radioButtonCelsius;
        private System.Windows.Forms.Label labelAlarmDelaySeconds;
        private System.Windows.Forms.NumericUpDown numericUpDownAlarmDelay;
        private System.Windows.Forms.Label labelAlarmDelay;
        private System.Windows.Forms.TabPage tabPageDisplay;
        private System.Windows.Forms.CheckBox checkBoxDisableLED;
        private System.Windows.Forms.CheckBox checkBoxShowMinMaxPast24;
        private System.Windows.Forms.CheckBox checkBoxShowMinMax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDisplay;
        private System.Windows.Forms.TabPage tabPageBoomerang;
        private System.Windows.Forms.Button buttonAddBoomerangMail;
        private System.Windows.Forms.TextBox textBoxBoomerangMailAdress;
        private System.Windows.Forms.ListBox listBoxBoomerangEmails;
        private System.Windows.Forms.CheckBox checkBoxBoomerang;
        private System.Windows.Forms.CheckBox checkBoxBoomerangAlarmLevel;
        private System.Windows.Forms.TextBox textBoxBoomerangComment;
        private System.Windows.Forms.TextBox textBoxBoomerangGeneratedBy;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton radioButtonBoomerangFahrenheit;
        private System.Windows.Forms.RadioButton radioButtonBoomerangCelsius;
        private System.Windows.Forms.Label labelBoomerangTemperatureUnit;
        private System.Windows.Forms.Label labelTemperatureUnit;
        private System.Windows.Forms.Label labelBatteryLevel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelFWVersion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkBoxTimerRun;
        private System.Windows.Forms.CheckBox checkBoxPushToRun;
        private System.Windows.Forms.Label labelMemorySize;
        private System.Windows.Forms.ComboBox comboBoxMemorySize;
        protected System.Windows.Forms.Label labelInterval;
        protected System.Windows.Forms.Label labelAveragingPointsType;
        protected System.Windows.Forms.Label labelAverangingPoints;
        protected System.Windows.Forms.NumericUpDown numericUpDownAveragingPoints;
        protected System.Windows.Forms.TabControl tabControlSetup;
        protected System.Windows.Forms.CheckBox checkBoxStopOnSensorDisconnects;
        protected System.Windows.Forms.CheckBox cbDebug;
        protected System.Windows.Forms.TabPage tabPageProperties;
        protected System.Windows.Forms.CheckBox checkBoxDeepSleepMode;
    }
}
