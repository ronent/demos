﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Base.Devices;
using Base.Devices.Types;

namespace API_UI.Forms.UserControls.Devices
{
    /// <summary>
    /// This form is to get windows forms designer to render a form that implements an abstract base class
    /// </summary>
    public partial class UserControlLoggerMiddleV2 : UserControlLoggerV2
    {
        public UserControlLoggerMiddleV2(GenericLoggerV2 logger)
            :base(logger)
        {
            InitializeComponent();
        }

        public UserControlLoggerMiddleV2()
        {
            InitializeComponent();
        }
    }
}
