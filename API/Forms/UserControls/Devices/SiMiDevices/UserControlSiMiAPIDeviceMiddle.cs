﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SiMiAPI.Devices;

namespace API_UI.Forms.UserControls.Devices.SiMiDevices
{
    public partial class UserControlSiMiAPIDeviceMiddle : UserControlSiMiAPIDevice
    {
        public UserControlSiMiAPIDeviceMiddle(GenericSiMiLogLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

        public UserControlSiMiAPIDeviceMiddle()
        {
            InitializeComponent();
        }
    }
}
