﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SiMiDryIce.DataStructures;
using SiMiDryIce.Devices;
using SiMiBase.DataStructures.TSetup;
using SiMiAPI.DataStructures.ESetup;
using Base.Sensors.Management;
using SiMiRH.DataStructures;
using SiMiRH.Devices;

namespace API_UI.Forms.UserControls.Devices.SiMiDevices.SiMi
{
    public partial class UserControlRH : UserControlSiMiAPIDeviceMiddle
    {
        public UserControlRH(SiMiRHLogger device)
            :base(device)
        {
            InitializeComponent();               
        }


        public UserControlRH()
        {
            InitializeComponent();
        }

        protected override Image InitializeLoggerPicture()
        {
            return new Bitmap(API_UI.Properties.Resources.SiMiDryIce);
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            return new SiMiRHSetupConfiguration();
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            var config = setup as SiMiRHSetupConfiguration;

//            config.DeepSleepMode = setupControl.DeepSleepMode;

            config.TemperatureSensor = new TSensorSetup()
            {
                TemperatureEnabled = setupControl.TemperatureEnable,
            };

            if (setupControl.TemperatureAlarmEnable && setupControl.TemperatureEnable)
            {
                config.TemperatureSensor.TemperatureAlarm.Enabled = setupControl.TemperatureAlarmEnable;

                config.TemperatureSensor.TemperatureAlarm.Low = setupControl.TemperatureAlarmLow;
                config.TemperatureSensor.TemperatureAlarm.High = setupControl.TemperatureAlarmHigh;
            }

//            if (setupControl.ExternalEnable)
//            {
//                config.ExternalSensor = new ESensorSetup()
//                {
//                    Type = setupControl.ExternalType,
//                };
//
//                if (setupControl.ExternalType == eSensorType.UserDefined && setupControl.ExternalEnable)
//                {
//                    config.ExternalSensor.UserDefinedSensor = setupControl.ExternalUserDefinedSensor;
//                }
//
//                config.ExternalSensor.Alarm.Enabled = setupControl.ExternalAlarmEnable;
//                config.ExternalSensor.Alarm.Low = setupControl.ExternalAlarmLow;
//                config.ExternalSensor.Alarm.High = setupControl.ExternalAlarmHigh;
//            }
//            else
//            {
//                config.ExternalSensor.Type = eSensorType.None;
//            }

            base.FillSetup(setup);
        }
    }
}
