﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using API_UI.Forms.Calibration;
using Base.OpCodes;
using MicroLogAPI.DataStructures;
using SiMiAPI.DataStructures;
using SiMiAPI.Devices;

namespace API_UI.Forms.UserControls.Devices.SiMiDevices
{
    public partial class UserControlSiMiAPIDevice : UserControlSiMiBaseDeviceMiddle
    {
         new FactoryCalibrationFormV2 calibrationForm { get { return base.calibrationForm as FactoryCalibrationFormV2; } set { base.calibrationForm = value; } }

        public UserControlSiMiAPIDevice(GenericSiMiLogLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

        public UserControlSiMiAPIDevice()
        {
            InitializeComponent();
        }

        protected override void RegisterEvents()
        {
            base.RegisterEvents();

            calibrationForm.OnFactoryReset += m_CalibrationForm_OnFactoryReset;
            calibrationForm.OnFactorySave += m_CalibrationForm_OnFactorySave;
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            var config = setup as SiMiLogSetupConfiguration;

            config.ShowMinMax = setupControl.ShowMinMax;
            config.ShowPast24HMinMax = setupControl.ShowMinMax24h;
            config.StopOnKeyPress = setupControl.StopOnKeyPress;
            config.EnableLEDOnAlarm = setupControl.DisableLEDOnAlarm;
            config.AveragePoints = setupControl.AveragingPoints;

            base.FillSetup(setup);
        }

        protected override void InitializeCalibrationForm()
        {
            calibrationForm = new FactoryCalibrationFormV2(this);
        }

        #region Calibration
        private async void m_CalibrationForm_OnFactoryReset()
        {
            var result = await (Logger as GenericSiMiLogLogger).Functions.RestoreDefaultCalibration();

            if (!result.IsOK)
                updateResult("Error factory reset: " + result.ToString());
            else
                resetFactoryCalibrationCompleted(result);
        }

        private async void m_CalibrationForm_OnFactorySave()
        {
            var result = await (Logger as GenericSiMiLogLogger).Functions.SaveDefaultCalibration();

            if (!result.IsOK)
                updateResult("Error saving factory: " + result.ToString());
            else
                saveFactoryCalibrationCompleted(result);
        }

        #endregion
        #region Results
        private void resetFactoryCalibrationCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error restoring factory calibration: " + result.ToString());
            else
                updateResult("Factory calibration restored successfully");
        }

        private void saveFactoryCalibrationCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error saving factory calibration: " + result.ToString());
            else
                updateResult("Factory calibration saved successfully");
        }

        #endregion
    }
}
