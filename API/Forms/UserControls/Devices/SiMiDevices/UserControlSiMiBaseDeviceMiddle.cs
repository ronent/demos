﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SiMiBase.Devices.Types;

namespace API_UI.Forms.UserControls.Devices.SiMiDevices
{
    public partial class UserControlSiMiBaseDeviceMiddle : UserControlSiMiBaseDevice
    {
         public UserControlSiMiBaseDeviceMiddle(SiMiBaseLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

         public UserControlSiMiBaseDeviceMiddle()
        {
            InitializeComponent();
        }
    }
}
