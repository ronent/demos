﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Base.Devices;
using API_UI.Forms.UserControls.Setup;
using MicroXBase.DataStructures;
using API_UI.Utilities;
using Base.Sensors.Samples;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Types;
using System.Diagnostics;
using System.IO;
using Base.Devices.Features;
using Base.DataStructures.Logger;
using API_UI.Forms.Calibration;
using Base.Devices.Types;
using Base.Sensors.Calibrations;
using Log4Tech;

namespace API_UI.Forms.UserControls.Devices
{
    public abstract partial class UserControlLoggerV2 : SiMiUserControlDeviceMiddle
    {
        #region Fields
        private static SaveAsParameters CSV = new SaveAsParameters("csv", "csv files (*.csv)|*.csv|All files (*.*)|*.*");
        private static SaveAsParameters PDF = new SaveAsParameters("pdf", "pdf files (*.pdf)|*.pdf|All files (*.*)|*.*");

        protected SiMiUserControlSetup setupControl;
        protected CalibrationFormV2 calibrationForm { get; set; }

        private GenericLoggerV2 lastDownload;
        private bool m_DataGridInitialized;
        protected int numberOfSamples;

        #endregion
        #region Constructor

        protected UserControlLoggerV2(GenericLoggerV2 logger)
            :base(logger)
        {
            InitializeComponent();
        }

        protected UserControlLoggerV2()
        {
            InitializeComponent();
        }

        protected override void RegisterEvents()
        {
            base.RegisterEvents();

            Logger.Functions.OnOnlineSampleAdded += Sensors_OnSampleAdded;

            Logger.Functions.OnDownloadProgressReported += Logger_OnDownloadProgressReported;
            Logger.Functions.OnDownloadCompleted += Logger_OnDownloadCompleted;

            calibrationForm.OnResetCalibration += m_CalibrationForm_OnResetCalibration;
            calibrationForm.OnSendCalibration += m_CalibrationForm_OnSendCalibration;
        }

        protected override void Initialize()
        {
            base.Initialize();

            InitializeCalibrationForm();

            if (Logger != null)
            {
                if (Logger.Status.IsRunning)
                    BackColor = System.Drawing.Color.Green;
                else
                    BackColor = Color.Red;
            }

            setupControl = FactoryV2.GetSetupControl(Logger);
            if (setupControl != null)
            {
                setupControl.LoadFromStatus();
                setupControl.Dock = DockStyle.Fill;
                panelSetup.Controls.Add(setupControl);
            }

            m_StatusControl = FactoryV2.GetSetupControl(Logger);
            if (m_StatusControl != null)
            {
                m_StatusControl.DisbleAllControls();
                m_StatusControl.Dock = DockStyle.Fill;
                panelStatus.Controls.Add(m_StatusControl);
            }

            initializeTBLog();

            m_DataGridInitialized = true;
        }

        protected virtual void InitializeCalibrationForm()
        {
            calibrationForm = new CalibrationFormV2(this);
        }

        #endregion
        #region Properties
        public GenericLoggerV2 Logger { get { return Device as GenericLoggerV2;  } }

        #endregion
        #region Methods
        private void Sensors_OnSampleAdded(object sender, SampleArrivedV2EventArgs e)
        {
            if (!m_DataGridInitialized)
            {
                initializeTBLog();
                m_DataGridInitialized = true;
            }

            addSample(e.Sample, e.Name);
        }

        protected virtual void addSample(SampleV2 sample, string name)
        {
            updateLog(name + ": " + sample.ToString());
            numberOfSamples++;
            updateNumberOfSamples(numberOfSamples);
        }

        public override void Close()
        {
            try
            {
                setupControl.Close();
            }
            catch { }

            base.Close();
        }

        protected override IEnumerable<MenuItem> GetFeaturesMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemGenerateCSV = new MenuItem("Generate CSV");
            menuItemGenerateCSV.Click += menuItemGenerateCSV_Click;
            items.Add(menuItemGenerateCSV);

            items.AddRange(base.GetFeaturesMenuItems());
            return items;
        }

        protected override IEnumerable<MenuItem> GetDeviceMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemRun = new MenuItem("Run");
            menuItemRun.Click += menuItemRun_Click;
            items.Add(menuItemRun);

            MenuItem menuItemStop = new MenuItem("Stop");
            menuItemStop.Click += menuItemStop_Click;
            items.Add(menuItemStop);

            MenuItem menuItemDownload = new MenuItem("Download");
            menuItemDownload.Click += menuItemDownload_Click;
            items.Add(menuItemDownload);

            MenuItem menuItemCancelDownload = new MenuItem("Cancel Download");
            menuItemCancelDownload.Click += menuItemCancelDownload_Click;
            items.Add(menuItemCancelDownload);

            MenuItem menuItemCalibration = new MenuItem("Calibration");
            menuItemCalibration.Click += menuItemCalibration_Click;
            items.Add(menuItemCalibration);

            foreach (MenuItem item in items)
            {
                item.Visible = false;
            }

            foreach (var item in Device.Functions.FunctionsState)
            {
                switch (item.Function)
                {
                    case eDeviceFunction.None:
                        break;
                    case eDeviceFunction.TurnOff:
                        break;
                    case eDeviceFunction.UpdateFirmware:
                        break;
                    case eDeviceFunction.CancelUpdateFirmware:
                        break;
                    case eDeviceFunction.Run:
                        SetMenuItem(menuItemRun, item);
                        break;
                    case eDeviceFunction.Stop:
                        SetMenuItem(menuItemStop, item);
                        break;
                    case eDeviceFunction.Download:
                        SetMenuItem(menuItemDownload, item);
                        break;
                    case eDeviceFunction.CancelDownload:
                        SetMenuItem(menuItemCancelDownload, item);
                        break;
                    case eDeviceFunction.Calibrate:
                        SetMenuItem(menuItemCalibration, item);
                        break;
                    case eDeviceFunction.SaveDefaultCalibration:
                        break;
                    case eDeviceFunction.LoadDefaultCalibration:
                        break;
                    case eDeviceFunction.Setup:
                        break;
                    case eDeviceFunction.DebugMode:
                        break;
                    case eDeviceFunction.MarkTimeStamp:
                        break;
                    case eDeviceFunction.DeepSleep:
                        break;
                    default:
                        break;
                }
            }

            items.AddRange(base.GetDeviceMenuItems());
            return items;
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            var config = setup as BaseLoggerSetupConfiguration;

            if (config.Interval == default(TimeSpan))
                config.Interval = new TimeSpan(0, 0, Convert.ToInt32(setupControl.Interval));

            config.SerialNumber = setupControl.SerialNumber;
            config.Comment = setupControl.Comment;
            config.FahrenheitMode = setupControl.Fahrenheit;
        }

        #endregion
        #region Logger Functions
        protected virtual void Logger_OnDownloadCompleted(object sender, DownloadCompletedArgsV2 args)
        {
            lastDownload = args.Logger;

            initializeTBLog();

            var sb = new StringBuilder();
            var sensors = args.Logger.Sensors.GetAllEnabled().ToList(); ;

            var sensorNumber = sensors.Count;

            if (sensorNumber == 0)
                return;

            for (int i = 0; i < sensors[0].Samples.Count; i++)
            {
                for (int j = 0; j < sensorNumber; j++)
                {
                    var sample = sensors[j].Samples[i];
                    string sensorName = sensors[j].Name;

                    sb.AppendLine(GetSampleString(sample, sensorName));
                }
            }

            UpdateUIOfDownload(sb);
        }

        protected virtual string GetSampleString(SampleV2 sample, string sensorName)
        {
            //if (!sample.IsDummy)
                numberOfSamples++;

            return sensorName + ": " + sample;
        }

        protected virtual void UpdateUIOfDownload(StringBuilder sb)
        {
            updateNumberOfSamples(numberOfSamples);
            updateLog(sb.ToString());
        }

        void Logger_OnDownloadProgressReported(object sender, ProgressReportEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { Logger_OnDownloadProgressReported(sender, e); }));
            }
            else
            {
                switch (e.Stage)
                {
                    case eStage.Started:
                        updateResult("Download started");
                        break;
                    case eStage.Queued:
                        updateResult("Download queued");
                        break;
                    case eStage.Running:
                        updateProgressBar(e.Percent);
                        break;
                    default:
                        break;
                }
            }
        }

        protected override void OnStatusReceived()
        {
            base.OnStatusReceived();

            if (Logger.Status.IsRunning)
                m_DataGridInitialized = true;
            else
                m_DataGridInitialized = false;
        }

        #endregion
        #region Calibration Events
        private void m_CalibrationForm_OnSendCalibration(CalibrationConfigurationV2 config)
        {
            var result = Logger.Functions.SendCalibration(config).Result;

            if (!result.IsOK)
                updateResult("Error sending calibration: " + result);
            else
                sendCalibrtationCompleted(result);
        }

        private void m_CalibrationForm_OnResetCalibration()
        {
            var result = Logger.Functions.ResetCalibration().Result;

            if (!result.IsOK)
                updateResult("Error reseting calibration: " + result);
            else
                resetCalibrationCompleted(result);
        }

        #endregion
        #region Menu Functions
        internal void Download()
        {
            ResetProgressBar();
            var result = Logger.Functions.Download().Result;

            downloadCompleted(result);
        }

        private void menuItemCancelDownload_Click(object sender, EventArgs e)
        {
            //var result = await Logger.Functions.CancelDownload();
            //resetProgressBar();

            //if (!result.IsOK)
            //    updateResult("Error canceling download: " + result.ToString());
        }

        private async void menuItemRun_Click(object sender, EventArgs e)
        {
            initializeTBLog();

            var result = await Logger.Functions.Run();

            if (!result.IsOK)
                updateResult("Error running logger: " + result.ToString());
            else
            {
                runCompleted(result);
            }
        }

        private async void menuItemStop_Click(object sender, EventArgs e)
        {
            var result = await Logger.Functions.Stop();

            if (!result.IsOK)
                updateResult("Error stopping logger: " + result.ToString());
            else
            {
                ResetProgressBar();
                stopCompleted(result);
            }
        }

        protected virtual void menuItemCalibration_Click(object sender, EventArgs e)
        {

        }

        private void menuItemDownload_Click(object sender, EventArgs e)
        {
            Download();
        }

        #endregion
        #region Results
        private void stopCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error stopping logger: " + result.ToString());
            else
            {
                updateResult("Logger stopped");
                ChangeBackColor(Color.Red);
            }
        }

        private void runCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error running logger: " + result.ToString());
            else
            {
                updateResult("Logger started running");
                ChangeBackColor(Color.Green);
            }
        }

        private void downloadCompleted(Result result)
        {
            if (!result.IsOK)
                if (result.Value == eResult.CANCELED)
                    updateResult("Download canceled");
                else
                    updateResult("Error downloading: " + result.ToString());
            else
                updateResult("Download completed successfully");
        }

        private void sendCalibrtationCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error sending calibration: " + result.ToString());
            else
                updateResult("Logger calibrated successfully");
        }

        private void resetCalibrationCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error reseting calibration: " + result.ToString());
            else
                updateResult("Calibration reset successfully");
        }

        #endregion
        #region UI
        void menuItemGenerateCSV_Click(object sender, EventArgs e)
        {
            if (lastDownload == null)
                return;

            if (lastDownload.Sensors.HasData && !Logger.Functions.IsDownloading)
            {
                string path = getPathFromUser(CSV);
                if (path != string.Empty)
                    generateCSV(path);
            }
        }
                protected void updateNumberOfSamples(int num)
        {
            try
            {
                if (lblNumberOfSamples.IsDisposed)
                    return;

                if (lblNumberOfSamples.InvokeRequired)
                    lblNumberOfSamples.Invoke(new MethodInvoker(delegate { updateNumberOfSamples(num); }));
                else
                    lblNumberOfSamples.Text = num.ToString();
            }
            catch { }
        }

        protected void updateLog(string msg)
        {
            try
            {
                if (tbLog.IsDisposed)
                    return;
              
                if (tbLog.InvokeRequired)
                    tbLog.Invoke(new MethodInvoker(delegate { updateLog(msg); }));
                else
                    tbLog.AppendText(msg + Environment.NewLine);
            }
            catch { }
        }

        private void initializeTBLog()
        {
            if (tbLog.InvokeRequired)
            {
                tbLog.Invoke(new MethodInvoker(delegate() { initializeTBLog(); }));
            }
            else
            {
                resetCounters();
                tbLog.Clear();
            }
        }

        protected virtual void resetCounters()
        {
            updateNumberOfSamples(numberOfSamples = 0);
        }

        #endregion
        #region CSV & PDF
       
        private void generateCSV(string path)
        {
            try
            {
                var myString = new StringBuilder("Logs,\r\n");
                myString.Append("Date ,Time,");
                var sortedSamples = new List<List<SampleV2>>();

                foreach (var sensor in lastDownload.Sensors.EnabledSensors)
                {
                    sortedSamples.Add(sensor.Samples);

                    myString.Append(sensor.Name + " (" + sensor.Unit.Name + "),");
                }

                addSamplesToFile(myString, sortedSamples);

                using (FileStream fs = new FileStream(path, FileMode.CreateNew))
                {
                    fs.Write(Encoding.UTF8.GetBytes(myString.ToString()), 0, myString.ToString().Length);
                    fs.Flush();
                }

                Process.Start(new ProcessStartInfo(path));
            }
            catch (Exception ex)
            {
                throw new Exception("Error in generateCSV", ex);
            }
        }

        private static void addSamplesToFile(StringBuilder myString, List<List<SampleV2>> sortedSamples)
        {
            myString.Append("\r\n");

            // loop throw number of logs - row
            for (int i = 0; i < sortedSamples[0].Count; i++)
            {
                myString.Append(sortedSamples[0][i].Date.ToString("dd/MM/yyyy") + "," + sortedSamples[0][i].Date.ToString("HH:mm:ss") + ",");

                // loop throw samples on each sensor - each row
                for (int j = 0; j < sortedSamples.Count; j++)
                {
                    //if (sortedSamples[j][i].IsDummy)
                    //    myString.Append("Dummy,");
                    //else
                        myString.Append(sortedSamples[j][i].Value + ",");
                }

                // add a comment if exsist
                //if (sortedSamples[0][i] is TimeStamp)
                  //  myString.Append((sortedSamples[0][i] as TimeStamp).Comment + ",");

                myString.Append("\r\n");
            }
        }

        private string getPathFromUser(SaveAsParameters parameters)
        {
            if (this.InvokeRequired)
            {
                return (string)Invoke(new Func<string>(() => getPathFromUser(parameters)));
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Save As..";
                saveFileDialog.DefaultExt = parameters.DefaultExt;
                saveFileDialog.Filter = parameters.Filter;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFileDialog.FileName))
                        File.Delete(saveFileDialog.FileName);
                }
                else
                    saveFileDialog.FileName = string.Empty;

                return saveFileDialog.FileName;
            }
        }

        #endregion
    }
}