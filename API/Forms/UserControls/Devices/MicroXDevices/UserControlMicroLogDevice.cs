﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MicroLogAPI.Devices;
using MicroLogAPI.DataStructures;
using API_UI.Forms.Calibration;
using Base.OpCodes;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices
{
    public partial class UserControlMicroLogDevice : UserControlMicroXDeviceMiddle
    {
        new FactoryCalibrationForm calibrationForm { get { return base.calibrationForm as FactoryCalibrationForm; } set { base.calibrationForm = value; } }

        public UserControlMicroLogDevice(GenericMicroLogLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

        public UserControlMicroLogDevice()
        {
            InitializeComponent();
        }

        protected override void RegisterEvents()
        {
            base.RegisterEvents();

            calibrationForm.OnFactoryReset += m_CalibrationForm_OnFactoryReset;
            calibrationForm.OnFactorySave += m_CalibrationForm_OnFactorySave;
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            MicroLogSetupConfiguration config = setup as MicroLogSetupConfiguration;

            config.MemorySize = setupControl.MemorySize;
            config.LCDConfiguration = setupControl.LCDConfiguration;
            config.ShowMinMax = setupControl.ShowMinMax;
            config.ShowPast24HMinMax = setupControl.ShowMinMax24h;
            config.StopOnKeyPress = setupControl.StopOnKeyPress;
            config.EnableLEDOnAlarm = setupControl.DisableLEDOnAlarm;
            config.AveragePoints = setupControl.AveragingPoints;

            base.FillSetup(setup);
        }

        protected override void InitializeCalibrationForm()
        {
            calibrationForm = new FactoryCalibrationForm(this);
        }

        #region Calibration
        private async void m_CalibrationForm_OnFactoryReset()
        {
            var result = await (Logger as GenericMicroLogLogger).Functions.RestoreDefaultCalibration();

            if (!result.IsOK)
                updateResult("Error factory reset: " + result.ToString());
            else
                resetFactoryCalibrationCompleted(result);
        }

        private async void m_CalibrationForm_OnFactorySave()
        {
            var result = await (Logger as GenericMicroLogLogger).Functions.SaveDefaultCalibration();

            if (!result.IsOK)
                updateResult("Error saving factory: " + result.ToString());
            else
                saveFactoryCalibrationCompleted(result);
        }

        #endregion
        #region Results
        private void resetFactoryCalibrationCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error restoring factory calibration: " + result.ToString());
            else
                updateResult("Factory calibration restored successfully");
        }

        private void saveFactoryCalibrationCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error saving factory calibration: " + result.ToString());
            else
                updateResult("Factory calibration saved successfully");
        }

        #endregion
    }
}
