﻿using API_UI.Forms.Calibration;
using API_UI.Forms.UserControls.Setup;
using API_UI.Utilities;
using Base.Devices;
using Base.Devices.Features;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices
{
    public abstract partial class UserControlMicroXDevice : UserControlLoggerMiddle
    {
        #region Members
        new public MicroXLogger Logger { get { return Device as MicroXLogger; } }

        protected int numberOfTS;

        #endregion
        #region Constructor
        public UserControlMicroXDevice(MicroXLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

        public UserControlMicroXDevice()
        {
            InitializeComponent();
        }

        #endregion
        #region Override Methods
        protected override IEnumerable<MenuItem> GetFeaturesMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemGenerateBoomerangReport = new MenuItem("Generate Boomerang Report");
            menuItemGenerateBoomerangReport.Click += menuItemGenerateBoomerangReport_Click;
            items.Add(menuItemGenerateBoomerangReport);

            items.AddRange(base.GetFeaturesMenuItems());
            return items;
        }

        protected override IEnumerable<MenuItem> GetDeviceMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemMarkTimeStamp = new MenuItem("Mark Time Stamp");
            menuItemMarkTimeStamp.Click += menuItemMarkTimeStamp_Click;
            items.Add(menuItemMarkTimeStamp);

            MenuItem menuItemDeepSleep = new MenuItem("Deep Sleep");
            menuItemDeepSleep.Click += menuItemDeepSleep_Click;
            items.Add(menuItemDeepSleep);

            foreach (MenuItem item in items)
            {
                item.Visible = false;
            }

            foreach (var item in Device.Functions.FunctionsState)
            {
                switch (item.Function)
                {
                    case eDeviceFunction.None:
                        break;
                    case eDeviceFunction.TurnOff:
                        break;
                    case eDeviceFunction.UpdateFirmware:
                        break;
                    case eDeviceFunction.CancelUpdateFirmware:
                        break;
                    case eDeviceFunction.Run:
                        break;
                    case eDeviceFunction.Stop:
                        break;
                    case eDeviceFunction.Download:
                        break;
                    case eDeviceFunction.CancelDownload:
                        break;
                    case eDeviceFunction.Calibrate:
                        break;
                    case eDeviceFunction.SaveDefaultCalibration:
                        break;
                    case eDeviceFunction.LoadDefaultCalibration:
                        break;
                    case eDeviceFunction.Setup:
                        break;
                    case eDeviceFunction.DebugMode:
                        break;
                    case eDeviceFunction.MarkTimeStamp:
                        SetMenuItem(menuItemMarkTimeStamp, item);
                        break;
                    case eDeviceFunction.DeepSleep:
                        SetMenuItem(menuItemDeepSleep, item);
                        break;
                    default:
                        break;
                }
            }

            items.AddRange(base.GetDeviceMenuItems());
            return items;
        }

        public override void Close()
        {
            try
            {
                calibrationForm.Close();
            }
            catch { }

            base.Close();
        }
        #endregion
        #region Menu Functions
        protected override void menuItemCalibration_Click(object sender, EventArgs e)
        {
            calibrationForm.Initialize();
            calibrationForm.ShowDialog();
        }

        private void menuItemGenerateBoomerangReport_Click(object sender, EventArgs e)
        {
           
        }

        private async void menuItemDeepSleep_Click(object sender, EventArgs e)
        {
            PicoLiteLogger pico = Logger as PicoLiteLogger;
            var result = await pico.Functions.DeepSleep();

            if (!result.IsOK)
                updateResult("Error entering into deep sleep: " + result.ToString());
            else
            {
                updateResult("Disconnect logger to enter into Deep sleep mode.");
            }
        }

        private async void menuItemMarkTimeStamp_Click(object sender, EventArgs e)
        {
            if (Logger is PicoLiteLogger)
            {
                var result = await (Logger as PicoLiteLogger).Functions.MarkTimeStamp();

                if (!result.IsOK)
                    updateResult("Error marking time stamp: " + result.ToString());
            }
        }

        #endregion
        #region Logger Methods
        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            MicroXSetupConfiguration config = setup as MicroXSetupConfiguration;

            config.BoomerangEnabled = setupControl.BoomerangEnable;
            config.Boomerang.Author = setupControl.BoomerangGeneratedBy;
            config.Boomerang.CelsiusMode = setupControl.BoomerangUnit;
            config.Boomerang.Comment = setupControl.BoomerangComment;
            config.Boomerang.Contacts = setupControl.BoomerangEmailAdresses;
            config.Boomerang.DisplayAlarmLevels = setupControl.BoomerangDisplayAlarm;
            config.CyclicMode = setupControl.Cyclic;
            config.PushToRunMode = setupControl.PushToRun;
            config.TestMode = setupControl.DebugMode;
            config.TimerRunEnabled = setupControl.TimerRun;
            config.TimerStart = setupControl.TimerRunStart;

            base.FillSetup(setup);
        }

        protected override void UpdateUIOfDownload(StringBuilder sb)
        {
            base.UpdateUIOfDownload(sb);
            updateNumberOfTS(numberOfTS);
        }

        protected override void addSample(Sample sample, string name)
        {
            if (sample is TimeStamp)
            {
                updateLog("T.S " + name + ": " + sample.ToString());
                numberOfTS++;
                updateNumberOfTS(numberOfTS);
            }
            else
            {
                base.addSample(sample, name);
            }
        }

        protected override string GetSampleString(Sample sample, string sensorName)
        {
            if (sample is TimeStamp)
            {
                if (!sample.IsDummy)
                    numberOfTS++;

                return "T.S " + sensorName + ": " + sample.ToString();
            }
            else
            {
                return base.GetSampleString(sample, sensorName);
            }
        }

        #endregion
        #region UI
        protected override void resetCounters()
        {
            base.resetCounters();
            updateNumberOfTS(numberOfTS = 0);
        }

        protected void updateNumberOfTS(int num)
        {
            try
            {
                if (lblNumberOfTimeStamps.IsDisposed)
                    return;

                if (lblNumberOfTimeStamps.InvokeRequired)
                    lblNumberOfTimeStamps.Invoke(new MethodInvoker(delegate { updateNumberOfTS(num); }));
                else
                    lblNumberOfTimeStamps.Text = num.ToString();
            }
            catch { }
        }

        #endregion
    }
}
