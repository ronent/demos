﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MicroXBase.Devices.Types;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices
{
    public partial class UserControlMicroXDeviceMiddle : UserControlMicroXDevice
    {
        public UserControlMicroXDeviceMiddle(MicroXLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

        public UserControlMicroXDeviceMiddle()
        {
            InitializeComponent();
        }
    }
}
