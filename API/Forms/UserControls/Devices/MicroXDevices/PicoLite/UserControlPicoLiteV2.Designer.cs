﻿namespace API_UI.Forms.UserControls.Devices.MicroXDevices.PicoLite
{
    partial class UserControlPicoLiteV2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumberOfDummies = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNumberOfDummies
            // 
            this.lblNumberOfDummies.AutoSize = true;
            this.lblNumberOfDummies.Location = new System.Drawing.Point(685, 399);
            this.lblNumberOfDummies.Name = "lblNumberOfDummies";
            this.lblNumberOfDummies.Size = new System.Drawing.Size(19, 13);
            this.lblNumberOfDummies.TabIndex = 32;
            this.lblNumberOfDummies.Text = "(0)";
            // 
            // UserControlPicoLiteV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblNumberOfDummies);
            this.Name = "UserControlPicoLiteV2";
            this.Controls.SetChildIndex(this.tbResults, 0);
            this.Controls.SetChildIndex(this.panelSetup, 0);
            this.Controls.SetChildIndex(this.panelStatus, 0);
            this.Controls.SetChildIndex(this.lblNumberOfDummies, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNumberOfDummies;
    }
}
