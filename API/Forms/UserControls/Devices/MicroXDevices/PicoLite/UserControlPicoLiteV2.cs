﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PicoLite.Devices;
using PicoLite.DataStructures.V2;
using API_UI.Forms.UserControls.Setup.PicoLite;
using Base.Sensors.Samples;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices.PicoLite
{
    public partial class UserControlPicoLiteV2 : UserControlPicoLiteV1
    {
        private int numberOfDummies;

        public UserControlPicoLiteV2(PicoLiteLogger device)
            :base(device)
        {
            InitializeComponent();                
        }

        public UserControlPicoLiteV2()
        {
            InitializeComponent();
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            return new PicoLiteV2SetupConfiguration();
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            PicoLiteV2SetupConfiguration config = setup as PicoLiteV2SetupConfiguration;

            if (setupControl.PushToRun)
                config.RunDelay = (setupControl as PicoLiteV2Setup).PushToRunDelay;

            config.StopOnKeyPress = setupControl.StopOnKeyPress;

            base.FillSetup(setup);
        }

        protected override void addSample(Sample sample, string name)
        {
            if (sample is TimeStamp)
            {
                updateLog("T.S " + name + ": " + sample.ToString());
                numberOfTS++;
                updateNumberOfTS(numberOfTS);
            }
            else
            {
                updateLog(name + ": " + sample.ToString());

                if (sample.IsDummy)
                {
                    numberOfDummies++;
                    updateNumberOfDummies(numberOfDummies);
                }
                else
                {
                    numberOfSamples++;
                    updateNumberOfSamples(numberOfSamples);
                }
            }
        }

        protected override void resetCounters()
        {
            base.resetCounters();
            updateNumberOfDummies(numberOfDummies = 0);
        }

        protected override void Logger_OnDownloadCompleted(object sender, Base.OpCodes.Helpers.DownloadCompletedArgs args)
        {
            base.Logger_OnDownloadCompleted(sender, args);
            numberOfDummies = args.Logger.Sensors.GetAllEnabled().ToList()[0].Samples.Count((item) => item.IsDummy);
            updateNumberOfDummies(numberOfDummies);
        }

        protected void updateNumberOfDummies(int num)
        {
            if (lblNumberOfDummies.InvokeRequired)
                lblNumberOfDummies.Invoke(new MethodInvoker(delegate { updateNumberOfDummies(num); }));
            else
                lblNumberOfDummies.Text = string.Format("({0})", num);
        }
    }
}
