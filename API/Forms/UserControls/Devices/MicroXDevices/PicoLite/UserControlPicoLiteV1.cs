﻿using MicroXBase.DataStructures;
using PicoLite.DataStructures.V1;
using PicoLite.DataStructures.V2;
using PicoLite.Devices;
using System;
using System.Drawing;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices.PicoLite
{
    public partial class UserControlPicoLiteV1 : UserControlMicroXDeviceMiddle
    {
        public UserControlPicoLiteV1(PicoLiteLogger device)
            :base(device)
        {
            InitializeComponent();             
        }

        public UserControlPicoLiteV1()
        {
            InitializeComponent();
        }

        protected override Image InitializeLoggerPicture()
        {
            return new Bitmap(API_UI.Properties.Resources.PicoLite);
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            return new PicoLiteV1SetupConfiguration();
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            PicoLiteV1SetupConfiguration config = setup as PicoLiteV1SetupConfiguration;

            config.UtcOffset = getUtcOffset();
            config.TimerRunEnabled = setupControl.TimerRun;
            config.TemperatureSensor.TemperatureEnabled = setupControl.TemperatureEnable;
            config.TemperatureSensor.TemperatureAlarm.Enabled = setupControl.TemperatureAlarmEnable;

            if (setupControl.TemperatureAlarmEnable)
            {
                config.TemperatureSensor.TemperatureAlarm.High = setupControl.TemperatureAlarmHigh;
                config.TemperatureSensor.TemperatureAlarm.Low = setupControl.TemperatureAlarmLow;
            }

            if (!config.TestMode)
                config.Interval = new TimeSpan(0, Convert.ToInt32(setupControl.Interval), 0);   

            base.FillSetup(setup);
        }

        protected sbyte getUtcOffset()
        {
            if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
                return (sbyte)(TimeZoneInfo.Local.BaseUtcOffset.Hours + 1);
            else
                return (sbyte)TimeZoneInfo.Local.BaseUtcOffset.Hours;
        }
    }
}
