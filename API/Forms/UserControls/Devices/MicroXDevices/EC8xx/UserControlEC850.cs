﻿using Base.Sensors.Management;
using MicroLogAPI.DataStructures;
using MicroLogPro2TH.DataStructures;
using MicroLogPro2TH.Devices;
using System.Drawing;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices.EC8xx
{
    public partial class UserControlEC850 : UserControlMicroLogDeviceMiddle
    {
        public UserControlEC850(EC850B16Logger device)
            :base(device)
        {
            InitializeComponent();      
        }

        public UserControlEC850()
        {
            InitializeComponent();
        }

        protected override Image InitializeLoggerPicture()
        {
            return new Bitmap(API_UI.Properties.Resources.EC850);
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            return new EC850B16SetupConfiguration();
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            EC850B16SetupConfiguration config = setup as EC850B16SetupConfiguration;

            config.DeepSleepMode = setupControl.DeepSleepMode;

            config.FixedSensors = new THDSensorSetup()
            {
                TemperatureEnabled = setupControl.TemperatureEnable,
                HumidityEnabled = setupControl.HumidityEnable,
                DewPointEnabled = setupControl.DewPointEnable
            };

            if (setupControl.TemperatureAlarmEnable && setupControl.TemperatureEnable)
            {
                config.FixedSensors.TemperatureAlarm.Enabled = setupControl.TemperatureAlarmEnable;

                config.FixedSensors.TemperatureAlarm.Low = setupControl.TemperatureAlarmLow;
                config.FixedSensors.TemperatureAlarm.High = setupControl.TemperatureAlarmHigh;
            }

            if (setupControl.HumidityAlarmEnable && setupControl.HumidityEnable)
            {
                config.FixedSensors.HumidityAlarm.Enabled = setupControl.HumidityAlarmEnable;

                config.FixedSensors.HumidityAlarm.Low = setupControl.HumidityAlarmLow;
                config.FixedSensors.HumidityAlarm.High = setupControl.HumidityAlarmHigh;
            }

            if (setupControl.DewPointAlarmEnable && setupControl.DewPointEnable)
            {
                config.FixedSensors.DewPointAlarm.Enabled = setupControl.DewPointAlarmEnable;

                config.FixedSensors.DewPointAlarm.Low = setupControl.DewPointAlarmLow;
                config.FixedSensors.DewPointAlarm.High = setupControl.DewPointAlarmHigh;
            }

            if (setupControl.ExternalEnable)
            {
                config.ExternalSensor = new ESensorSetup()
                {
                    Type = setupControl.ExternalType,
                };

                if (setupControl.ExternalType == eSensorType.UserDefined && setupControl.ExternalEnable)
                {
                    config.ExternalSensor.UserDefinedSensor = setupControl.ExternalUserDefinedSensor;
                }

                config.ExternalSensor.Alarm.Enabled = setupControl.ExternalAlarmEnable;
                config.ExternalSensor.Alarm.Low = setupControl.ExternalAlarmLow;
                config.ExternalSensor.Alarm.High = setupControl.ExternalAlarmHigh;
            }
            else
            {
                config.ExternalSensor.Type = eSensorType.None;
            }

            base.FillSetup(setup);
        }
    }
}
