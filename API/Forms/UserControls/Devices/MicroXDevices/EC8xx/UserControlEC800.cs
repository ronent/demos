﻿using Base.Sensors.Management;
using MicroLogAPI.DataStructures;
using MicroLogPro2T.DataStructures;
using MicroLogPro2T.Devices;
using MicroLogPro2TH.Devices;
using MicroXBase.DataStructures;
using System.Drawing;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices.EC8xx
{
    public partial class UserControlEC800 : UserControlMicroLogDeviceMiddle
    {
        public UserControlEC800(EC800B16Logger device)
            :base(device)
        {
            InitializeComponent();               
        }

        internal UserControlEC800(EC850B16Logger device)
            :base(device)
        {
            InitializeComponent();               
        }

        public UserControlEC800()
        {
            InitializeComponent();
        }

        protected override Image InitializeLoggerPicture()
        {
            return new Bitmap(API_UI.Properties.Resources.EC800);
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            return new EC800B16SetupConfiguration();
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            EC800B16SetupConfiguration config = setup as EC800B16SetupConfiguration;

            config.DeepSleepMode = setupControl.DeepSleepMode;

            config.TemperatureSensor = new TSensorSetup()
            {
                TemperatureEnabled = setupControl.TemperatureEnable,
            };

            if (setupControl.TemperatureAlarmEnable && setupControl.TemperatureEnable)
            {
                config.TemperatureSensor.TemperatureAlarm.Enabled = setupControl.TemperatureAlarmEnable;

                config.TemperatureSensor.TemperatureAlarm.Low = setupControl.TemperatureAlarmLow;
                config.TemperatureSensor.TemperatureAlarm.High = setupControl.TemperatureAlarmHigh;
            }

            if (setupControl.ExternalEnable)
            {
                config.ExternalSensor = new ESensorSetup()
                {
                    Type = setupControl.ExternalType,
                };

                if (setupControl.ExternalType == eSensorType.UserDefined && setupControl.ExternalEnable)
                {
                    config.ExternalSensor.UserDefinedSensor = setupControl.ExternalUserDefinedSensor;
                }

                config.ExternalSensor.Alarm.Enabled = setupControl.ExternalAlarmEnable;
                config.ExternalSensor.Alarm.Low = setupControl.ExternalAlarmLow;
                config.ExternalSensor.Alarm.High = setupControl.ExternalAlarmHigh;
            }
            else
            {
                config.ExternalSensor.Type = eSensorType.None;
            }

            base.FillSetup(setup);
        }
    }
}
