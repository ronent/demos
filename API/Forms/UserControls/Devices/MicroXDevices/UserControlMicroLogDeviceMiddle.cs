﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MicroLogAPI.Devices;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices
{
    public partial class UserControlMicroLogDeviceMiddle : UserControlMicroLogDevice
    {
        public UserControlMicroLogDeviceMiddle(GenericMicroLogLogger logger)
            :base(logger)
        {
            InitializeComponent();            
        }

        public UserControlMicroLogDeviceMiddle()
        {
            InitializeComponent();
        }
    }
}
