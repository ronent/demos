﻿using Base.Sensors.Management;
using MicroLite2E.DataStructures;
using MicroLite2E.Devices;
using MicroLite2T.DataStructures;
using MicroLite2T.Devices;
using MicroLite2TH.DataStructures;
using MicroLite2TH.Devices;
using MicroLogAPI.DataStructures;
using MicroXBase.DataStructures;
using System.Drawing;

namespace API_UI.Forms.UserControls.Devices.MicroXDevices.ML2
{
    public partial class UserControlML2 : UserControlMicroLogDeviceMiddle
    {
        public UserControlML2(MicroLogAPI.Devices.MicroLiteLogger device)
            :base(device)
        {
            InitializeComponent();    
        }

        public UserControlML2()
        {
            InitializeComponent();
        }

        protected override Image InitializeLoggerPicture()
        {
            Image image = default(Image);

            if (Logger is MicroLite2T.Devices.MicroLite2TLogger || Logger is MicroLite2TH.Devices.MicroLite2THLogger)
            {
                image = new Bitmap(API_UI.Properties.Resources.MicroLiteII);
            }
            else if (Logger is MicroLite2E.Devices.MicroLite2ELogger)
            {
                image = new Bitmap(API_UI.Properties.Resources.MicroLiteII);
            }

            return image;
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            MicroXSetupConfiguration setup = default(MicroXSetupConfiguration);

            if (Logger is MicroLite2TLogger)
                setup = new MicroLite2TSetupConfiguration();
            else if (Logger is MicroLite2THLogger)
                setup = new MicroLite2THSetupConfiguration();
            else if (Logger is MicroLite2ELogger)
                setup = new MicroLite2ESetupConfiguration();

            return setup;
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
            if (setup is MicroLite2TSetupConfiguration)
                fillSetupForML2T(setup as MicroLite2TSetupConfiguration);
            else if (setup is MicroLite2THSetupConfiguration)
                fillSetupForML2TH(setup as MicroLite2THSetupConfiguration);
            else if (setup is MicroLite2ESetupConfiguration)
                fillSetupForML2E(setup as MicroLite2ESetupConfiguration);

            base.FillSetup(setup);
        }

        private void fillSetupForML2E(MicroLite2ESetupConfiguration setup)
        {
            setup.StopOnDisconnect = setupControl.StopOnSensorDisconnects;

            if (setupControl.ExternalEnable)
            {
                setup.ExternalSensor.Type = setupControl.ExternalType;

                if (setupControl.ExternalType == eSensorType.UserDefined && setupControl.ExternalEnable)
                {
                    setup.ExternalSensor.UserDefinedSensor = setupControl.ExternalUserDefinedSensor;
                }

                if (setupControl.ExternalAlarmEnable)
                {
                    setup.ExternalSensor.Alarm.Enabled = setupControl.ExternalAlarmEnable;

                    setup.ExternalSensor.Alarm.Low = setupControl.ExternalAlarmLow;
                    setup.ExternalSensor.Alarm.High = setupControl.ExternalAlarmHigh;
                }
            }
            else
            {
                setup.ExternalSensor.Type = eSensorType.None;
            }
        }

        private void fillSetupForML2TH(MicroLite2THSetupConfiguration setup)
        {
            setup.FixedSensors = new THDSensorSetup()
                {
                    TemperatureEnabled = setupControl.TemperatureEnable,
                    HumidityEnabled = setupControl.HumidityEnable,
                    DewPointEnabled = setupControl.DewPointEnable,
                };

            if (setupControl.TemperatureAlarmEnable && setupControl.TemperatureEnable)
            {
                setup.FixedSensors.TemperatureAlarm.Enabled = setupControl.TemperatureAlarmEnable;

                setup.FixedSensors.TemperatureAlarm.Low = setupControl.TemperatureAlarmLow;
                setup.FixedSensors.TemperatureAlarm.High = setupControl.TemperatureAlarmHigh;
            }

            if (setupControl.HumidityAlarmEnable && setupControl.HumidityEnable)
            {
                setup.FixedSensors.HumidityAlarm.Enabled = setupControl.HumidityAlarmEnable;

                setup.FixedSensors.HumidityAlarm.Low = setupControl.HumidityAlarmLow;
                setup.FixedSensors.HumidityAlarm.High = setupControl.HumidityAlarmHigh;
            }

            if (setupControl.DewPointAlarmEnable && setupControl.DewPointEnable)
            {
                setup.FixedSensors.DewPointAlarm.Enabled = setupControl.DewPointAlarmEnable;

                setup.FixedSensors.DewPointAlarm.Low = setupControl.DewPointAlarmLow;
                setup.FixedSensors.DewPointAlarm.High = setupControl.DewPointAlarmHigh;
            }
        }

        private void fillSetupForML2T(MicroLite2TSetupConfiguration setup)
        {
            setup.TemperatureSensor.TemperatureEnabled = setupControl.TemperatureEnable;
            if (setupControl.TemperatureAlarmEnable && setupControl.TemperatureEnable)
            {
                setup.TemperatureSensor.TemperatureAlarm.Enabled = setupControl.TemperatureAlarmEnable;

                setup.TemperatureSensor.TemperatureAlarm.Low = setupControl.TemperatureAlarmLow;
                setup.TemperatureSensor.TemperatureAlarm.High = setupControl.TemperatureAlarmHigh;
            }
        }
    }
}
