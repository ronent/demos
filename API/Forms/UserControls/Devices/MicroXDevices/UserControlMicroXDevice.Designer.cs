﻿namespace API_UI.Forms.UserControls.Devices.MicroXDevices
{
    partial class UserControlMicroXDevice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumberOfTimeStamps = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(541, 419);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Number Of T.S:";
            // 
            // lblNumberOfTimeStamps
            // 
            this.lblNumberOfTimeStamps.AutoSize = true;
            this.lblNumberOfTimeStamps.Location = new System.Drawing.Point(651, 419);
            this.lblNumberOfTimeStamps.Name = "lblNumberOfTimeStamps";
            this.lblNumberOfTimeStamps.Size = new System.Drawing.Size(13, 13);
            this.lblNumberOfTimeStamps.TabIndex = 39;
            this.lblNumberOfTimeStamps.Text = "0";
            // 
            // UserControlMicroXDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblNumberOfTimeStamps);
            this.Name = "UserControlMicroXDevice";
            this.Controls.SetChildIndex(this.tbResults, 0);
            this.Controls.SetChildIndex(this.panelSetup, 0);
            this.Controls.SetChildIndex(this.panelStatus, 0);
            this.Controls.SetChildIndex(this.lblNumberOfTimeStamps, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNumberOfTimeStamps;
    }
}
