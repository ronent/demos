﻿using Base.Devices;
using System;
using System.Drawing;

namespace API_UI.Forms.UserControls.Devices
{
    /// <summary>
    /// This form is to get windows forms designer to render a form that implements an abstract base class
    /// </summary>
    public partial class SiMiUserControlDeviceMiddle : SiMiUserControlDevice
    {
        public SiMiUserControlDeviceMiddle(GenericDevice device)
            :base(device)
        {
            InitializeComponent();               
        }

        public SiMiUserControlDeviceMiddle()
        {
            InitializeComponent();
        }

        protected override void Initialize()
        {
           
        }

        protected override Image InitializeLoggerPicture()
        {
            return default(Image);
        }

        protected override Base.DataStructures.Device.BaseDeviceSetupConfiguration GenerateSetup()
        {
            return null;
        }

        protected override void FillSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration setup)
        {
           
        }
    }
}
