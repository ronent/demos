﻿using API_UI.Forms;
using API_UI.Forms.UserControls.Setup;
using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using PicoLite.Devices;
using System.Runtime.Serialization.Formatters.Binary;
using API_UI.Forms.DeviceIO;
using Base.Devices.Features;
using Base.Functions.Management;
using Base.DataStructures.Device;

namespace API_UI.Forms.UserControls.Devices
{
    public abstract partial class SiMiUserControlDevice : UserControl
    {
        #region Members
        public GenericDevice Device { get; private set; }
        private DeviceIOForm m_DeviceIOForm;

        protected SiMiUserControlSetup  m_StatusControl;
        private Timer m_Timer;

        #endregion
        #region Constructor and Initializers

        protected SiMiUserControlDevice(GenericDevice device)
        {
            InitializeComponent();
            Device = device;
            m_DeviceIOForm = new DeviceIOForm(device);
        }

        protected SiMiUserControlDevice()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            initialize();
            Initialize();

            if (Device != null)
                RegisterEvents();

            base.OnLoad(e);
        }

        protected virtual void RegisterEvents()
        {
            Device.Functions.OnStatusReceived += Logger_OnStatusReceived;
            Device.Functions.OnFirmwareUpdateProgressReported += Functions_OnFirmwareUpdateProgressReported;
        }

        private void initialize()
        {
            initializeLoggerPicture();

            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User)
                btnFeatures.Visible = false;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["OpenDeviceIO"]))
                showDeviceIO();
        }

        private void initializeLoggerPicture()
        {
            pictureBoxLogger.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBoxLogger.BackgroundImage = InitializeLoggerPicture();
        }

        #endregion
        #region Abstract Methods
        protected abstract void Initialize();
        protected abstract Image InitializeLoggerPicture();
        protected abstract BaseDeviceSetupConfiguration GenerateSetup();
        protected abstract void FillSetup(BaseDeviceSetupConfiguration setup);

        #endregion
        #region Right Click Menu
        private void btnFeatures_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button)sender;
            Point ptLowerLeft = new Point(0, btnSender.Height);
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.AddRange((GetFeaturesMenuItems().ToArray()));

            contextMenu.Show(btnSender, ptLowerLeft, LeftRightAlignment.Right);
        }

        private void pictureBoxLogger_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenu contextMenu = new ContextMenu();
                contextMenu.MenuItems.AddRange(GetDeviceMenuItems().ToArray());

                contextMenu.Show(pictureBoxLogger, e.Location, LeftRightAlignment.Right);
            }
        }

        protected virtual IEnumerable<MenuItem> GetDeviceMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemSetup = new MenuItem("Send Setup");
            menuItemSetup.Click += menuItemSetup_Click;
            items.Add(menuItemSetup);

            MenuItem menuItemUpdateFW = new MenuItem("Update FW");
            menuItemUpdateFW.Click += menuItemUpdateFW_Click;
            items.Add(menuItemUpdateFW);

            MenuItem menuItemCancelUpdateFW = new MenuItem("Cancel Update FW");
            menuItemCancelUpdateFW.Click += menuItemCancelUpdateFW_Click;
            items.Add(menuItemCancelUpdateFW);

            foreach (MenuItem item in items)
            {
                item.Visible = false;
            }

            foreach (var item in Device.Functions.FunctionsState)
            {
                switch (item.Function)
                {
                    case eDeviceFunction.None:
                        break;
                    case eDeviceFunction.TurnOff:
                        break;
                    case eDeviceFunction.UpdateFirmware:
                        SetMenuItem(menuItemUpdateFW, item);
                        break;
                    case eDeviceFunction.CancelUpdateFirmware:
                        SetMenuItem(menuItemCancelUpdateFW, item);
                        break;
                    case eDeviceFunction.Run:
                        break;
                    case eDeviceFunction.Stop:
                        break;
                    case eDeviceFunction.Download:
                        break;
                    case eDeviceFunction.CancelDownload:
                        break;
                    case eDeviceFunction.Calibrate:
                        break;
                    case eDeviceFunction.SaveDefaultCalibration:
                        break;
                    case eDeviceFunction.LoadDefaultCalibration:
                        break;
                    case eDeviceFunction.Setup:
                        SetMenuItem(menuItemSetup, item);
                        break;
                    case eDeviceFunction.DebugMode:
                        break;
                    case eDeviceFunction.MarkTimeStamp:
                        break;
                    case eDeviceFunction.DeepSleep:
                        break;
                    default:
                        break;
                }
            }

            return items;
        }

        protected virtual IEnumerable<MenuItem> GetFeaturesMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemDeviceIO = new MenuItem("Open Device I/O");
            menuItemDeviceIO.Click += menuItemDeviceIO_Click;
            items.Add(menuItemDeviceIO);

            MenuItem menuItemStatus = new MenuItem("Get Status");
            menuItemStatus.Click += menuItemStatus_Click;
            items.Add(menuItemStatus);

            return items;
        }

        private void pictureBoxLogger_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void pictureBoxLogger_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void menuItemDeviceIO_Click(object sender, EventArgs e)
        {
            showDeviceIO();
        }

        #endregion
        #region Device Functions
        void Logger_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate() { Logger_OnStatusReceived(sender, e); }));
            }
            else
            {
                OnStatusReceived();
            }
        }

        protected virtual void OnStatusReceived()
        {
            try
            {
                ResetProgressBar();
                m_StatusControl.LoadFromStatus();

                ToolTip toolTip1 = new ToolTip();
                toolTip1.BackColor = Color.Yellow;
                toolTip1.IsBalloon = true;
                toolTip1.UseFading = true;

                toolTip1.Show("New Status", panelStatus, 1500);
            }
            catch { }
        }

        void Functions_OnFirmwareUpdateProgressReported(object sender, ProgressReportEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { Functions_OnFirmwareUpdateProgressReported(sender, e); }));
            }
            else
            {
                switch (e.Stage)
                {
                    case eStage.Started:
                        updateResult("FW Update started");
                        break;
                    case eStage.Queued:
                        updateResult("FW Update queued");
                        break;
                    case eStage.Running:
                        updateProgressBar(e.Percent);
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion
        #region Menu Functions
        private async void menuItemSetup_Click(object sender, EventArgs e)
        {
            BaseDeviceSetupConfiguration setup = GenerateSetup();
            if (setup != null)
            {
                FillSetup(setup);

                var result = await Device.Functions.SendSetup(setup);

                if (!result.IsOK)
                    updateResult("Error sending setup: " + result.ToString());
                else
                    setupCompleted(result);
            }
            else
            {
                updateResult("Error sending setup: " + Result.UNSUPPORTED.ToString());
            }
        }

        private void menuItemStatus_Click(object sender, EventArgs e)
        {
            var result = Device.Functions.GetStatus().Result;

            if (!result.IsOK)
                updateResult("Error getting status: " + result);
            else
            {
                updateResult("New status arrived.");
            }
        }

        private async void menuItemUpdateFW_Click(object sender, EventArgs e)
        {
            var result = await Device.Functions.UploadFirmware();

            if (!result.IsOK)
                updateResult("Error updating firmware: " + result);
            else
                fwUpdateCompleted(result);
        }

        private  void menuItemCancelUpdateFW_Click(object sender, EventArgs e)
        {
            
        }

        #endregion
        #region Results
        protected void ChangeBackColor(Color color)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { ChangeBackColor(color); }));
            }
            else
            {
                BackColor = color;
            }
        }

        protected void updateResult(string msg)
        {
            try
            {
                if (tbResults.IsDisposed)
                    return;

                if (tbResults.InvokeRequired)
                    tbResults.Invoke(new MethodInvoker(delegate { updateResult(msg); }));
                else
                    tbResults.AppendText(msg + Environment.NewLine);
            }
            catch { }
        }

        private void setupCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error sending setup: " + result.ToString());
            else
                updateResult("Setup sent successfully");
        }


        private void fwUpdateCompleted(Result result)
        {
            if (!result.IsOK)
                if (result.Value == eResult.CANCELED)
                    updateResult("Firmware update canceled");
                else
                    updateResult("Error updating firmware: " + result.ToString());
            else
                updateResult("Firmware update finished successfully");
        }

        #endregion
        #region Methods
        public virtual void Close()
        {
            try
            {
                if (m_DeviceIOForm != null)
                    m_DeviceIOForm.Close();

                if (m_Timer != null)
                    m_Timer.Dispose();
            }
            catch { }

            Dispose();
        }

        protected void updateProgressBar(int p)
        {
            if (progressBar.InvokeRequired)
            {
                progressBar.Invoke(new MethodInvoker(delegate() { updateProgressBar(p); }));
            }
            else
            {
                if (p >= 100)
                {
                    if (m_Timer == null)
                    {
                        m_Timer = new Timer();
                        m_Timer.Interval = 3000;
                        m_Timer.Tick += m_Timer_Tick;
                        m_Timer.Start();
                    }
                }
                else
                {
                    progressBar.Value = p;
                }
            }
        }

        void m_Timer_Tick(object sender, EventArgs e)
        {
            ResetProgressBar();
        }

        protected void ResetProgressBar()
        {
            if (m_Timer != null)
            {
                m_Timer.Enabled = false;
                m_Timer.Stop();

                m_Timer = null;
            }

            updateProgressBar(0);
        }

        protected static void SetMenuItem(MenuItem menuItem, AvailableFunction item)
        {
            menuItem.Enabled = item.Enabled;
            menuItem.Visible = item.Visible;
        }

        protected void showDeviceIO()
        {
            if (!m_DeviceIOForm.Visible)
                m_DeviceIOForm.Show();
        }

        #endregion
        #region Static Methods
        public static List<Sample> GetSortedListByDate(GenericSensor i_Sensor)
        {
            return GetSortedListByDate(i_Sensor.Samples);
        }

        public static List<Sample> GetSortedListByDate(List<Sample> list)
        {
            return list.OrderBy(sample => sample.Date).ToList();
        }

        public static string DoubleFormat(double i_Num)
        {
            return (String.Format(numberFormat(), i_Num));
        }

        public static string DecimalFormat(decimal i_Num)
        {
            return (String.Format(numberFormat(), i_Num));
        }

        private static string numberFormat()
        {
            return "{0:0.##}";
        }

        #endregion
    }
}