﻿namespace API_UI.Forms.UserControls.Devices
{
    partial class UserControlDevice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxLogger = new System.Windows.Forms.PictureBox();
            this.btnFeatures = new System.Windows.Forms.Button();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.panelSetup = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogger)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxLogger
            // 
            this.pictureBoxLogger.Location = new System.Drawing.Point(348, 301);
            this.pictureBoxLogger.Name = "pictureBoxLogger";
            this.pictureBoxLogger.Size = new System.Drawing.Size(160, 160);
            this.pictureBoxLogger.TabIndex = 23;
            this.pictureBoxLogger.TabStop = false;
            this.pictureBoxLogger.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxLogger_MouseClick);
            this.pictureBoxLogger.MouseEnter += new System.EventHandler(this.pictureBoxLogger_MouseEnter);
            this.pictureBoxLogger.MouseLeave += new System.EventHandler(this.pictureBoxLogger_MouseLeave);
            // 
            // btnFeatures
            // 
            this.btnFeatures.Location = new System.Drawing.Point(541, 331);
            this.btnFeatures.Name = "btnFeatures";
            this.btnFeatures.Size = new System.Drawing.Size(120, 36);
            this.btnFeatures.TabIndex = 38;
            this.btnFeatures.Text = "Features";
            this.btnFeatures.UseVisualStyleBackColor = true;
            this.btnFeatures.Click += new System.EventHandler(this.btnFeatures_Click);
            // 
            // tbResults
            // 
            this.tbResults.BackColor = System.Drawing.Color.White;
            this.tbResults.Font = new System.Drawing.Font("Lucida Sans Typewriter", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbResults.ForeColor = System.Drawing.Color.Black;
            this.tbResults.Location = new System.Drawing.Point(6, 403);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.ReadOnly = true;
            this.tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbResults.Size = new System.Drawing.Size(310, 94);
            this.tbResults.TabIndex = 50;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(322, 474);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(485, 23);
            this.progressBar.TabIndex = 49;
            // 
            // panelStatus
            // 
            this.panelStatus.Location = new System.Drawing.Point(823, 8);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(313, 389);
            this.panelStatus.TabIndex = 52;
            // 
            // panelSetup
            // 
            this.panelSetup.Location = new System.Drawing.Point(3, 8);
            this.panelSetup.Name = "panelSetup";
            this.panelSetup.Size = new System.Drawing.Size(313, 389);
            this.panelSetup.TabIndex = 51;
            // 
            // UserControlDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.panelSetup);
            this.Controls.Add(this.tbResults);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnFeatures);
            this.Controls.Add(this.pictureBoxLogger);
            this.Name = "UserControlDevice";
            this.Size = new System.Drawing.Size(1142, 505);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogger)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxLogger;
        private System.Windows.Forms.Button btnFeatures;
        private System.Windows.Forms.ProgressBar progressBar;
        protected System.Windows.Forms.TextBox tbResults;
        protected System.Windows.Forms.Panel panelStatus;
        protected System.Windows.Forms.Panel panelSetup;
    }
}
