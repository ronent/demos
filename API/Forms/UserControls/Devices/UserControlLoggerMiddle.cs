﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Base.Devices;

namespace API_UI.Forms.UserControls.Devices
{
    /// <summary>
    /// This form is to get windows forms designer to render a form that implements an abstract base class
    /// </summary>
    public partial class UserControlLoggerMiddle : UserControlLogger
    {
        public UserControlLoggerMiddle(GenericLogger logger)
            :base(logger)
        {
            InitializeComponent();
        }

        public UserControlLoggerMiddle()
        {
            InitializeComponent();
        }
    }
}
