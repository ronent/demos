﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using DataNetBase.Devices.Types;
using Base.Devices.Features;
using Base.OpCodes;

namespace API_UI.Forms.UserControls.Devices.DataNetDevices
{
    public partial class UserControlDataNetDevice : UserControlDeviceMiddle
    {
      /*  #region Constructor
        public UserControlDataNetDevice(DataNetDevice device)
            :base(device)
        {
            InitializeComponent();            
        }

        public UserControlDataNetDevice()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties
        new public DataNetDevice Device { get { return base.Device as DataNetDevice; } }

        #endregion
        protected override IEnumerable<MenuItem> GetDeviceMenuItems()
        {
            List<MenuItem> items = new List<MenuItem>();

            MenuItem menuItemTurnOff = new MenuItem("Turn Off");
            menuItemTurnOff.Click += menuItemTurnOff_Click;
            items.Add(menuItemTurnOff);

            foreach (MenuItem item in items)
            {
                item.Visible = false;
            }

            foreach (var item in Device.Functions.FunctionsState)
            {
                switch (item.Function)
                {
                    case eDeviceFunction.None:
                        break;
                    case eDeviceFunction.TurnOff:
                        SetMenuItem(menuItemTurnOff, item);
                        break;
                    case eDeviceFunction.UpdateFirmware:
                        break;
                    case eDeviceFunction.CancelUpdateFirmware:
                        break;
                    case eDeviceFunction.Run:
                        break;
                    case eDeviceFunction.Stop:
                        break;
                    case eDeviceFunction.Download:
                        break;
                    case eDeviceFunction.CancelDownload:
                        break;
                    case eDeviceFunction.Calibrate:
                        break;
                    case eDeviceFunction.SaveDefaultCalibration:
                        break;
                    case eDeviceFunction.LoadDefaultCalibration:
                        break;
                    case eDeviceFunction.Setup:
                        break;
                    case eDeviceFunction.DebugMode:
                        break;
                    case eDeviceFunction.MarkTimeStamp:
                        break;
                    case eDeviceFunction.DeepSleep:
                        break;
                    default:
                        break;
                }
            }

            items.AddRange(base.GetDeviceMenuItems());
            return items;
        }

        private async void menuItemTurnOff_Click(object sender, EventArgs e)
        {
            var result = await Device.Functions.TurnOff();

            if (!result.IsOK)
                updateResult("Error turning off device: " + result.ToString());
            else
            {
                turnOffCompleted(result);
            }
        }

        private void turnOffCompleted(Result result)
        {
            if (!result.IsOK)
                updateResult("Error turning off device: " + result.ToString());
            else
            {
                updateResult("Device turned off.");
                ChangeBackColor(Color.Green);
            }
        }*/
    }
}
