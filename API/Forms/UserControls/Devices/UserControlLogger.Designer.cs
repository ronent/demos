﻿namespace API_UI.Forms.UserControls.Devices
{
    partial class UserControlLogger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.lblNumberOfSamples = new System.Windows.Forms.Label();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(541, 399);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Number Of Samples:";
            // 
            // lblNumberOfSamples
            // 
            this.lblNumberOfSamples.AutoSize = true;
            this.lblNumberOfSamples.Location = new System.Drawing.Point(651, 399);
            this.lblNumberOfSamples.Name = "lblNumberOfSamples";
            this.lblNumberOfSamples.Size = new System.Drawing.Size(13, 13);
            this.lblNumberOfSamples.TabIndex = 46;
            this.lblNumberOfSamples.Text = "0";
            // 
            // tbLog
            // 
            this.tbLog.BackColor = System.Drawing.Color.White;
            this.tbLog.Font = new System.Drawing.Font("Lucida Sans Typewriter", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLog.ForeColor = System.Drawing.Color.Black;
            this.tbLog.Location = new System.Drawing.Point(328, 8);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(485, 283);
            this.tbLog.TabIndex = 45;
            // 
            // UserControlLogger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblNumberOfSamples);
            this.Controls.Add(this.tbLog);
            this.Name = "UserControlLogger";
            this.Controls.SetChildIndex(this.tbLog, 0);
            this.Controls.SetChildIndex(this.lblNumberOfSamples, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNumberOfSamples;
        private System.Windows.Forms.TextBox tbLog;
    }
}
