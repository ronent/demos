﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Base.Sensors.Management;
using Base.Sensors.Types;
using MicroLogAPI.Device;
using MicroXBase.DataStructures;
using Base.Sensors;
using Base.Sensors.Calibrations;

namespace MicroLabAPIUI.UserControls
{
    public partial class UserControlCalibration : UserControl
    {
        #region Members
        protected UserControlLogger m_UserControlLogger;

        #endregion
        #region Constructors
        protected UserControlCalibration()
        {
            InitializeComponent();
        }

        public UserControlCalibration(UserControlLogger i_UserControlLogger)
        {
            m_UserControlLogger = i_UserControlLogger;

            InitializeComponent();
            initialize();
        }

        private void initialize()
        {
            var sensors = m_UserControlLogger.Logger.Sensors.GetAll();

            comboBoxSensors.Items.Clear();
            foreach (ISensor item in sensors)
            {
                comboBoxSensors.Items.Add((item as ISensor).Type);
            }

            comboBoxSensors.SelectedIndex = 0;
        }

        #endregion
        #region Properties
        public decimal Ref1
        {
            get { return Convert.ToDecimal(textBoxRef1.Text); }
            private set { textBoxRef1.Text = value.ToString(); }
        }

        public decimal Logger1
        {
            get { return Convert.ToDecimal(textBoxLogger1.Text); }
            private set { textBoxLogger1.Text = value.ToString(); }
        }

        public decimal Ref2
        {
            get { return Convert.ToDecimal(textBoxRef2.Text); }
            private set { textBoxRef2.Text = value.ToString(); }
        }

        public decimal Logger2
        {
            get { return Convert.ToDecimal(textBoxLogger2.Text); }
            private set { textBoxLogger2.Text = value.ToString(); }
        }

        public decimal Ref3
        {
            get 
            {
                decimal d = default(decimal);

                if (!string.IsNullOrEmpty(textBoxRef3.Text))
                {
                    d = Convert.ToDecimal(textBoxRef3.Text);
                }

                return d; 
            }

            private set { textBoxRef3.Text = value.ToString(); }
        }

        public decimal Logger3
        {
            get
            {
                decimal d = default(decimal);

                if (!string.IsNullOrEmpty(textBoxLogger3.Text))
                {
                    d = Convert.ToDecimal(textBoxLogger3.Text);
                }

                return d;
            }

            private set { textBoxLogger3.Text = value.ToString(); }
        }

        #endregion
        #region Events
        private void comboBoxSensors_SelectedIndexChanged(object sender, EventArgs e)
        {
            initializeTwoThreePoint(getSelectedSensor().Calibration.Points);
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            var logger = m_UserControlLogger.Logger as GenericMicroLogDevice;

            var sensor = getSelectedSensor();

            LoggerReference[] pairs = 
            { 
                new LoggerReference { Logger = Logger1, Reference = Ref1},
                new LoggerReference { Logger = Logger2, Reference = Ref2},
                new LoggerReference { Logger = Logger3, Reference = Ref3},
            };

            CalibrationConfiguration config = new CalibrationConfiguration(logger.Sensors);
            config.Add(sensor.Index, sensor.GenerateCalibration(pairs));

            var result = logger.BeginSendCalibration(config, null);

            switch (result.Value)
            {
                case Base.OpCodes.ResultEnum.OK:
                    MessageBox.Show("Calibration sent successfully");
                    break;
                case Base.OpCodes.ResultEnum.ILLEGAL_CALL:   
                case Base.OpCodes.ResultEnum.ERROR:
                case Base.OpCodes.ResultEnum.OFFLINE:
                case Base.OpCodes.ResultEnum.TIMED_OUT:
                case Base.OpCodes.ResultEnum.CANCELED:
                case Base.OpCodes.ResultEnum.BUSY:
                    MessageBox.Show("Error");
                    break;
                default:
                    break;
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            var result = m_UserControlLogger.Logger.BeginResetCalibration(null);

            switch (result.Value)
            {
                case Base.OpCodes.ResultEnum.OK:
                    MessageBox.Show("Reset calibration success");
                    break;
                case Base.OpCodes.ResultEnum.ILLEGAL_CALL:
                case Base.OpCodes.ResultEnum.ERROR:
                case Base.OpCodes.ResultEnum.OFFLINE:
                case Base.OpCodes.ResultEnum.TIMED_OUT:
                case Base.OpCodes.ResultEnum.CANCELED:
                case Base.OpCodes.ResultEnum.BUSY:
                    MessageBox.Show("Error");
                    break;
                default:
                    break;
            }
        }

        private void buttonRestore_Click(object sender, EventArgs e)
        {
            var result = m_UserControlLogger.Logger.BeginRestoreDefaultCalibration(null);

            switch (result.Value)
            {
                case Base.OpCodes.ResultEnum.OK:
                    MessageBox.Show("Restore factory calibration success");
                    break;
                case Base.OpCodes.ResultEnum.ILLEGAL_CALL:
                case Base.OpCodes.ResultEnum.ERROR:
                case Base.OpCodes.ResultEnum.OFFLINE:
                case Base.OpCodes.ResultEnum.TIMED_OUT:
                case Base.OpCodes.ResultEnum.CANCELED:
                case Base.OpCodes.ResultEnum.BUSY:
                    MessageBox.Show("Error");
                    break;
                default:
                    break;
            }
        }

        #endregion
        #region Methods
        private void initializeTwoThreePoint(int p)
        {
            textBoxLogger1.Text = UserControlLogger.DecimalFormat(getSelectedSensor().Calibration.A);
            textBoxLogger2.Text = UserControlLogger.DecimalFormat(getSelectedSensor().Calibration.B);

            double range = Math.Abs(getSelectedSensor().Minimum) + Math.Abs(getSelectedSensor().Maximum);

            if (p == 2)
            {
                enableDisableThreePoint(false);
                textBoxRef1.Text = UserControlLogger.DoubleFormat((range / 3));
                textBoxRef2.Text = UserControlLogger.DoubleFormat((range / 3 * 2));
            }
            else
            {
                enableDisableThreePoint(true);
                textBoxLogger3.Text = (getSelectedSensor().Calibration as ThreePoint).C.ToString();
                textBoxRef1.Text = UserControlLogger.DoubleFormat((range / 4));
                textBoxRef2.Text = UserControlLogger.DoubleFormat((range / 4 * 2));
                textBoxRef3.Text = UserControlLogger.DoubleFormat((range / 4 * 3));
            }
        }

        private void enableDisableThreePoint(bool i_ThreePoint)
        {
            textBoxLogger3.Visible = i_ThreePoint;
            textBoxRef3.Visible = i_ThreePoint;
            labelPoint3.Visible = i_ThreePoint;
        }

        protected eSensorType getSelectedSensorType()
        {
            return (eSensorType)comboBoxSensors.SelectedItem;
        }

        private ISensor getSelectedSensor()
        {
            ISensor[] sensors = m_UserControlLogger.Logger.Sensors.GetAll();
            foreach (var item in sensors)
            {
                if (item.Type == getSelectedSensorType())
                {
                    return item;
                }
            }

            return null;
        }
        #endregion
    }
}
