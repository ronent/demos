﻿namespace API_UI.Forms.UserControls.Sensors
{
    partial class SiMiUserControlFixedSensor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxSensorName = new System.Windows.Forms.CheckBox();
            this.checkBoxAlarm = new System.Windows.Forms.CheckBox();
            this.textBoxLow = new System.Windows.Forms.TextBox();
            this.textBoxHigh = new System.Windows.Forms.TextBox();
            this.labelSeperator = new System.Windows.Forms.Label();
            this.labelUnit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // checkBoxSensorName
            // 
            this.checkBoxSensorName.AutoSize = true;
            this.checkBoxSensorName.Location = new System.Drawing.Point(3, 3);
            this.checkBoxSensorName.Name = "checkBoxSensorName";
            this.checkBoxSensorName.Size = new System.Drawing.Size(86, 17);
            this.checkBoxSensorName.TabIndex = 1;
            this.checkBoxSensorName.Text = "sensor name";
            this.checkBoxSensorName.UseVisualStyleBackColor = true;
            this.checkBoxSensorName.CheckedChanged += new System.EventHandler(this.checkBoxSensorName_CheckedChanged);
            this.checkBoxSensorName.SizeChanged += new System.EventHandler(this.checkBoxSensorName_SizeChanged);
            // 
            // checkBoxAlarm
            // 
            this.checkBoxAlarm.AutoSize = true;
            this.checkBoxAlarm.Location = new System.Drawing.Point(76, 33);
            this.checkBoxAlarm.Name = "checkBoxAlarm";
            this.checkBoxAlarm.Size = new System.Drawing.Size(52, 17);
            this.checkBoxAlarm.TabIndex = 2;
            this.checkBoxAlarm.Text = "Alarm";
            this.checkBoxAlarm.UseVisualStyleBackColor = true;
            this.checkBoxAlarm.CheckedChanged += new System.EventHandler(this.checkBoxAlarm_CheckedChanged);
            // 
            // textBoxLow
            // 
            this.textBoxLow.Location = new System.Drawing.Point(143, 31);
            this.textBoxLow.Name = "textBoxLow";
            this.textBoxLow.Size = new System.Drawing.Size(42, 20);
            this.textBoxLow.TabIndex = 3;
            // 
            // textBoxHigh
            // 
            this.textBoxHigh.Location = new System.Drawing.Point(216, 31);
            this.textBoxHigh.Name = "textBoxHigh";
            this.textBoxHigh.Size = new System.Drawing.Size(42, 20);
            this.textBoxHigh.TabIndex = 4;
            // 
            // labelSeperator
            // 
            this.labelSeperator.AutoSize = true;
            this.labelSeperator.Location = new System.Drawing.Point(193, 35);
            this.labelSeperator.Name = "labelSeperator";
            this.labelSeperator.Size = new System.Drawing.Size(10, 13);
            this.labelSeperator.TabIndex = 5;
            this.labelSeperator.Text = "-";
            // 
            // labelUnit
            // 
            this.labelUnit.AutoSize = true;
            this.labelUnit.Location = new System.Drawing.Point(95, 4);
            this.labelUnit.Name = "labelUnit";
            this.labelUnit.Size = new System.Drawing.Size(30, 13);
            this.labelUnit.TabIndex = 6;
            this.labelUnit.Text = "[unit]";
            // 
            // UserControlFixedSensor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelUnit);
            this.Controls.Add(this.labelSeperator);
            this.Controls.Add(this.textBoxHigh);
            this.Controls.Add(this.textBoxLow);
            this.Controls.Add(this.checkBoxAlarm);
            this.Controls.Add(this.checkBoxSensorName);
            this.Name = "UserControlFixedSensor";
            this.Size = new System.Drawing.Size(262, 55);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.CheckBox checkBoxSensorName;
        private System.Windows.Forms.CheckBox checkBoxAlarm;
        private System.Windows.Forms.TextBox textBoxLow;
        private System.Windows.Forms.TextBox textBoxHigh;
        private System.Windows.Forms.Label labelSeperator;
        protected System.Windows.Forms.Label labelUnit;
    }
}
