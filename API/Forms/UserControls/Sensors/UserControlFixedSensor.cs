﻿using API_UI.Forms.UserControls.Setup;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Base.Sensors.Types.Temperature;
using MicroLogAPI.DataStructures;
using PicoLite.DataStructures.V1;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace API_UI.Forms.UserControls.Sensors
{
    public partial class UserControlFixedSensor : UserControl
    {
        #region Members
        protected GenericSensor m_Sensor;
        private UserControlSetup m_Parent;

        #endregion
        #region Constructor
        protected UserControlFixedSensor()
        {
            InitializeComponent();
        }

        public UserControlFixedSensor(GenericSensor i_Sensor, UserControlSetup parent)
        {
            InitializeComponent();

            m_Sensor = i_Sensor;
            m_Parent = parent;
            initialize();
        }

        #endregion
        #region Properties
        public eSensorIndex Index
        {
            get { return m_Sensor.Index; }    
        }

        public bool IsTemperature
        {
            get { return m_Sensor is GenericTemperatureSensor; }
        }

        public string SensorName
        {
            get { return checkBoxSensorName.Text; }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { sensorName(value); }));
                }
                else
                {
                    sensorName(value);
                }
            }
        }

        private void sensorName(string value)
        {
            checkBoxSensorName.Text = value; 
        }

        public bool Enable
        {
            get { return checkBoxSensorName.Checked; }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { enable(value); }));
                }
                else
                {
                    enable(value);
                }
            }
        }

        private void enable(bool value)
        {
            checkBoxSensorName.Checked = value; 
        }

        public bool AlarmEnable
        {
            get { return checkBoxAlarm.Checked; }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { alarmEnable(value); }));
                }
                else
                {
                    alarmEnable(value);
                }
            }
        }

        private void alarmEnable(bool value)
        {
            checkBoxAlarm.Checked = value; 
        }

        public decimal AlarmLow
        {
            get { return Convert.ToDecimal(textBoxLow.Text); }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { alarmLow(value); }));
                }
                else
                {
                    alarmLow(value);
                }
            }
        }

        private void alarmLow(decimal value)
        {
            textBoxLow.Text = Math.Round(value, 2, MidpointRounding.ToEven).ToString(); ; 
        }

        public decimal AlarmHigh
        {
            get { return Convert.ToDecimal(textBoxHigh.Text); }
            protected set 
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { alarmHigh(value); }));
                }
                else
                {
                    alarmHigh(value);
                }
            }
        }

        private void alarmHigh(decimal value)
        {
            textBoxHigh.Text = Math.Round(value, 2, MidpointRounding.ToEven).ToString(); 
        }

        public string Unit
        {
            get { return labelUnit.Text; }
            protected set
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new MethodInvoker(delegate() { unit(value); }));
                }
                else
                {
                    unit(value);
                }
            }
        }

        private void unit(string value)
        {
            labelUnit.Text = string.Format("[{0}]", value);
        }

        public bool Checkable
        {
            get { return checkBoxSensorName.Enabled; }
            set { checkBoxSensorName.Enabled = value; }
        }

        #endregion
        #region Methods
        public virtual void Close() { }

        protected virtual void initialize()
        {
            checkBoxAlarm_CheckedChanged(checkBoxAlarm, EventArgs.Empty);
            checkBoxSensorName_CheckedChanged(checkBoxSensorName, EventArgs.Empty);

            refreshSensor();
        }

        protected virtual void refreshSensor()
        {
            Enable = m_Sensor.Enabled;
            SensorName = (m_Sensor as GenericSensor).Name;
            Unit = (m_Sensor as GenericSensor).Unit.Name;

            decimal low = Convert.ToDecimal(m_Sensor.Minimum);
            decimal high = Convert.ToDecimal(m_Sensor.Maximum);

            //if (m_Sensor is MicroLogAPI.Sensors.V1.UserDefinedSensor)
            //{
            //    var sensor = m_Sensor as UserDefinedSensor;
            //    low = Convert.ToDecimal(sensor.TransformFromBase(sensor.Minimum));
            //    high = Convert.ToDecimal(sensor.TransformFromBase(sensor.Maximum));
            //}

            AlarmLow = low;
            AlarmHigh = high;
            AlarmEnable = m_Sensor.Alarm.Enabled;
        }

        internal void RefreshUnit(bool celsuis)
        {
            if (!IsTemperature)
                return;

            if (celsuis)
            {
                Unit = "°C";
                AlarmLow = Convert.ToDecimal(Auxiliary.Tools.Transformation.ConvertToCelsius(AlarmLow));
                AlarmHigh = Convert.ToDecimal(Auxiliary.Tools.Transformation.ConvertToCelsius(AlarmHigh));
            }
            else
            {
                Unit = "°F";
                AlarmLow = Convert.ToDecimal(Auxiliary.Tools.Transformation.ConvertToFahrenheit(AlarmLow));
                AlarmHigh = Convert.ToDecimal(Auxiliary.Tools.Transformation.ConvertToFahrenheit(AlarmHigh));
            }
        }

        internal virtual void LoadFromStatus(MicroLogStatusConfiguration i_Status)
        {
            switch (m_Sensor.Index)
            {
                case eSensorIndex.Temperature:
                    Enable = i_Status.FixedSensors.TemperatureEnabled;
                    AlarmEnable = i_Status.FixedSensors.TemperatureAlarm.Enabled;
                    if (AlarmEnable)
                    {
                        AlarmLow = Convert.ToDecimal(i_Status.FixedSensors.TemperatureAlarm.Low);
                        AlarmHigh = Convert.ToDecimal(i_Status.FixedSensors.TemperatureAlarm.High);
                    }

                    break;
                case eSensorIndex.Humidity:
                    Enable = i_Status.FixedSensors.HumidityEnabled;
                    AlarmEnable = i_Status.FixedSensors.HumidityAlarm.Enabled;
                    if (AlarmEnable)
                    {
                        AlarmLow = Convert.ToDecimal(i_Status.FixedSensors.HumidityAlarm.Low);
                        AlarmHigh = Convert.ToDecimal(i_Status.FixedSensors.HumidityAlarm.High);
                    }

                    break;
                case eSensorIndex.DewPoint:
                    Enable = i_Status.FixedSensors.DewPointEnabled;
                    AlarmEnable = i_Status.FixedSensors.DewPointAlarm.Enabled;
                    if (AlarmEnable)
                    {
                        AlarmLow = Convert.ToDecimal(i_Status.FixedSensors.DewPointAlarm.Low);
                        AlarmHigh = Convert.ToDecimal(i_Status.FixedSensors.DewPointAlarm.High);
                    }

                    break;
            }

            Unit = m_Sensor.Unit.Name;
        }

        internal void LoadFromStatus(PicoLiteV1StatusConfiguration i_Status)
        {
            Enable = i_Status.TemperatureSensor.TemperatureEnabled;
            AlarmEnable = i_Status.TemperatureSensor.TemperatureAlarm.Enabled;
            if (AlarmEnable)
            {
                AlarmLow = Convert.ToDecimal(i_Status.TemperatureSensor.TemperatureAlarm.Low);
                AlarmHigh = Convert.ToDecimal(i_Status.TemperatureSensor.TemperatureAlarm.High);
            }

            Unit = m_Sensor.Unit.Name;
        }

        private bool checkSensorEnabled(eSensorIndex i_Index)
        {
            return m_Parent.GetUCFixedSensor(i_Index).Enable;
        }

        #endregion
        #region Events
        private void checkBoxAlarm_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (sender as CheckBox);

            textBoxHigh.Visible = cb.Checked;
            textBoxLow.Visible = cb.Checked;
            labelSeperator.Visible = cb.Checked;
        }

        protected virtual void checkBoxSensorName_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (sender as CheckBox);

            if (m_Sensor.Index == eSensorIndex.DewPoint)
            {
                if (!checkSensorEnabled(eSensorIndex.Temperature) || !checkSensorEnabled(eSensorIndex.Humidity))
                {
                    cb.Checked = false;
                }
            }

            if (m_Sensor.Index == eSensorIndex.Humidity)
            {
                if (!checkSensorEnabled(eSensorIndex.Temperature))
                {
                    cb.Checked = false;
                }
            }

            if (m_Sensor.Index == eSensorIndex.Temperature || m_Sensor.Index == eSensorIndex.Humidity && cb.Checked == false)
            {
                var dewPoint = m_Parent.GetUCFixedSensor(eSensorIndex.DewPoint);
                if (dewPoint != null)
                {
                    dewPoint.Enable = false;
                }

                var humidity = m_Parent.GetUCFixedSensor(eSensorIndex.Humidity);
                if (humidity != null)
                {
                    humidity.Enable = false;
                }
            }

            checkBoxAlarm.Enabled = cb.Checked;
            textBoxHigh.Enabled = cb.Checked;
            textBoxLow.Enabled = cb.Checked;
            checkBoxSensorName.Checked = cb.Checked;
        }

        protected virtual void checkBoxSensorName_SizeChanged(object sender, EventArgs e)
        {
            labelUnit.Location = new Point(checkBoxSensorName.Width + 2, labelUnit.Location.Y);
        }

        #endregion
    }
}
