﻿using API_UI.Forms;
using Base.Sensors.Management;
using Base.Sensors.Types;
using MicroLogAPI.DataStructures;
using MicroXBase.Sensors.Management;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace API_UI.Forms.UserControls.Sensors
{
    public partial class UserControlDetachableSensors : UserControlFixedSensor
    {
        #region Members
        private List<GenericSensor> m_Sensors;
        private MicroXSensorManager m_SensorManager;
        private MicroLogUDSConfiguration m_Configuration;
        private DefineSensorForm m_DefineSensorForm;
        private string m_UserDefineName = "User Define Sensor";
        private bool m_FirstTime = true;

        #endregion
        #region Constructor
        public UserControlDetachableSensors(MicroXSensorManager i_SensorManager, List<GenericSensor> i_Sensors, bool i_CanDisable)
            : base()
        {
            InitializeComponent();
            m_Sensors = i_Sensors.ToList();
            m_SensorManager = i_SensorManager;

            initialize();

            checkBoxSensorName.Enabled = i_CanDisable;
        }

        public UserControlDetachableSensors(MicroXSensorManager i_SensorManager, GenericSensor sensor)
            : this(i_SensorManager, new List<GenericSensor>() { sensor }, true) 
        {
            
        }

        #endregion
        #region Methods
        public override void Close()
        {
            if (m_DefineSensorForm != null)
                m_DefineSensorForm.Close();
        }

        protected override void initialize()
        {
            foreach (var item in m_Sensors)
            {
                if (item.Enabled)
                {
                    Sensor = item;
                    Enable = true;
                    break;
                }
            }

            if (Sensor == null)
                Sensor = m_Sensors[0];

            base.initialize();

            bool userDefineExists = false;
            foreach (var item in m_Sensors)
            {
                if (item.Type == eSensorType.UserDefined)
                    userDefineExists = true;

                comboBoxDetachableSensor.Items.Add(item.Name == null ? "" : item.Name);
            }

            // user define
            if (!userDefineExists && !(Sensor is Base.Sensors.Types.Temperature.ExternalNTC))
                comboBoxDetachableSensor.Items.Add(m_UserDefineName);
        }

        protected override void refreshSensor()
        {
            bool preEnable = Enable;
            bool preAlarm = AlarmEnable;

            base.refreshSensor();
            
            Enable = preEnable;
            AlarmEnable = preAlarm;

            labelUnit.Visible = Enable;
        }

        internal override void LoadFromStatus(MicroLogStatusConfiguration i_Status)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate() { LoadFromStatus(i_Status); }));
            }
            else
            {
                switch (m_Sensor.Index)
                {
                    case eSensorIndex.External1:
                        for (int i = 0; i < m_Sensors.Count; i++)
                        {
                            if (m_Sensors[i].Type == i_Status.ExternalSensor.Type)
                            {
                                comboBoxDetachableSensor.SelectedIndex = i;
                            }
                        }
                        Enable = i_Status.ExternalSensor.Enabled;
                        AlarmEnable = i_Status.ExternalSensor.Alarm.Enabled;
                        if (AlarmEnable)
                        {
                            AlarmLow = Convert.ToDecimal(i_Status.ExternalSensor.Alarm.Low);
                            AlarmHigh = Convert.ToDecimal(i_Status.ExternalSensor.Alarm.High);
                        }

                        var enumType = i_Status.ExternalSensor.UserDefinedSensor.Unit;
                        var customString = i_Status.ExternalSensor.UserDefinedSensor.CustomUnit;
                        //Unit = DefineSensorForm.GetUnitString(enumType, customString); 
                        break;
                    case eSensorIndex.External2:
                        break;
                    case eSensorIndex.External3:
                        break;
                    case eSensorIndex.External4:
                        break;
                }
            } 
        }

        string lastSensor;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (comboBoxDetachableSensor.SelectedItem != null)
                lastSensor = comboBoxDetachableSensor.SelectedItem.ToString();
        }

        #endregion
        #region Events
        private void comboBoxDetachableSensor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var oldConfig = Configuration;

            if ((cb.SelectedItem as string).Equals(DefineSensorName) && !m_FirstTime)
            {
                if (m_DefineSensorForm == null)
                    m_DefineSensorForm = new DefineSensorForm(m_Sensors);                    

                m_DefineSensorForm.ShowDialog();

                if (m_DefineSensorForm.DialogResult == DialogResult.OK)
                {
                    // create user define sensor
                    Configuration = m_DefineSensorForm.Configuration;
                    DefineSensorName = Configuration.Name;
                    refreshSensor(Configuration);
                }
                else
                {
                    if (m_DefineSensorForm.Configuration != null)
                        refreshSensor(Configuration);
                    else
                        cb.SelectedItem = lastSensor;
                }
            }
            else
            {
                m_Sensor = m_Sensors[cb.SelectedIndex];

                if (m_Sensor.Type == eSensorType.UserDefined && !m_FirstTime)
                {
                    List<GenericSensor> baseSensors = m_Sensors.ToList();
                    baseSensors.Remove(m_Sensor);

                    if (m_DefineSensorForm == null || m_DefineSensorForm.IsDisposed)
                        m_DefineSensorForm = new DefineSensorForm(m_Sensor, baseSensors);

                    m_DefineSensorForm.ShowDialog();

                    if (m_DefineSensorForm.DialogResult == DialogResult.OK)
                    {
                        Configuration = m_DefineSensorForm.Configuration;
                        refreshSensor(Configuration);
                    }
                }
                else
                {
                    refreshSensor();
                    SensorName = string.Empty;
                }
            }

            if (m_FirstTime)
                m_FirstTime = !m_FirstTime;
        }

        private void refreshSensor(MicroLogUDSConfiguration Configuration)
        {
            Unit = DefineSensorForm.GetUnitString(Configuration.Unit, Configuration.CustomUnit);
            decimal low = m_SensorManager.GetDetachableByType(Configuration.BaseType).Minimum;
            AlarmLow = Convert.ToDecimal(Configuration.TransformFromBase(low));
            decimal high = m_SensorManager.GetDetachableByType(Configuration.BaseType).Maximum;
            AlarmHigh = Convert.ToDecimal(Configuration.TransformFromBase(high));

        }

        protected override void checkBoxSensorName_CheckedChanged(object sender, EventArgs e)
        {
            base.checkBoxSensorName_CheckedChanged(sender, e);
            comboBoxDetachableSensor.Enabled = (sender as CheckBox).Checked;

            if (comboBoxDetachableSensor.Enabled)
                if (comboBoxDetachableSensor.SelectedIndex == -1 && comboBoxDetachableSensor.Items.Count > 0)
                    comboBoxDetachableSensor.SelectedIndex = 0;
        }

        protected override void checkBoxSensorName_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                labelUnit.Location = new Point(comboBoxDetachableSensor.ClientSize.Width + 25, labelUnit.Location.Y);
            }
            catch { }
        }

        #endregion
        #region Properties
        public bool DefineSensor 
        { 
            get 
            {
                if (Sensor == null)
                {
                    return true;
                }

                return Sensor.Type == eSensorType.UserDefined; 
            } 
        }

        public GenericSensor Sensor
        {
            get 
            {
                try
                {
                    if (comboBoxDetachableSensor.SelectedIndex >= 0)
                        return m_Sensors.Find(sensor => sensor.Name.Equals(comboBoxDetachableSensor.SelectedItem));
                }
                catch
                {
                    return null;
                }

                return m_Sensor;
            }
            private set { m_Sensor = value; }
        }

        public MicroLogUDSConfiguration Configuration 
        {
            get
            {
                return m_Configuration;
            }
            private set 
            {
                if (value != null)
                {
                    m_Configuration = value;
                    DefineSensorName = m_Configuration.Name;
                }
            }
        }

        private string DefineSensorName
        {
            get { return m_UserDefineName; }
            set
            {
                m_UserDefineName = value;

                comboBoxDetachableSensor.Items.Remove(comboBoxDetachableSensor.SelectedItem);
                comboBoxDetachableSensor.Items.Add(value);

                comboBoxDetachableSensor.SelectedIndexChanged -= comboBoxDetachableSensor_SelectedIndexChanged;
                comboBoxDetachableSensor.SelectedItem = value;
                comboBoxDetachableSensor.SelectedIndexChanged += comboBoxDetachableSensor_SelectedIndexChanged;
            }
        }

        #endregion
    }
}
