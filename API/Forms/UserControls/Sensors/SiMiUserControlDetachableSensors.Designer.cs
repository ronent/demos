﻿namespace API_UI.Forms.UserControls.Sensors
{
    partial class SiMiUserControlDetachableSensors
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxDetachableSensor = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // labelUnit
            // 
            this.labelUnit.Location = new System.Drawing.Point(233, 4);
            // 
            // comboBoxDetachableSensor
            // 
            this.comboBoxDetachableSensor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDetachableSensor.FormattingEnabled = true;
            this.comboBoxDetachableSensor.Location = new System.Drawing.Point(21, 1);
            this.comboBoxDetachableSensor.Name = "comboBoxDetachableSensor";
            this.comboBoxDetachableSensor.Size = new System.Drawing.Size(182, 21);
            this.comboBoxDetachableSensor.TabIndex = 7;
            this.comboBoxDetachableSensor.SelectedIndexChanged += new System.EventHandler(this.comboBoxDetachableSensor_SelectedIndexChanged);
            // 
            // UserControlDetachableSensors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxDetachableSensor);
            this.Name = "UserControlDetachableSensors";
            this.Size = new System.Drawing.Size(262, 55);
            this.Controls.SetChildIndex(this.checkBoxSensorName, 0);
            this.Controls.SetChildIndex(this.labelUnit, 0);
            this.Controls.SetChildIndex(this.comboBoxDetachableSensor, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDetachableSensor;
    }
}
