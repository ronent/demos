﻿namespace API_UI.Forms
{
    partial class DefineSensorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSend = new System.Windows.Forms.Button();
            this.comboBoxDecimalDigits = new System.Windows.Forms.ComboBox();
            this.comboBoxBaseSensor = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxRef2 = new System.Windows.Forms.TextBox();
            this.textBoxRef1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxUnit = new System.Windows.Forms.ComboBox();
            this.textBoxLogger2 = new System.Windows.Forms.TextBox();
            this.textBoxLogger1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(165, 259);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 46;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSend
            // 
            this.buttonSend.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonSend.Location = new System.Drawing.Point(41, 259);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(75, 23);
            this.buttonSend.TabIndex = 6;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // comboBoxDecimalDigits
            // 
            this.comboBoxDecimalDigits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDecimalDigits.FormattingEnabled = true;
            this.comboBoxDecimalDigits.Location = new System.Drawing.Point(110, 116);
            this.comboBoxDecimalDigits.Name = "comboBoxDecimalDigits";
            this.comboBoxDecimalDigits.Size = new System.Drawing.Size(62, 21);
            this.comboBoxDecimalDigits.TabIndex = 3;
            // 
            // comboBoxBaseSensor
            // 
            this.comboBoxBaseSensor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBaseSensor.FormattingEnabled = true;
            this.comboBoxBaseSensor.Location = new System.Drawing.Point(110, 79);
            this.comboBoxBaseSensor.Name = "comboBoxBaseSensor";
            this.comboBoxBaseSensor.Size = new System.Drawing.Size(136, 21);
            this.comboBoxBaseSensor.TabIndex = 2;
            this.comboBoxBaseSensor.SelectedIndexChanged += new System.EventHandler(this.comboBoxBaseSensor_SelectedIndexChanged);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(110, 14);
            this.textBoxName.MaxLength = 10;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(136, 20);
            this.textBoxName.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Decimal digits";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Base sensor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Sensor unit";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Sensor name";
            // 
            // textBoxRef2
            // 
            this.textBoxRef2.Location = new System.Drawing.Point(83, 226);
            this.textBoxRef2.Name = "textBoxRef2";
            this.textBoxRef2.Size = new System.Drawing.Size(62, 20);
            this.textBoxRef2.TabIndex = 5;
            // 
            // textBoxRef1
            // 
            this.textBoxRef1.Location = new System.Drawing.Point(83, 185);
            this.textBoxRef1.Name = "textBoxRef1";
            this.textBoxRef1.Size = new System.Drawing.Size(62, 20);
            this.textBoxRef1.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Reference Value";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Value #2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Value #1";
            // 
            // comboBoxUnit
            // 
            this.comboBoxUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnit.FormattingEnabled = true;
            this.comboBoxUnit.Location = new System.Drawing.Point(110, 45);
            this.comboBoxUnit.MaxLength = 4;
            this.comboBoxUnit.Name = "comboBoxUnit";
            this.comboBoxUnit.Size = new System.Drawing.Size(62, 21);
            this.comboBoxUnit.TabIndex = 1;
            // 
            // textBoxLogger2
            // 
            this.textBoxLogger2.Location = new System.Drawing.Point(178, 226);
            this.textBoxLogger2.Name = "textBoxLogger2";
            this.textBoxLogger2.Size = new System.Drawing.Size(62, 20);
            this.textBoxLogger2.TabIndex = 48;
            // 
            // textBoxLogger1
            // 
            this.textBoxLogger1.Location = new System.Drawing.Point(178, 185);
            this.textBoxLogger1.Name = "textBoxLogger1";
            this.textBoxLogger1.Size = new System.Drawing.Size(62, 20);
            this.textBoxLogger1.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(175, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "Logger Value";
            // 
            // DefineSensorForm
            // 
            this.AcceptButton = this.buttonSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 296);
            this.ControlBox = false;
            this.Controls.Add(this.textBoxLogger2);
            this.Controls.Add(this.textBoxLogger1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxUnit);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.comboBoxDecimalDigits);
            this.Controls.Add(this.comboBoxBaseSensor);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxRef2);
            this.Controls.Add(this.textBoxRef1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DefineSensorForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define Sensor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.ComboBox comboBoxDecimalDigits;
        private System.Windows.Forms.ComboBox comboBoxBaseSensor;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxRef2;
        private System.Windows.Forms.TextBox textBoxRef1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxUnit;
        private System.Windows.Forms.TextBox textBoxLogger2;
        private System.Windows.Forms.TextBox textBoxLogger1;
        private System.Windows.Forms.Label label6;
    }
}