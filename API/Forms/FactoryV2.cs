﻿using API_UI.Forms.UserControls.Devices;
using API_UI.Forms.UserControls.Devices.DataNetDevices.DNL9x0;
using API_UI.Forms.UserControls.Devices.DataNetDevices.DNR900;
using API_UI.Forms.UserControls.Devices.MicroXDevices.EC8xx;
using API_UI.Forms.UserControls.Devices.MicroXDevices.ML2;
using API_UI.Forms.UserControls.Devices.MicroXDevices.PicoLite;
using API_UI.Forms.UserControls.Setup;
using API_UI.Forms.UserControls.Setup.PicoLite;
using Base.Devices;
//using DataNet910.Devices.V1;
//using DataNet920.Devices;
//using DataNetRepeater.Devices.V1;
using MicroLogPro2TH.Devices;
using System;
using System.Windows.Forms;
using API_UI.Forms.UserControls.Devices.SiMiDevices.SiMi;
using SiMiAPI.DataStructures;
using SiMiAPI.Devices;
using SiMiExternal.Devices;
using SiMiRH.Devices;
using SiMiTemperature.Devices;

namespace API_UI.Forms
{
    public static class FactoryV2
    {
        public static SiMiUserControlSetup GetSetupControl(GenericDevice device)
        {
            
            if (device is SiMiLogger)
            {
                return new SiMiSetup(device as SiMiLogger);
            }
            return null;
        }

        public static UserControl GetDeviceControl(GenericDevice device)
        {
           
            if (device is SiMiRHLogger )
            {
                return new UserControlRH(device as SiMiRHLogger);
            }
            if (device is SiMiTemperatureLogger)
            {
                return new UserControlTemperature(device as SiMiTemperatureLogger);
            }
            if (device is SiMiExternalLogger)
            {
                return new UserControlExternal(device as SiMiExternalLogger);
            }
            throw new Exception("Unsupported device type");
        }
    }
}
