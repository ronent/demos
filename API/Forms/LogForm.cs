﻿using Log4Tech;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace API_UI.Forms
{
    public partial class LogForm : Form
    {
        public LogForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Log.Instance.OnEventLogged += Log_OnEventLogged;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }
  
        void Log_OnEventLogged(object sender, string message)
        {
            appendText(message);
        }

        private void appendText(string msg)
        {
            if (textBox1.InvokeRequired)
            {
                textBox1.Invoke(new MethodInvoker(delegate() { appendText(msg); }));
            }
            else
            {
                textBox1.AppendText(msg + Environment.NewLine);
            }
        }
    }
}
