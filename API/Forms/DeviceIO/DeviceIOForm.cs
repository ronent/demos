﻿using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using API_UI.Utilities;
using Infrastructure.Communication;
using Base.Devices;
//using DataNetLoggersAPI.Devices;

namespace API_UI.Forms.DeviceIO
{
    public partial class DeviceIOForm : Form
    {
        private OpCodesManagerForm m_OpCodesManagerForm;

        private GenericDevice device;

        public DeviceIOForm(GenericDevice device)
        {
            InitializeComponent();

            this.device = device;

            initialize();
        }

        private void initialize()
        {
            this.Text += " - " + device.ToString();

            m_OpCodesManagerForm = new OpCodesManagerForm(device);

            cbInput.DataSource = m_OpCodesManagerForm.OpCodeBinder;
        }

        protected override void OnLoad(EventArgs e)
        {
            device.OpCodes.OnDataReceived += DeviceIO_OnDataReceived;
            device.OpCodes.OnDataSent += DeviceIO_OnDataSent;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        void DeviceIO_OnDataSent(object sender, DataTransmittedEventArgs e)
        {
            updateOutput("Out: " + BitConverter.ToString(e.Data), Color.Red);            
        }

        void DeviceIO_OnDataReceived(object sender, DataTransmittedEventArgs e)
        {
            updateOutput("In:  " + BitConverter.ToString(e.Data), Color.Blue);
        }

        private void updateOutput(string msg, Color color)
        {
            if (tbOutput.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { updateOutput(msg, color); }));
            }
            else
            {
                tbOutput.AppendText(msg + Environment.NewLine, color);

                if (cbAutoScroll.Checked)
                {
                    tbOutput.SelectionStart = tbOutput.Text.Length;
                    tbOutput.ScrollToCaret();
                }
            }
        }

        private void sendData(byte[] data)
        {
            device.OpCodes.SendData(data);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (cbInput.SelectedItem is OpCode)
            {
                sendData((cbInput.SelectedItem as OpCode).Data);
            }
            else
            {
                sendData(GetBytes(device, cbInput.Text));
            }
        }

        public static byte[] GetBytes(GenericDevice device, string str)
        {
            str = str.Replace(" ", "-");
            var splittedString = str.Split('-');
            var length = splittedString.Length;
            byte[] arr;

            if (device is MicroXLogger)
            {
                arr = new byte[device.OpCodes.MaximumArraySize];
            }
            else
            {
                arr = new byte[length];
            }

            int i = 0;
            try
            {
                for (i = 0; i < length; i++)
                {
                    arr[i + device.OpCodes.OpCodeStartByte] = Convert.ToByte(splittedString[i], 16);
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                for (int j = i; j < arr.Length; j++)
                {
                    arr[j] = Convert.ToByte("00", 16);
                }
            }

            return arr;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            m_OpCodesManagerForm.ShowDialog();
        }
    }
}
