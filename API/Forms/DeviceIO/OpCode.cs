﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_UI.Forms.DeviceIO
{
    [Serializable]
    public class OpCode
    {
        public string Name { get; private set; }
        public byte[] Data { get; private set; }
        public int StartByte { get; private set; }

        public OpCode(string name, byte[] data, int startByte)
        {
            Name = name;
            Data = data;
            StartByte = startByte;
        }

        public override string ToString()
        {
            return "Name: " + Name + ", OpCode: '" + Data[StartByte] + "'";
        }
    }
}
