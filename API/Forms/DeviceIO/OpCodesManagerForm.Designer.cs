﻿namespace API_UI.Forms.DeviceIO
{
    partial class OpCodesManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbOpcodes = new System.Windows.Forms.ListBox();
            this.tbOpcode = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbOpcodeName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbOpcodes
            // 
            this.lbOpcodes.FormattingEnabled = true;
            this.lbOpcodes.Location = new System.Drawing.Point(7, 18);
            this.lbOpcodes.Name = "lbOpcodes";
            this.lbOpcodes.Size = new System.Drawing.Size(176, 199);
            this.lbOpcodes.TabIndex = 4;
            this.lbOpcodes.SelectedIndexChanged += new System.EventHandler(this.lbOpcodes_SelectedIndexChanged);
            this.lbOpcodes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lbOpcodes_MouseUp);
            // 
            // tbOpcode
            // 
            this.tbOpcode.Location = new System.Drawing.Point(202, 52);
            this.tbOpcode.Multiline = true;
            this.tbOpcode.Name = "tbOpcode";
            this.tbOpcode.Size = new System.Drawing.Size(332, 134);
            this.tbOpcode.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(440, 194);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save And Close";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(202, 194);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbOpcodeName
            // 
            this.tbOpcodeName.Location = new System.Drawing.Point(289, 18);
            this.tbOpcodeName.Name = "tbOpcodeName";
            this.tbOpcodeName.Size = new System.Drawing.Size(114, 20);
            this.tbOpcodeName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(201, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Opcode Name: ";
            // 
            // OpCodesManagerForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 229);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbOpcodeName);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbOpcode);
            this.Controls.Add(this.lbOpcodes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OpCodesManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Opcodes Manager";
            this.Shown += new System.EventHandler(this.OpCodesManagerForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbOpcodes;
        private System.Windows.Forms.TextBox tbOpcode;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox tbOpcodeName;
        private System.Windows.Forms.Label label1;
    }
}