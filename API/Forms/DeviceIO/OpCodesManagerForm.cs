﻿using Auxiliary.Tools;
using Base.Devices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace API_UI.Forms.DeviceIO
{
    public enum eMode
    {
        AddNew,
        Edit,
    }

    public partial class OpCodesManagerForm : Form
    {
        private string fileName = Application.StartupPath + "\\" + "deviceio.oc"; 

        public OpCodeManager OpCodesManager { get; private set; }
        public BindingSource OpCodeBinder { get; private set; }

        public GenericDevice Device { get; private set; }
        private eMode mode;
        private OpCode current;

        public OpCodesManagerForm(GenericDevice device)
        {
            InitializeComponent();
            Device = device;
            initialize();
        }

        private void initialize()
        {
            if (!open())
                OpCodesManager = new OpCodeManager();

            mode = eMode.AddNew;

            OpCodeBinder = new BindingSource();
            OpCodeBinder.DataSource = OpCodesManager.OpCodes;
            lbOpcodes.DataSource = OpCodeBinder;
        }

        private bool open()
        {
            foreach (var item in Directory.GetFiles(".", "*.oc"))
            {
                FileInfo file = new FileInfo(item);

                if (file.Exists)
                {
                    using (IO fileIO = new IO(file.FullName, false))
                    {
                        OpCodesManager = fileIO.DecryptAndDeserialize<OpCodeManager>();
                    }

                    return true;
                }
            }

            return false;
        }

        private void save()
        {
            using (IO fileIO = new IO(fileName, true))
            {
                fileIO.FileName = fileName;
                fileIO.EncryptAndSerialize<OpCodeManager>(OpCodesManager);
            }

            tbOpcode.Text = string.Empty;
            tbOpcodeName.Text = string.Empty;
        }

        private eMode Mode
        {
            get { return mode; }
            set
            {
                switch (value)
                {
                    case eMode.AddNew:
                        btnAdd.Text = "Add New";
                        break;
                    case eMode.Edit:
                        btnAdd.Text = "Save";
                        break;
                    default:
                        break;
                }

                mode = value;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            switch (Mode)
            {
                case eMode.AddNew:
                    current = null;
                    addNew();
                    save();
                    break;
                case eMode.Edit:
                    edit();
                    break;
                default:
                    break;
            }
        }

        private void edit()
        {
            try
            {
                var newOpCode = generate();

                OpCodeBinder.Remove(current);
                OpCodeBinder.Add(newOpCode);
                Mode = eMode.AddNew;
            }
            catch
            {
                MessageBox.Show("Error editing the opcode,\nNo changes are made.");
            }
        }

        private void addNew()
        {
            try
            {
                OpCodeBinder.Add(generate());
            }
            catch
            {
                MessageBox.Show("Error generating new opcode,\nPlease check your inputs.");
            }
        }

        private OpCode generate()
        {
            string name = tbOpcodeName.Text;
            byte[] byteArr = DeviceIOForm.GetBytes(Device, tbOpcode.Text);

            if (name == null || name == string.Empty)
                throw new Exception();

            tbOpcode.Text = string.Empty;
            tbOpcodeName.Text = string.Empty;

            return new OpCode(name, byteArr, Device.OpCodes.OpCodeStartByte);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save();
            Close();
        }

        private void lbOpcodes_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int index = lbOpcodes.IndexFromPoint(e.Location);
                if (index != ListBox.NoMatches)
                {
                    updateCurrent(index);

                    ContextMenu contextMenu = new ContextMenu();

                    MenuItem menuItemEdit = new MenuItem("Edit");
                    menuItemEdit.Click += menuItemEdit_Click;
                    contextMenu.MenuItems.Add(menuItemEdit);

                    MenuItem menuItemDelete = new MenuItem("Delete");
                    menuItemDelete.Click += menuItemDelete_Click;
                    contextMenu.MenuItems.Add(menuItemDelete);

                    contextMenu.Show(lbOpcodes, e.Location, LeftRightAlignment.Right);
                }
            }
        }

        private void updateCurrent(int index)
        {
            try
            {
                current = (OpCode)lbOpcodes.Items[index];
            }
            catch { }
        }

        void menuItemDelete_Click(object sender, EventArgs e)
        {
            tbOpcode.Text = string.Empty;
            tbOpcodeName.Text = string.Empty;
            
            OpCodeBinder.Remove(current);
        }

        void menuItemEdit_Click(object sender, EventArgs e)
        {
            Mode = eMode.Edit;

            updateOpCodeView();
        }

        private void updateOpCodeView()
        {
            tbOpcode.Text = BitConverter.ToString(current.Data);
            tbOpcodeName.Text = current.Name;
        }

        private void lbOpcodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateCurrent(lbOpcodes.SelectedIndex);
                updateOpCodeView();
            }
            catch { }
        }

        private void OpCodesManagerForm_Shown(object sender, EventArgs e)
        {
            lbOpcodes_SelectedIndexChanged(sender, e);
        }
    }
}
