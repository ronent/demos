﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_UI.Forms.DeviceIO
{
    [Serializable]
    public class OpCodeManager 
    {
        public List<OpCode> OpCodes { get; private set; }

        public OpCodeManager()
        {
            OpCodes = new List<OpCode>();            
        }
    }
}
