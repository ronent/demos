﻿using API_UI.Forms.UserControls.Devices;
using API_UI.Forms.UserControls.Devices.DataNetDevices.DNL9x0;
using API_UI.Forms.UserControls.Devices.DataNetDevices.DNR900;
using API_UI.Forms.UserControls.Devices.MicroXDevices.EC8xx;
using API_UI.Forms.UserControls.Devices.MicroXDevices.ML2;
using API_UI.Forms.UserControls.Devices.MicroXDevices.PicoLite;
using API_UI.Forms.UserControls.Setup;
using API_UI.Forms.UserControls.Setup.PicoLite;
using Base.Devices;
//using DataNet910.Devices.V1;
//using DataNet920.Devices;
//using DataNetRepeater.Devices.V1;
using MicroLogPro2TH.Devices;
using System;
using System.Windows.Forms;
using API_UI.Forms.UserControls.Devices.SiMiDevices.SiMi;
using SiMiAPI.DataStructures;
using SiMiAPI.Devices;
using SiMiRH.Devices;

namespace API_UI.Forms
{
    public static class Factory
    {
        public static UserControlSetup GetSetupControl(GenericDevice device)
        {
            if (device is PicoLite.Devices.PicoLiteLogger)
            {
                if (device.ClassVersion == "V1")
                    return new PicoLiteV1Setup(device as PicoLite.Devices.PicoLiteLogger);
                else
                    return new PicoLiteV2Setup(device as PicoLite.Devices.PicoLiteLogger);
            }
            if (device is MicroLogAPI.Devices.MicroLiteLogger)
            {
                return new MicroLite2Setup(device as MicroLogAPI.Devices.MicroLiteLogger);
            }
            if (device is MicroLogAPI.Devices.MicroLog16BitLogger)
            {
                return new MicroLogSetup(device as MicroLogAPI.Devices.MicroLog16BitLogger);
            }
            
            return null;
        }

        public static UserControl GetDeviceControl(GenericDevice device)
        {
            if (device is PicoLite.Devices.PicoLiteLogger)
            {
                if (device.ClassVersion == "V1")
                    return new UserControlPicoLiteV1(device as PicoLite.Devices.V1.PicoLiteLogger);
                return new UserControlPicoLiteV2(device as PicoLite.Devices.V2.PicoLiteLogger);
            }
            if (device is MicroLogAPI.Devices.MicroLiteLogger)
            {
                return new UserControlML2(device as MicroLogAPI.Devices.MicroLiteLogger);
            }
            if (device is MicroLogPro2T.Devices.EC800B16Logger)
            {
                return new UserControlEC800(device as MicroLogPro2T.Devices.EC800B16Logger);
            }
            if (device is MicroLogPro2TH.Devices.EC850B16Logger)
            {
                return new UserControlEC850(device as MicroLogPro2TH.Devices.EC850B16Logger);
            }
//            if (device is DataNetRepeater.Devices.DNR900Device)
//            {
//                return new UserControlDNR900(device as DNR900Device);
//            }
//            if (device is DataNet910.Devices.DataNet910Logger)
//            {
//                return new UserControlDNL910(device as DataNet910Logger);
//            }
//            if (device is DataNet920.Devices.DataNet920Logger)
//             {
//                return new UserControlDNL920(device as DataNet920Logger);
//            }
           
            throw new Exception("Unsupported device type");
        }
    }
}
