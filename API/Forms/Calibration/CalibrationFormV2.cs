﻿using API_UI.Forms.UserControls.Devices;
using API_UI.Forms.UserControls.Devices.MicroXDevices;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Base.Sensors.Types.Temperature;
using MicroLogAPI.Devices;
using MicroXBase.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace API_UI.Forms.Calibration
{
    public delegate void SendCalibrationV2Delegate(CalibrationConfigurationV2 config);

    public partial class CalibrationFormV2 : Form
    {
        #region Members
        protected UserControlLoggerV2 parentUserConrolLogger;

        public event SendCalibrationV2Delegate OnSendCalibration;
        public event Action OnResetCalibration;

        #endregion
        #region Constructors
        public CalibrationFormV2(UserControlLoggerV2 userControlLogger)
        {
            parentUserConrolLogger = userControlLogger;

            InitializeComponent();
            Initialize();
        }

        public CalibrationFormV2()
        {
            InitializeComponent();
        }

        public virtual void Initialize()
        {
            var sensors = new List<GenericSensorV2>();

            if (Program.UserMode)
            {
                sensors.AddRange(parentUserConrolLogger.Logger.Sensors.GetAllFixed());
                sensors.Add(parentUserConrolLogger.Logger.Sensors.GetEnabledDetachable());
            }
            else
            {
                sensors = parentUserConrolLogger.Logger.Sensors.GetAll().ToList();
            }

            comboBoxSensors.Items.Clear();

            sensors.RemoveAll(sensor => sensor is DewPoint);

            foreach (GenericSensorV2 item in sensors)
            {
                if (item != null)
                {
                    comboBoxSensors.Items.Add(item.Name == null ? "" : item.Name); ;            
                }
            }

            if (comboBoxSensors.Items.Count > 0)
                comboBoxSensors.SelectedIndex = 0;
        }

        #endregion
        #region Properties
        public decimal Ref1
        {
            get { return Convert.ToDecimal(textBoxRef1.Text); }
            private set { textBoxRef1.Text = value.ToString(); }
        }

        public decimal Logger1
        {
            get { return Convert.ToDecimal(textBoxLogger1.Text); }
            private set { textBoxLogger1.Text = value.ToString(); }
        }

        public decimal Ref2
        {
            get { return Convert.ToDecimal(textBoxRef2.Text); }
            private set { textBoxRef2.Text = value.ToString(); }
        }

        public decimal Logger2
        {
            get { return Convert.ToDecimal(textBoxLogger2.Text); }
            private set { textBoxLogger2.Text = value.ToString(); }
        }

        public decimal Ref3
        {
            get 
            {
                decimal d = default(decimal);

                if (!string.IsNullOrEmpty(textBoxRef3.Text))
                {
                    d = Convert.ToDecimal(textBoxRef3.Text);
                }

                return d; 
            }

            private set { textBoxRef3.Text = value.ToString(); }
        }

        public decimal Logger3
        {
            get
            {
                decimal d = default(decimal);

                if (!string.IsNullOrEmpty(textBoxLogger3.Text))
                {
                    d = Convert.ToDecimal(textBoxLogger3.Text);
                }

                return d;
            }

            private set { textBoxLogger3.Text = value.ToString(); }
        }

        #endregion
        #region Events
        private void comboBoxSensors_SelectedIndexChanged(object sender, EventArgs e)
        {
            initializeTwoThreePoint(getSelectedSensor().Calibration.Points);
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            var logger = parentUserConrolLogger.Logger;

            var sensor = getSelectedSensor();
            
            LoggerReference[] pairs = 
            { 
                new LoggerReference { Logger = Logger1, Reference = Ref1},
                new LoggerReference { Logger = Logger2, Reference = Ref2},
                new LoggerReference { Logger = Logger3, Reference = Ref3},
            };

            try
            {
                CalibrationConfigurationV2 config = new CalibrationConfigurationV2(logger.Sensors);
                CalibrationCoefficients coefficients = sensor.GenerateCalibration(pairs);

                config.Add(sensor.Index, coefficients);

                if (OnSendCalibration != null)
                    OnSendCalibration.Invoke(config);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            if (OnResetCalibration != null)
                OnResetCalibration.Invoke();
        }

        #endregion
        #region Methods
        private void initializeTwoThreePoint(int p)
        {
            var sensor = getSelectedSensor();
            var range = Math.Abs(sensor.Minimum) + Math.Abs(sensor.Maximum);

            if (p == 2)
            {
                enableDisableThreePoint(false);
                textBoxRef1.Text = UserControlDevice.DecimalFormat(sensor.Minimum + (range / 3));
                textBoxRef2.Text = UserControlDevice.DecimalFormat(sensor.Minimum + (range / 3 * 2));
            }
            else
            {
                enableDisableThreePoint(true);
                textBoxRef1.Text = UserControlDevice.DecimalFormat(sensor.Minimum + (range / 4));
                textBoxRef2.Text = UserControlDevice.DecimalFormat(sensor.Minimum + (range / 4 * 2));
                textBoxRef3.Text = UserControlDevice.DecimalFormat(sensor.Minimum + (range / 4 * 3));

                textBoxLogger3.Text = textBoxRef3.Text;
            }

            textBoxLogger1.Text = textBoxRef1.Text;
            textBoxLogger2.Text = textBoxRef2.Text;
        }

        private void enableDisableThreePoint(bool i_ThreePoint)
        {
            textBoxLogger3.Visible = i_ThreePoint;
            textBoxRef3.Visible = i_ThreePoint;
            labelPoint3.Visible = i_ThreePoint;
        }

        private GenericSensorV2 getSelectedSensor()
        {
            IEnumerable<GenericSensorV2> sensors = parentUserConrolLogger.Logger.Sensors.GetAll();

            var enu = sensors.GetEnumerator();
            while (enu.MoveNext())
            {
                if (enu.Current.Name == (string)comboBoxSensors.SelectedItem)
                {
                    return enu.Current;
                }
            }

            return null;
        }

        #endregion
    }
}
