﻿namespace API_UI.Forms.Calibration
{
    partial class CalibrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxRef3 = new System.Windows.Forms.TextBox();
            this.labelPoint3 = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonSend = new System.Windows.Forms.Button();
            this.textBoxRef2 = new System.Windows.Forms.TextBox();
            this.textBoxRef1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSensors = new System.Windows.Forms.ComboBox();
            this.textBoxLogger3 = new System.Windows.Forms.TextBox();
            this.textBoxLogger2 = new System.Windows.Forms.TextBox();
            this.textBoxLogger1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxRef3
            // 
            this.textBoxRef3.Location = new System.Drawing.Point(80, 180);
            this.textBoxRef3.Name = "textBoxRef3";
            this.textBoxRef3.Size = new System.Drawing.Size(62, 20);
            this.textBoxRef3.TabIndex = 37;
            // 
            // labelPoint3
            // 
            this.labelPoint3.AutoSize = true;
            this.labelPoint3.Location = new System.Drawing.Point(12, 183);
            this.labelPoint3.Name = "labelPoint3";
            this.labelPoint3.Size = new System.Drawing.Size(47, 13);
            this.labelPoint3.TabIndex = 36;
            this.labelPoint3.Text = "Point #3";
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(149, 218);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(98, 23);
            this.buttonReset.TabIndex = 34;
            this.buttonReset.Text = "Reset Calibration";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(15, 218);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(98, 23);
            this.buttonSend.TabIndex = 33;
            this.buttonSend.Text = "Send Calibration";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // textBoxRef2
            // 
            this.textBoxRef2.Location = new System.Drawing.Point(80, 142);
            this.textBoxRef2.Name = "textBoxRef2";
            this.textBoxRef2.Size = new System.Drawing.Size(62, 20);
            this.textBoxRef2.TabIndex = 30;
            // 
            // textBoxRef1
            // 
            this.textBoxRef1.Location = new System.Drawing.Point(80, 101);
            this.textBoxRef1.Name = "textBoxRef1";
            this.textBoxRef1.Size = new System.Drawing.Size(62, 20);
            this.textBoxRef1.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Reference Value";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Point #2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Point #1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Sensor";
            // 
            // comboBoxSensors
            // 
            this.comboBoxSensors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSensors.FormattingEnabled = true;
            this.comboBoxSensors.Location = new System.Drawing.Point(65, 23);
            this.comboBoxSensors.Name = "comboBoxSensors";
            this.comboBoxSensors.Size = new System.Drawing.Size(187, 21);
            this.comboBoxSensors.TabIndex = 23;
            this.comboBoxSensors.SelectedIndexChanged += new System.EventHandler(this.comboBoxSensors_SelectedIndexChanged);
            // 
            // textBoxLogger3
            // 
            this.textBoxLogger3.Location = new System.Drawing.Point(185, 180);
            this.textBoxLogger3.Name = "textBoxLogger3";
            this.textBoxLogger3.Size = new System.Drawing.Size(62, 20);
            this.textBoxLogger3.TabIndex = 42;
            // 
            // textBoxLogger2
            // 
            this.textBoxLogger2.Location = new System.Drawing.Point(185, 142);
            this.textBoxLogger2.Name = "textBoxLogger2";
            this.textBoxLogger2.Size = new System.Drawing.Size(62, 20);
            this.textBoxLogger2.TabIndex = 41;
            // 
            // textBoxLogger1
            // 
            this.textBoxLogger1.Location = new System.Drawing.Point(185, 101);
            this.textBoxLogger1.Name = "textBoxLogger1";
            this.textBoxLogger1.Size = new System.Drawing.Size(62, 20);
            this.textBoxLogger1.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(182, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Logger Value";
            // 
            // CalibrationForm
            // 
            this.AcceptButton = this.buttonSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 255);
            this.Controls.Add(this.textBoxLogger3);
            this.Controls.Add(this.textBoxLogger2);
            this.Controls.Add(this.textBoxLogger1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxRef3);
            this.Controls.Add(this.labelPoint3);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBoxRef2);
            this.Controls.Add(this.textBoxRef1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxSensors);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalibrationForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Calibration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxRef3;
        private System.Windows.Forms.Label labelPoint3;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TextBox textBoxRef2;
        private System.Windows.Forms.TextBox textBoxRef1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxSensors;
        private System.Windows.Forms.TextBox textBoxLogger3;
        private System.Windows.Forms.TextBox textBoxLogger2;
        private System.Windows.Forms.TextBox textBoxLogger1;
        private System.Windows.Forms.Label label6;
    }
}