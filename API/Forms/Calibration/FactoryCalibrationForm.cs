﻿using API_UI.Forms.UserControls.Devices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace API_UI.Forms.Calibration
{
    public partial class FactoryCalibrationForm : CalibrationForm
    {
        public event Action OnFactoryReset;
        public event Action OnFactorySave;

        public FactoryCalibrationForm(UserControlLogger userControlLogger)
            :base(userControlLogger)
        {
            InitializeComponent();
        }

        public FactoryCalibrationForm()
            :base()
        {
            InitializeComponent();
        }

        public override void Initialize()
        {
            base.Initialize();

            if (Program.UserMode)
            {
                buttonSave.Visible = false;
            }
        }

        private void buttonRestore_Click(object sender, EventArgs e)
        {
            if (OnFactoryReset != null)
                OnFactoryReset.Invoke();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (OnFactorySave != null)
                OnFactorySave.Invoke();
        }
    }
}
