﻿using PicoLite.OpCodes.Helpers.V2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace API_UI.Forms
{
    public partial class PicoLiteTimeParser : Form
    {
        public PicoLiteTimeParser()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] arr = new byte[6];
                arr[0] = StringToByteArray(textBox1.Text)[0];
                arr[1] = StringToByteArray(textBox2.Text)[0];
                arr[2] = StringToByteArray(textBox3.Text)[0];
                arr[3] = StringToByteArray(textBox4.Text)[0];
                arr[4] = StringToByteArray(textBox5.Text)[0];
                arr[5] = StringToByteArray(textBox6.Text)[0];


                DateTime dt = PicoLiteV2Time.Parse(arr).ToDateTime();

                textBoxResult.Text = dt.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
