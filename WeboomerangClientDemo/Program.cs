﻿using ClientCommunication.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeboomerangClientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            DeviceManager manager = new DeviceManager();
            manager.Start();

            Console.ReadLine();
        }
    }
}
