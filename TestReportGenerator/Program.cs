﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReportsGenerator;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using Base.Devices;
using Base.Devices.Management;
using MicroXBase.Devices.Types;
using ReportsGenerator.DeviceDataObjects;
using ReportsGenerator.Generate;
using GrapeCity.ActiveReports;
using System.Data;
using ReportsGenerator.CalibrationDataObjects;
using Base.OpCodes.Helpers;
using Log4Tech;
using Newtonsoft.Json;
using ReportsGenerator.TestPicoLiteData;

namespace TestReportGenerator
{
    public enum eGenerateFrom
    {
        Dummy,
        API,
        Calibration,
        PicoLiteTester,
    }

    class Program
    {
        static ReportGenerator Report = new ReportGenerator();
        static string[] dummies = new string[] { "ML2RH16K.api" };

        static void Main(string[] args)
        {
            Report.OnFinished += report_OnFinished;
            ReportGenerator.OnProgress += report_OnProgress;


            generateFrom(eGenerateFrom.Calibration);
        }

        private static void generateFrom(eGenerateFrom method)
        {
            switch (method)
            {
                case eGenerateFrom.Dummy:
                    generateFromDummyDevice(dummies[0]);
                    break;
                case eGenerateFrom.API:
                    runSuperDeviceManager();
                    Console.ReadKey();
                    break;
                case eGenerateFrom.Calibration:
                    runCalibrationCertificate();
                    Console.ReadKey();
                    break;
                case eGenerateFrom.PicoLiteTester:
                    runPicoLiteTester();
                    Console.ReadKey();
                    break;
                default:
                    break;
            }
        }

        private static void generateFromDummyDevice(string path)
        {
            //Log.Log.WriteLine("Before de-serialize the file for report");
            GenericDevice device = DeserializeDevice(path);
            //Log.Log.WriteLine("Start generate the report");

            //var logger = device as MicroXDevice;
            //var sensor = logger.Sensors[Base.Sensors.Management.eSensorIndex.Temperature];
            //DateTime dt = DateTime.Now;

            //for (int i = 0; i < sensor.Samples.Count; i++)
            //{
            //    //sensor.Samples[i].Value = 50;
            //    sensor.Samples[i].Date = dt.AddHours(i);

            //    if (i % 1000 == 0)
            //    {
            //        sensor.Samples[i].Value = 35 + (i / 500);
            //    }
            //    else
            //    {
            //        sensor.Samples[i].Value = 10;
            //    }
            //}

            //generateReport(device);
        }

        private static void runSuperDeviceManager()
        {
            SuperDeviceManager.OnDeviceConnected += SuperDeviceManager_OnDeviceConnected;
            SuperDeviceManager.OnDeviceRemoved += SuperDeviceManager_OnDeviceRemoved;
            SuperDeviceManager.Start();
        }

        static void SuperDeviceManager_OnDeviceRemoved(object sender, Base.Devices.Management.EventsAndExceptions.ConnectionEventArgs<GenericDevice> e)
        {
            Report.Dispose();
        }

        private static void runPicoLiteTester()
        {
            List<FailResult> failedList = new List<FailResult>();

            failedList.Add(new FailResult()
            {
                Accuracy = "0.9",
                BatteryVoltage = "2.89",
                Button = "Pass",
                Firmware = "Pass",
                LED = "Pass",
                SerialNumber = "22222"
            });

            failedList.Add(new FailResult()
            {
                Accuracy = "Pass",
                BatteryVoltage = "Pass",
                Button = "Fail",
                Firmware = "Fail",
                LED = "Pass",
                SerialNumber = "33333"
            });

            ReportsGenerator.TestPicoLiteData.PicoLiteData data = new ReportsGenerator.TestPicoLiteData.PicoLiteData()
            {
                Accuracy = "±1 C from reference",
                BatteryVoltage = "2.99 V",
                FirmwareVersion = "1.11",
                Failed = "2",
                Passed = "5",
                PassedList = new List<string>() { "1", "2", "3", "4", "5" },
                FailedList = failedList,
                NumberOfDevices = "7"
            };

            Report.Generate(data);

        }

        private static void runCalibrationCertificate()
        {
            TestEquipment test = new TestEquipment()
            {
                CalibrationDate = DateTime.Now,
                Manufacturer = "Sam",
                Model = "Abce",
                IsNIST = true,
                ProductName = "Calibrator",
                SN = "12345"
            };
            List<SensorCalibration> sensors = new List<SensorCalibration>();
            sensors.Add(new SensorCalibration()
            {
                InputType = "Current 4-20 mA",
                PointNum = "Point 1",
                ReferenceValue = "5.13 mA",
                AcceptableTolerance = "+- 100 uA",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "15.38 mA",
                AcceptableTolerance = "+- 100 uA",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "Temperature PT-100",
                PointNum = "Point 1",
                ReferenceValue = "0 C",
                AcceptableTolerance = "+- 0.5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "350 C",
                AcceptableTolerance = "+- 1.5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "Temperature TC/J",
                PointNum = "Point 1",
                ReferenceValue = "0 C",
                AcceptableTolerance = "+- 0.5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "950 C",
                AcceptableTolerance = "+- 5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "Temperature TC/K",
                PointNum = "Point 1",
                ReferenceValue = "0 C",
                AcceptableTolerance = "+- 0.5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "950 C",
                AcceptableTolerance = "+- 5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "Temperature TC/T",
                PointNum = "Point 1",
                ReferenceValue = "0 C",
                AcceptableTolerance = "+- 0.5 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "350 C",
                AcceptableTolerance = "+- 2 C",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "Voltage 0-1 V",
                PointNum = "Point 1",
                ReferenceValue = "0 V",
                AcceptableTolerance = "+- 50 mV",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "1 V",
                AcceptableTolerance = "+- 50 mV",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "Voltage 0-50 mV",
                PointNum = "Point 1",
                ReferenceValue = "0 mV",
                AcceptableTolerance = "+- 250 mV",
                LoggerValue = "",
            });
            sensors.Add(new SensorCalibration()
            {
                InputType = "",
                PointNum = "Point 2",
                ReferenceValue = "50 V",
                AcceptableTolerance = "+- 250 mV",
                LoggerValue = "",
            });

            var data = new CalibrationData(test, sensors)
            {
                CalibrationDate = DateTime.Now,
                CertificateIssueDate = DateTime.Now,
                DeviceSN = "2001203",
                EnvironmentHumidity = 15,
                EnvironmentTemperature = 25.4,
                ModelName = "PicoLite",
                PartNumber = "12345",
                TechnicianName = "Ronen"
            };


            var reportSections = new List<eReportSections>
            {
                eReportSections.CalibrationInfoArgs,
                eReportSections.CalibrationEquipmentArgs,
                eReportSections.CalibrationDataArgs,
                eReportSections.CalibrationSummaryArgs
            };

            Report.Generate(data, reportSections);
        }

        static void SuperDeviceManager_OnDeviceConnected(object sender, Base.Devices.Management.EventsAndExceptions.ConnectionEventArgs<GenericDevice> e)
        {
            if (e.Device is MicroXLogger)
            {
                var device = e.Device as MicroXLogger;
                device.Functions.OnDownloadCompleted += device_OnDownloadCompleted;
                device.Functions.Download();
            }
        }

        static void device_OnDownloadCompleted(object sender, Base.OpCodes.Helpers.DownloadCompletedArgs args)
        {
            //SerializeDevice(SuperDeviceManager.FindDevice(args.SerialNumber), dummies[0]);
            generateReport(args.Logger);
        }

        private static void generateReport(GenericDevice device)
        {
            DeviceData data = new DeviceData(device);

            if (device is MicroXLogger)
                if (!(device as MicroXLogger).Status.Boomerang.CelsiusMode)
                    data.ReportParams.TemperatureUnit = eTemperatueUnit.Fahrenheit;

            Report.GenerateBommerang(data);
        }

        static void report_OnProgress(ReportProgressArgs args)
        {
            //Console.WriteLine("Report progress {0}%",args.Precent);
        }

        static void report_OnFinished(ReportFinishArgs args)
        {
            //Log.Log.WriteLine("Report has finished!");
            args.streamPDF.Position = 0;
            args.streamRDF.Position = 0;

            string pdfFile = string.Format("report_{0}_{1}.pdf", args.SerialNumber, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
            using (FileStream file = new FileStream(pdfFile, FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[args.streamPDF.Length];
                args.streamPDF.Read(bytes, 0, (int)args.streamPDF.Length);
                file.Write(bytes, 0, bytes.Length);
                args.streamPDF.Close();
            }
            //open the PDF file with the native app
            // Process.Start(pdfFile);
            //digital sign the pdf
            ReportDigitalSignArgs signatureArgs = new ReportDigitalSignArgs()
            {
                //Comment = , //taken from the comment db field
                FullName = "", //taken from the user full name db field
                IsSignaturePageExists = true, //indication from db if has at least one signature
                IsTSARequired = true, //always true for time verfication
                SavedReport = args.streamRDF, //the saved binary report from db
                SignatureImage = new System.IO.FileStream("Fourtec_Wax_Seal.bmp", FileMode.Open, FileAccess.Read), //the image for the signature (if user doesn't have take default - fourtec logo)
                SignatureReason = "",//  string.Concat("Report was ", ReportGenerator.SignatureReason.Approved, ". ", " comment..."), //taken from process type in db
                SignatureRectangle = new System.Drawing.RectangleF(0.4F, 10F, 6.0F, 0.9F), //x and y are from db (default value if this is the first time)
                UserName = "", //the username from db
                IsUTCRequired = false,
            };
            Stream stream = ReportGenerator.DigitalSignPDF(signatureArgs);
            stream.Seek(0, SeekOrigin.Begin);
            convert2Pdf(args, stream);

        }

        private static void convert2Pdf(ReportFinishArgs args, Stream stream)
        {
            stream.Position = 0;
            string pdfFile = string.Format("report_signed_{0}_{1}.pdf", args.SerialNumber, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
            using (FileStream file = new FileStream(pdfFile, FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                file.Write(bytes, 0, bytes.Length);
                stream.Close();
            }
            
            Process.Start(pdfFile);
        }

        public static void SerializeDevice(GenericDevice device, string path)
        {
            Stream stream = File.Open(path, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();

            bformatter.Serialize(stream, device);
            stream.Close();
        }

        public static GenericDevice DeserializeDevice(string path)
        {
            Stream stream = File.Open(path, FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();

            GenericDevice device = (GenericDevice)bformatter.Deserialize(stream);
            stream.Close();
            return device;
        }
    }
}