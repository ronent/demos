﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Log4Tech;
using PicoLite.Devices;

namespace PicoLite2Fixer
{
    internal class Manager
    {
        public void Start()
        {
            SuperDeviceManager.OnDeviceConnected +=SuperDeviceManager_OnDeviceConnected;
            SuperDeviceManager.OnDeviceRemoved +=SuperDeviceManager_OnDeviceRemoved;
            SuperDeviceManager.Start();
        }

        void SuperDeviceManager_OnDeviceRemoved(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            Log.Instance.Write("Device {0} is disconnected", this, Log.LogSeverity.DEBUG, e.Device.DeviceTypeName);
            if (e.Device is PicoLiteLogger) 
                Console.WriteLine("PicoLite #{0} is disconnected", e.SerialNumber);
        }

        void SuperDeviceManager_OnDeviceConnected(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            Log.Instance.Write("Device {0} is connected", this, Log.LogSeverity.DEBUG, e.Device.DeviceTypeName);
            if (!(e.Device is PicoLiteLogger)) return;
            Log.Instance.Write("Check if PicoLite #{0} is V1", this, Log.LogSeverity.DEBUG, e.Device.Status.SerialNumber);

            if (e.Device.ClassVersion == "V1")
                Task.Factory.StartNew(() => checkFirmwareValid(e.SerialNumber));
            else
            //if valid (after firmware update) then send setup and deep sleep
                Task.Factory.StartNew(() => SendSetupAndDeepSleep(e.SerialNumber));
        }

        private PicoLiteLogger GetPicolLite(string serialNumber)
        {
            try
            {
                //try to locate the device and get his status
                var genericDevice = SuperDeviceManager.GetDevice(serialNumber);
                //if found and it is picolite logger then return it
                if (genericDevice is PicoLiteLogger)
                    return genericDevice as PicoLiteLogger;
            }
            catch
            {
                // ignored
            }
            return null;
        }

        private void checkFirmwareValid(string serialNumber)
        {
            Log.Instance.Write("PicoLite #{0} is V1, try to update firmware", this, Log.LogSeverity.DEBUG, serialNumber);
            Console.WriteLine("PicoLite #{0} is updating. [PLEASE WAIT, SETTING UP THE DEVICE]", serialNumber);
            try
            {
                //get the picolite first
                var picolite = GetPicolLite(serialNumber);
                if (picolite == null) return;
                //checks if the firmware is the right version
                if (picolite.Status.FirmwareVersion != Version.Parse("1.11.11"))
                {
                    Log.Instance.Write("Sending firmware update to picolite #{0}", this, Log.LogSeverity.INFO, serialNumber);
                    //if not then update firmware
                    var result = picolite.Functions.UploadFirmware().Result;
                    if(result.IsOK)
                        Log.Instance.Write("Picolite #{0} firmware was updated OK", this, Log.LogSeverity.INFO, serialNumber);
                    else
                        Log.Instance.Write("Fail to update picolite #{0} firmware , messageLog: {1}, exception: {2}", this, Log.LogSeverity.INFO, serialNumber,result.MessageLog,result.LastException);
                }
            }
            catch
            {
                // ignored
            }
        }

        private void SendSetupAndDeepSleep(string serialNumber)
        {
            Log.Instance.Write("Sending default setup to picolite #{0}", this, Log.LogSeverity.INFO, serialNumber);
            Console.WriteLine("PicoLite #{0} is in progress. [PLEASE WAIT, SETTING UP THE DEVICE]", serialNumber);
            try
            {
                //get the picolite
                var picolite = (SuperDeviceManager.GetDevice(serialNumber) as PicoLite.Devices.V2.PicoLiteLogger);
                if (picolite == null) return;
                //checks first if the logger is running
                if (picolite.Functions.IsRunning)
                    picolite.Functions.Stop().Wait();
                
                //setup configuration
                var picoliteSetup = new PicoLite.DataStructures.V2.PicoLiteV2SetupConfiguration
                {
                    SerialNumber = serialNumber,
                    BoomerangEnabled = false,
                    Comment = ConfigurationManager.AppSettings["comment"],
                    CyclicMode = Convert.ToBoolean(ConfigurationManager.AppSettings["cyclicMode"]),
                    FahrenheitMode = Convert.ToBoolean(ConfigurationManager.AppSettings["fahrenheitMode"]),
                    Interval = new TimeSpan(0, Convert.ToInt32(ConfigurationManager.AppSettings["interval"]), 0),
                    TimerRunEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["timerRunEnabled"]),
                    StopOnKeyPress = Convert.ToBoolean(ConfigurationManager.AppSettings["stopOnKeyPress"]),
                    PushToRunMode = Convert.ToBoolean(ConfigurationManager.AppSettings["pushToRunMode"])
                };
                //sensor configuration
                picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = Convert.ToBoolean(ConfigurationManager.AppSettings["alarmEnabled"]);
                picoliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(ConfigurationManager.AppSettings["alarmLow"]);
                picoliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(ConfigurationManager.AppSettings["alarmHigh"]);
                picoliteSetup.RunDelay = Convert.ToUInt16(ConfigurationManager.AppSettings["runDelay"]);

                //send setup 
                var result = picolite.Functions.SendSetup(picoliteSetup).Result;
                //if was success
                if (result.IsOK)
                    //send deep sleep
                    result = picolite.Functions.DeepSleep().Result;
               
                //if fail then document it
                if (!result.IsOK)
                    Log.Instance.Write("Fail to put picolite #{0} to deep sleep with error {1}", this,
                        Log.LogSeverity.ERROR, serialNumber, result.ToString());
                else
                {
                    //went this far, cool :-) 
                    Log.Instance.Write("Picolite #{0} is ready!", this, Log.LogSeverity.INFO, serialNumber);
                    Console.WriteLine("PicoLite #{0} IS READY. [IT IS NOW SAFE TO DISCONNECT THE DEVICE]", serialNumber);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in SendSetupAndDeepSleep", this, ex);
            }
        }
    }
}
