﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite2Fixer
{
     class Program
    {
        static int Main(string[] args)
        {
            Log4Tech.Log.Instance.IsWrite2Cloud = false;
            Log4Tech.Log.Instance.IsWrite2Console = false;
            Log4Tech.Log.Instance.IsWrite2File = true;
            Console.WriteLine("Please insert PicoLite ...");
            var lic = new LicenseAPI.Manager();
            var executablePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var licensePath = string.Format("{0}\\license.lic",executablePath);
            if (!File.Exists(licensePath) || lic.IsLicenseExpired(licensePath))
            {
                Log4Tech.Log.Instance.Write("License is not valid!", "Main");
                Console.WriteLine("License is not valid!");
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
                return 0;
            }
            var manager = new Manager();
            manager.Start();

            Console.Read();
            return 1;
        }
    }
}
