﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace MongoDbSearch.UI
{
    public delegate void ConnectDelegation(string ip);

    public partial class ucLogin : UserControl
    {
        public event ConnectDelegation OnConnect;

        public ucLogin()
        {
            InitializeComponent();
            init();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (OnConnect != null)
                OnConnect(getSelectedServer());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void init()
        {
            var section = ConfigurationManager.AppSettings.GetEnumerator();

            cmbServer.Items.Clear();
            while (section.MoveNext())
            {
                string item  = section.Current.ToString();
                cmbServer.Items.Add(ConfigurationManager.AppSettings.Get(item));
            }
        }

        private string getSelectedServer()
        {
            if (this.cmbServer.InvokeRequired)
                return (string)this.cmbServer.Invoke(new Func<string>(() => getSelectedServer()));
            else
                return this.cmbServer.SelectedItem.ToString();
        }
    }
}
