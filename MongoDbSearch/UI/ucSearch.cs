﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDbSearch.UI
{
    public delegate void SearchDelegation(SearchEventArgs args);

    public partial class ucSearch : UserControl
    {
        public event SearchDelegation OnSearchClick;

        public ucSearch()
        {
            InitializeComponent();
            fillSensorTypes();
        }

        private void fillSensorTypes()
        {
            ComboboxItem item = new ComboboxItem();
            item.Text = "Internal NTC";
            item.Value = 1;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "DigitalTemperature";
            item.Value =2;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "Humidity";
            item.Value = 3;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "DewPoint";
            item.Value = 4;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "Current4_20mA";
            item.Value = 5;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "Voltage0_10V";
            item.Value = 6;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "ExternalNTC";
            item.Value = 7;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "PT100";
            item.Value = 8;

            cmbSensorType.Items.Add(item);

            item = new ComboboxItem();
            item.Text = "Custom Sensor";
            item.Value = 9;

            cmbSensorType.Items.Add(item);

        }

        public void fillResult(List<dynamic> result)
        {
            this.dgResults.Rows.Clear();
            lblCount.Text = result.Count.ToString();
            result.ForEach((x) =>
                {
                    Sample sample = (Sample)x;
                    this.dgResults.Rows.Add(sample.SerialNumber, sample.Value, sample.SensorTypeID, sample.SampleTime);
                });

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (OnSearchClick != null)
            {
                dgResults.Rows.Clear();
                int serialNumber = 0;
                if (int.TryParse(txtSN.Text, out serialNumber))

                    OnSearchClick(new SearchEventArgs()
                        {
                            SerailNumber = serialNumber,
                            SensorType = Convert.ToInt32(((ComboboxItem)cmbSensorType.SelectedItem).Value),
                            From = dtFrom.Checked? Convert.ToInt32(dtFrom.Value.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds):0,
                            To = dtTo.Checked? Convert.ToInt32(dtTo.Value.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds):0,
                        });

            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dgResults.Rows.Clear();
            txtSN.Text = string.Empty;
            cmbSensorType.SelectedIndex = -1;
            dtFrom.Value = DateTime.Now;
            dtTo.Value = DateTime.Now;
            dtFrom.Checked = false;
            dtTo.Checked = false;
            lblCount.Text = "0";
        }
    }
}
