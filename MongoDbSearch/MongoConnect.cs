﻿using Infrastructure;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDbSearch
{
    public class MongoConnect
    {
        private MongoClient mongoClient;
        private MongoServer mongoServer;
        private MongoDatabase mongoDatabase;
        string connectionString = string.Empty;

        public MongoConnect(string ip)
        {
            connectionString = string.Format("mongodb://{0}:27017",ip);
            initialize();
        }

        public bool IsConnected()
        {
            return true;
        }

        private void initialize()
        {
            mongoClient = new MongoClient(connectionString);
            mongoServer = mongoClient.GetServer();
            mongoDatabase = mongoServer.GetDatabase("Loggers");
        }

        internal List<dynamic> Find(int serialNumber, int sensorTypeID)
        {
            try
            {
                MongoQuery<Sample> query = new MongoQuery<Sample>(mongoDatabase);
                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
                var result = query.Execute();//.SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(150);
                var x = result.OrderBy(a => a.SampleTime).ToList();
                return x.ToList<dynamic>();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Find", this, ex);
            }
            return null;
        }

        internal List<dynamic> Find(int serialNumber, int sensorTypeID, bool lastDownloadData = true, short alarmStatus = 0)
        {
            try
            {
                MongoQuery<Sample> query = new MongoQuery<Sample>(mongoDatabase);
                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);

                //checks if need only the last download data
                if (lastDownloadData)
                {
                    //get the last download start and end time
                    var lastDownloadParams = DataManager.Instance.GetLastSensorDownload(serialNumber, sensorTypeID);
                    //checks if found any
                    if (lastDownloadParams != null
                        && ((IDictionary<string, Object>)lastDownloadParams).ContainsKey("FirstDownloadTime")
                        && ((IDictionary<string, Object>)lastDownloadParams).ContainsKey("LastDownloadTime"))
                    {
                        //add to the condition
                        query.AddParameter("SampleTime", Convert.ToInt32(lastDownloadParams.FirstDownloadTime), MongoQuery<Sample>.QueryCond.GTE);
                        query.AddParameter("SampleTime", Convert.ToInt32(lastDownloadParams.LastDownloadTime), MongoQuery<Sample>.QueryCond.LTE);
                        query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);
                    }
                    else
                    {
                        query.AddParameter("SampleTime", 1, MongoQuery<Sample>.QueryCond.GTE);
                        query.AddParameter("SampleTime", 1999999999, MongoQuery<Sample>.QueryCond.LTE);
                        query.AddParameter("IsDummy", 0, MongoQuery<Sample>.QueryCond.EQ);
                    }
                }
                if (alarmStatus > 0)
                {
                    query.AddParameter("AlarmStatus", alarmStatus, MongoQuery<Sample>.QueryCond.EQ);
                }

                var result = query.Execute();//.SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(150);
                var x = result.OrderBy(a => a.SampleTime).ToList();
                return x.ToList<dynamic>();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Find", this, ex);
            }
            return null;
        }

        internal List<dynamic> Find(int serialNumber, int sensorTypeID, int startTime, int endTime, short alarmStatus = 0)
        {
            try
            {
                MongoQuery<Sample> query = new MongoQuery<Sample>(mongoDatabase);
                query.AddParameter("SerialNumber", serialNumber, MongoQuery<Sample>.QueryCond.EQ);
                query.AddParameter("SensorTypeID", sensorTypeID, MongoQuery<Sample>.QueryCond.EQ);
                query.AddParameter("SampleTime", startTime, MongoQuery<Sample>.QueryCond.GTE);
               // query.AddParameter("SampleTime", endTime, MongoQuery<Sample>.QueryCond.LTE);
                if (alarmStatus > 0)
                {
                    query.AddParameter("AlarmStatus", alarmStatus, MongoQuery<Sample>.QueryCond.EQ);
                }

                //query.prepareCondition();


                var result = query.Execute();//.SetSortOrder(SortBy.Descending("SampleTime")).SetLimit(150);
                var x = result.OrderBy(a => a.SampleTime).ToList();
                return x.ToList<dynamic>();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Find", this, ex);
            }
            return null;
        }
    }
}
