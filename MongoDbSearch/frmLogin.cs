﻿using Infrastructure;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDbSearch
{
    public partial class frmLogin : Form
    {
        public string MongoDNS { get; private set; }

        public frmLogin()
        {
            InitializeComponent();
            
        }

        void ucLogin1_OnConnect(string ip)
        {
            MongoDNS = ip;
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            ucLogin2.OnConnect += ucLogin1_OnConnect;
        }

        
    }
}
