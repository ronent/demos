﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDbSearch
{
    public partial class frmMain : Form
    {
        private MongoConnect mongoConnect;

        public frmMain()
        {
            InitializeComponent();
        }

        public void Init(MongoConnect mongo)
        {
            this.mongoConnect = mongo;
        }

        private void ucSearch1_Load(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ucSearch1.OnSearchClick += ucSearch1_OnSearchClick;
        }

        private void ucSearch1_OnSearchClick(SearchEventArgs args)
        {
            List<dynamic> result = null;
         
            if (args.From > 0)
                result = mongoConnect.Find(args.SerailNumber, args.SensorType, args.From, args.To);
            else
                result = mongoConnect.Find(args.SerailNumber, args.SensorType);

            if (result != null && result.Count > 0)
                ucSearch1.fillResult(result);
            else
                MessageBox.Show("No results!");

        }
    }
}
