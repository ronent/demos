﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDbSearch
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Log4Tech.Log.Instance.IsWrite2Cloud = false;
            Log4Tech.Log.Instance.IsWrite2Console = true;
            Log4Tech.Log.Instance.IsWrite2File = true;

            frmLogin login = new frmLogin();
            var result = login.ShowDialog();

            MongoConnect conn = new MongoConnect(login.MongoDNS);

            frmMain main=null;

            if (result == System.Windows.Forms.DialogResult.OK)
            //Application.Run(new frmMain()); 
            {
                main = new frmMain();
                main.Init(conn);
                main.ShowDialog();
            }
                
        }
    }
}
