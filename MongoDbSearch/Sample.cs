﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDbSearch
{
    [BsonIgnoreExtraElements]
    public class Sample
    {
        [BsonElement("SerialNumber")]
        public Int32 SerialNumber { get; set; }
        [BsonElement("SensorTypeID")]
        public Int32 SensorTypeID { get; set; }
        [BsonElement("Value")]
        public double Value { get; set; }
        [BsonElement("SampleTime")]
        public int SampleTime { get; set; }
        [BsonElement("IsTimeStamp")]
        public bool IsTimeStamp { get; set; }
        [BsonElement("TimeStampComment")]
        public string TimeStampComment { get; set; }
        [BsonElement("AlarmStatus")]
        public Int16 AlarmStatus { get; set; }
        [BsonElement("IsDummy")]
        public Int16 IsDummy { get; set; }
    }
}
