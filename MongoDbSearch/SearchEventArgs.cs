﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDbSearch
{
    public class SearchEventArgs
    {
        public int SerailNumber { get; set; }
        public int SensorType { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }
}
