﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLiteQCTester
{
    public class ReportProfile
    {
        public ReportProfile()
        {
            this.TestStarted = DateTime.Now;
            this.IsBatteryPass = true;
            this.IsDisconnectLEDRedPass = true;
            this.IsFirmwarePass = true;
            this.IsPCBPass = true;
            this.IsPushToRunPass = true;
            this.IsSensorPass = true;
        }

        public string SerialNumber { get; set; }
        public decimal LoggerSampleValue { get; set; }
        public bool IsFirmwarePass { get; set; }
        public bool IsPCBPass { get; set; }
        public bool IsBatteryPass { get; set; }
        public bool IsDisconnectLEDRedPass { get; set; }
        public bool IsPushToRunPass { get; set; }
        public bool IsSensorPass { get; set; }
        public DateTime TestStarted { get; set; }
        public DateTime TestEnded { get; set; }
        public byte BatteryValue { get; set; }
        public Version FirmwareVersion { get; set; }
        public byte PCBAssembly { get; set; }
    }
}
