﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PicoLiteQCTester
{
    class Utilities
    {
        #region Messages
        public readonly string TEST_FAILED = "Test Failed !";
        public readonly string CONNECT_PICOLITE_MSG = "Please connect logger to start new test";
        public readonly string DISCONNECT_PICOLITE_MSG = "Please disconnect the logger";
        public readonly string DISCONNECTION_RED_FLASH_MSG = "Please disconnect the logger and verify that LED flashed red three times upon disconnection";
        public readonly string PUSH2RUN_MSG = "Press button till green LED starts flashing, and verify it flashes for 5 seconds";
        public readonly string RECONNECT_MSG = "Please reconnect logger";
        public readonly string QCFINISHED_MSG = "PicoLite has passed the QC" + Environment.NewLine + "You can now disconnect it from PC and connect the next PicoLite";
        public readonly string TEST_FAILED_SENSOR = "Test Failed !" + Environment.NewLine + "Sensor readings error.";
        public readonly string REPORT_SAVED_MSG = "Report for all PicoLites was saved successfully";
        public readonly string REPORT_FAIL_SAVED_MSG = "Reports wasn't saved!";
        #endregion
        #region Properties
        public readonly string PICOLITE_DEFAULT_COMMENT = "Fourtec";
        public int PCB_Version { get; internal set; }
        public Version PicoLiteFirmwareVersion { get; internal set; }

        public byte Battery { get; private set; }
        public int Low { get; private set; }
        public int High { get; private set; }

        #endregion
        #region singleton
        private static readonly Lazy<Utilities> lazy = new Lazy<Utilities>(() => new Utilities());

        public static Utilities Instance
        {
            get { return lazy.Value; }
        }

        private Utilities() 
        {
            Battery = Convert.ToByte(ConfigurationManager.AppSettings["battery"]);
            Low = Convert.ToInt32(ConfigurationManager.AppSettings["low"]);
            High = Convert.ToInt32(ConfigurationManager.AppSettings["high"]);
        }

        #endregion
        #region Methods
        public bool ValidateNumOnly(KeyEventArgs e)
        {
            if (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9 ||
                e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9 ||
                e.KeyCode == Keys.Back ||
                e.KeyCode == Keys.Delete)
                return true;

            return false;
        }

        static string reportFileName = string.Format("Reports\\PicoLite_QC_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd_HH-mm"));

        public bool SaveReports(List<ReportProfile> Reports)
        {
            try
            {
                //checks if have reports to save 
                if (Reports != null && Reports.Count > 0)
                {
                    if (!Directory.Exists("Reports"))
                        Directory.CreateDirectory("Reports");

                    using (StreamWriter sw = new StreamWriter(reportFileName))
                    {
                        //print header of report
                        //***********************
                        sw.WriteLine("PicoLite QC Test :");
                        sw.WriteLine("-----------------------------");
                        //how much process 
                        sw.WriteLine("Tested : {0}", Reports.Count);
                        //how much pass
                        var passed  = (from i in Reports
                                       where 
                                        i.IsBatteryPass == true &&
                                        i.IsDisconnectLEDRedPass == true &&
                                        i.IsFirmwarePass == true &&
                                        i.IsPCBPass == true &&
                                        i.IsPushToRunPass == true &&
                                        i.IsSensorPass == true
                                       select i);
                                      

                        if (passed.Count() > 0)
                        {
                            sw.WriteLine("Passed : {0}\r\n", passed.Count());
                            sw.WriteLine("\r\nPassed Loggers Summary : ");
                            sw.WriteLine("-------------------------------------");

                            var list = passed.ToList();
                            for (int i = 0; i < list.Count(); i++ )
                            {
                                sw.Write("SN #{0}", list[i].SerialNumber);
                                if (i != 0 && i % 5 == 0)
                                    sw.WriteLine(" ");
                                else
                                    sw.Write(", ");
                            }
                        }
                    

                        //details of those who fail
                        //***************************
                        //which was failed
                        var failed = from i in Reports
                                     where 
                                        i.IsBatteryPass ==false ||
                                        i.IsDisconnectLEDRedPass == false ||
                                        i.IsFirmwarePass==false ||
                                        i.IsPCBPass==false ||
                                        i.IsPushToRunPass == false ||
                                        i.IsSensorPass == false
                                     select i;
                        if (failed.Count() > 0)
                        {
                            sw.WriteLine("\r\nFailing Loggers Summary : ");
                            sw.WriteLine("-------------------------------------");

                            failed.ToList().ForEach((i) =>
                                {
                                    if (i.IsBatteryPass == false)
                                        sw.WriteLine("SN #{0} failed on Battey Check with value of {1}", i.SerialNumber, i.BatteryValue);
                                    else if (i.IsDisconnectLEDRedPass == false)
                                        sw.WriteLine("SN #{0} failed on Disconnection LED Red Flash Check", i.SerialNumber);
                                    else if (i.IsFirmwarePass == false)
                                        sw.WriteLine("SN #{0} failed on Firmware Version Check with value of {1}", i.SerialNumber, i.FirmwareVersion);
                                    else if (i.IsPCBPass == false)
                                        sw.WriteLine("SN #{0} failed on PCB Assembly Check with value of {1}", i.SerialNumber, i.PCBAssembly);
                                    else if (i.IsPushToRunPass == false)
                                        sw.WriteLine("SN #{0} failed on Push To Run Green Flash Check", i.SerialNumber);
                                    else if (i.IsSensorPass == false)
                                        sw.WriteLine("SN #{0} failed on Sensor Check, Sample value: {1}", i.SerialNumber,           i.LoggerSampleValue);
                                });
                        }
                        //summarize of failing count type
                        //********************************
                        sw.WriteLine("\r\nStatistics Summary : ");
                        sw.WriteLine("------------------------------");
                        if (failed.Count() > 0)
                        {
                            sw.WriteLine(" - Total failed {0} / {1}", failed.Count(), Reports.Count);
                        }
                        double avgTime = calculateAvgTestTime(Reports);
                        sw.WriteLine(" - Average test time : {0} (sec.)", avgTime);
                        
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in save report", this, ex);
            }
            return false;
        }

        private double calculateAvgTestTime(List<ReportProfile> Reports)
        {
            var sum = (from i in Reports
                       select i.TestEnded.Subtract(i.TestStarted).TotalSeconds).Sum();

            return Math.Round(sum / Reports.Count(),2);

        }

        public void SerializeObject(List<ReportProfile> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(List<ReportProfile>));
            using (var stream = File.OpenWrite(fileName))
            {
                serializer.Serialize(stream, list);
            }
        }

        public void Deserialize(List<ReportProfile> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(List<ReportProfile>));
            using (var stream = File.OpenRead(fileName))
            {
                var other = (List<ReportProfile>)(serializer.Deserialize(stream));
                list.Clear();
                list.AddRange(other);
            }
        }

        #endregion
    }
}