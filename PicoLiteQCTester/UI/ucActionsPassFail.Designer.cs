﻿namespace PicoLiteQCTester.UI
{
    partial class ucActionsPassFail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessages = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblMessages
            // 
            this.lblMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMessages.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblMessages.Location = new System.Drawing.Point(0, 0);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(494, 268);
            this.lblMessages.TabIndex = 0;
            this.lblMessages.Text = "Please verify that LED flashed red three times upon disconnection";
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucActionsPassFail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblMessages);
            this.Name = "ucActionsPassFail";
            this.Size = new System.Drawing.Size(494, 268);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessages;
    }
}
