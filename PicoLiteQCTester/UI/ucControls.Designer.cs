﻿namespace PicoLiteQCTester.UI
{
    partial class ucControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnFail = new System.Windows.Forms.Button();
            this.btnPass = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnStart.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnStart.Location = new System.Drawing.Point(108, 9);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(106, 45);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnFail
            // 
            this.btnFail.BackColor = System.Drawing.Color.Tomato;
            this.btnFail.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFail.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnFail.Location = new System.Drawing.Point(212, 9);
            this.btnFail.Name = "btnFail";
            this.btnFail.Size = new System.Drawing.Size(106, 45);
            this.btnFail.TabIndex = 4;
            this.btnFail.Text = "Fail";
            this.btnFail.UseVisualStyleBackColor = false;
            this.btnFail.Visible = false;
            this.btnFail.Click += new System.EventHandler(this.btnFail_Click_1);
            // 
            // btnPass
            // 
            this.btnPass.BackColor = System.Drawing.Color.LimeGreen;
            this.btnPass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnPass.Location = new System.Drawing.Point(3, 9);
            this.btnPass.Name = "btnPass";
            this.btnPass.Size = new System.Drawing.Size(106, 45);
            this.btnPass.TabIndex = 3;
            this.btnPass.Text = "Pass";
            this.btnPass.UseVisualStyleBackColor = false;
            this.btnPass.Visible = false;
            this.btnPass.Click += new System.EventHandler(this.btnPass_Click_1);
            // 
            // ucControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnFail);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnPass);
            this.Name = "ucControls";
            this.Size = new System.Drawing.Size(320, 63);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnFail;
        private System.Windows.Forms.Button btnPass;
    }
}
