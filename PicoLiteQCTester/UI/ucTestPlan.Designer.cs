﻿namespace PicoLiteQCTester.UI
{
    partial class ucTestPlan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.picBatteryFail = new System.Windows.Forms.PictureBox();
            this.picBatteryPass = new System.Windows.Forms.PictureBox();
            this.picPCBFail = new System.Windows.Forms.PictureBox();
            this.picPCBPass = new System.Windows.Forms.PictureBox();
            this.picFWFail = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.picFWPass = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBatteryFail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBatteryPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPCBFail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPCBPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFWFail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFWPass)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.80712F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.37685F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.4095F));
            this.tableLayoutPanel1.Controls.Add(this.picBatteryFail, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.picBatteryPass, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.picPCBFail, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.picPCBPass, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.picFWFail, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.picFWPass, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 14);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.6875F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.03125F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.28125F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(336, 176);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // picBatteryFail
            // 
            this.picBatteryFail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picBatteryFail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picBatteryFail.Image = global::PicoLiteQCTester.Properties.Resources.Green_Checkmark1_500x420;
            this.picBatteryFail.Location = new System.Drawing.Point(258, 134);
            this.picBatteryFail.Name = "picBatteryFail";
            this.picBatteryFail.Size = new System.Drawing.Size(65, 28);
            this.picBatteryFail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picBatteryFail.TabIndex = 11;
            this.picBatteryFail.TabStop = false;
            this.picBatteryFail.Visible = false;
            // 
            // picBatteryPass
            // 
            this.picBatteryPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picBatteryPass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picBatteryPass.Image = global::PicoLiteQCTester.Properties.Resources.Green_Checkmark1_500x420;
            this.picBatteryPass.Location = new System.Drawing.Point(165, 134);
            this.picBatteryPass.Name = "picBatteryPass";
            this.picBatteryPass.Size = new System.Drawing.Size(65, 28);
            this.picBatteryPass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picBatteryPass.TabIndex = 10;
            this.picBatteryPass.TabStop = false;
            this.picBatteryPass.Visible = false;
            // 
            // picPCBFail
            // 
            this.picPCBFail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picPCBFail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picPCBFail.Image = global::PicoLiteQCTester.Properties.Resources.Green_Checkmark1_500x420;
            this.picPCBFail.Location = new System.Drawing.Point(258, 85);
            this.picPCBFail.Name = "picPCBFail";
            this.picPCBFail.Size = new System.Drawing.Size(65, 28);
            this.picPCBFail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPCBFail.TabIndex = 8;
            this.picPCBFail.TabStop = false;
            this.picPCBFail.Visible = false;
            // 
            // picPCBPass
            // 
            this.picPCBPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picPCBPass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picPCBPass.Image = global::PicoLiteQCTester.Properties.Resources.Green_Checkmark1_500x420;
            this.picPCBPass.Location = new System.Drawing.Point(165, 85);
            this.picPCBPass.Name = "picPCBPass";
            this.picPCBPass.Size = new System.Drawing.Size(65, 28);
            this.picPCBPass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPCBPass.TabIndex = 7;
            this.picPCBPass.TabStop = false;
            this.picPCBPass.Visible = false;
            // 
            // picFWFail
            // 
            this.picFWFail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picFWFail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picFWFail.Image = global::PicoLiteQCTester.Properties.Resources.Green_Checkmark1_500x420;
            this.picFWFail.Location = new System.Drawing.Point(258, 42);
            this.picFWFail.Name = "picFWFail";
            this.picFWFail.Size = new System.Drawing.Size(65, 28);
            this.picFWFail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picFWFail.TabIndex = 6;
            this.picFWFail.TabStop = false;
            this.picFWFail.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(16, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Check List";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(167, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pass";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(268, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fail";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(9, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Firmware Version";
            // 
            // picFWPass
            // 
            this.picFWPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picFWPass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picFWPass.Image = global::PicoLiteQCTester.Properties.Resources.Green_Checkmark1_500x420;
            this.picFWPass.Location = new System.Drawing.Point(165, 42);
            this.picFWPass.Name = "picFWPass";
            this.picFWPass.Size = new System.Drawing.Size(65, 28);
            this.picFWPass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picFWPass.TabIndex = 5;
            this.picFWPass.TabStop = false;
            this.picFWPass.Visible = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(26, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Battery Level";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.Location = new System.Drawing.Point(25, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "PCB Version";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.Location = new System.Drawing.Point(11, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 29);
            this.label7.TabIndex = 1;
            this.label7.Text = "error label";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucTestPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucTestPlan";
            this.Size = new System.Drawing.Size(500, 303);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBatteryFail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBatteryPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPCBFail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPCBPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFWFail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFWPass)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picFWPass;
        private System.Windows.Forms.PictureBox picPCBFail;
        private System.Windows.Forms.PictureBox picPCBPass;
        private System.Windows.Forms.PictureBox picFWFail;
        private System.Windows.Forms.PictureBox picBatteryFail;
        private System.Windows.Forms.PictureBox picBatteryPass;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;

    }
}
