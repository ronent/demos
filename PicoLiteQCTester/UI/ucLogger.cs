﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester.UI
{
    public partial class ucLogger : UserControl
    {
        #region Delegate Events
        public event Action OnValidationPass;
        public event Action OnValidationFail;
        #endregion
        #region Constructor
        public ucLogger()
        {
            InitializeComponent();


        }
        #endregion
        #region Events
        private void txtSN_TextChanged(object sender, EventArgs e)
        {
            int num;
            int length = txtSN.Text.Length;

            if (length <= 8 && length >= 6 && int.TryParse(txtSN.Text, out num))
            {
                txtSN.BackColor = Color.White;
                if (OnValidationPass != null)
                    OnValidationPass();
            }

            else
            {
                txtSN.BackColor = Color.Red;
                if (OnValidationFail != null)
                    OnValidationFail();
            }
        }

        #endregion
        #region Methods
        internal void Reset()
        {
            SetSerialNumber(string.Empty);
            SetSerialNumberEnable(true);
            SerialNumberFocus();
        }

        public void SetFwVersion()
        {
            if (this.lblFWVersion.InvokeRequired)
                this.lblFWVersion.Invoke(new Action(() => SetFwVersion()));
            else
                this.lblFWVersion.Text = Utilities.Instance.PicoLiteFirmwareVersion.ToString();
        }

        public void SetPcb()
        {
            if (this.labelPCB.InvokeRequired)
                this.labelPCB.Invoke(new Action(() => SetPcb()));
            else
                this.labelPCB.Text = Utilities.Instance.PCB_Version.ToString();
        }

        public void SetBattery()
        {
            if (this.lblBattery.InvokeRequired)
                this.lblBattery.Invoke(new Action(() => SetBattery()));
            else
                this.lblBattery.Text = Utilities.Instance.Battery.ToString();
        }

        public void SetAccurecy()
        {
            if (this.lblAccuracy.InvokeRequired)
                this.lblAccuracy.Invoke(new Action(() => SetAccurecy()));
            else
                this.lblAccuracy.Text = string.Format("{0}-{1}", Utilities.Instance.Low, Utilities.Instance.High);
        }

        public void SerialNumberFocus()
        {
            if (this.txtSN.InvokeRequired)
                this.txtSN.Invoke(new Action(() => SerialNumberFocus()));
            else
            {
                this.txtSN.Focus();
                this.txtSN.BackColor = Color.Red;
            }
        }

        public string GetSerialNumber()
        {
            if (this.txtSN.InvokeRequired)
                return (string)this.txtSN.Invoke(new Func<string>(() => GetSerialNumber()));
            else
                return this.txtSN.Text;
        }

        public void SetSerialNumber(string txt)
        {
            if (this.txtSN.InvokeRequired)
                this.txtSN.Invoke(new MethodInvoker(delegate { SetSerialNumber(txt); }));
            else
                txtSN.Text = txt;
        }

        public void SetSerialNumberEnable(bool enabled)
        {
            if (this.txtSN.InvokeRequired)
                this.txtSN.Invoke(new MethodInvoker(delegate { SetSerialNumberEnable(enabled); }));
            else
                txtSN.Enabled = enabled;
        }

        #endregion
        #region functions
        private void LoggerBatteryEnabled(bool bEnable)
        {
            if (this.lblBattery.InvokeRequired)
                this.lblBattery.Invoke(new Action<bool>(LoggerBatteryEnabled), bEnable);
            else
                this.lblBattery.Enabled = bEnable;
        }

        private void LoggerAccuracyEnabled(bool bEnable)
        {
            if (this.lblAccuracy.InvokeRequired)
                this.lblAccuracy.Invoke(new Action<bool>(LoggerAccuracyEnabled), bEnable);
            else
                this.lblAccuracy.Enabled = bEnable;
        }
        #endregion
    }
}