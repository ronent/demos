﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester.UI
{
    public partial class ucTestPlan : UserControl
    {
        private bool isBatteryPass = false;
        private bool isFWPass = false;
        private bool isPCBPass = false;
        #region Constructor
        public ucTestPlan()
        {
            InitializeComponent();
            ShowMessage(false);
        }
        #endregion
        #region Methods
        internal void Reset()
        {
            isBatteryPass = false;
            isFWPass = false;
            isPCBPass = false;
            setAllUnvisible();
            ShowMessage(false);
        }

        public bool Check(string serialNumber, byte batteryLevel, Version firmwareVersion, byte pcbVersion)
        {
            Log4Tech.Log.Instance.Write("Check #{0} with parameters: battery - {1} , FW - {2}, PCB - {3}", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber, batteryLevel, firmwareVersion, pcbVersion);

            if (batteryLevel >= Utilities.Instance.Battery)
                isBatteryPass = true;
            if (Utilities.Instance.PicoLiteFirmwareVersion == firmwareVersion)
                isFWPass = true;
            if (Utilities.Instance.PCB_Version == pcbVersion)
                isPCBPass = true;

            setPassFailVisibility();

            return isBatteryPass && isFWPass && isPCBPass;
        }

        public bool IsBatteryPass()
        {
            return this.isBatteryPass;
        }

        public bool IsFWPass()
        {
            return this.isFWPass;
        }

        public bool IsPCBPass()
        {
            return this.isPCBPass;
        }

        public void ShowMessage(bool show, string message = null)
        {
            if (label7.InvokeRequired)
            {
                label7.Invoke(new MethodInvoker(delegate { ShowMessage(show, message); }));
            }
            else
            {
                label7.Visible = show;
                if(message != null)
                    label7.Text = message;
            }
        }
        #endregion
        #region private
        private void setPassFailVisibility()
        {
            setVisble(picBatteryFail, !isBatteryPass);
            setVisble(picBatteryPass, isBatteryPass);
            setVisble(picFWFail, !isFWPass);
            setVisble(picFWPass, isFWPass);
            setVisble(picPCBFail, !isPCBPass);
            setVisble(picPCBPass, isPCBPass);
        }

        private void setAllUnvisible()
        {
            setVisble(picBatteryFail, false);
            setVisble(picBatteryPass, false);
            setVisble(picFWFail, false);
            setVisble(picFWPass, false);
            setVisble(picPCBFail, false);
            setVisble(picPCBPass, false);
        }

        private void setVisble(PictureBox pic, bool visible)
        {
            if (pic.InvokeRequired)
            {
                pic.Invoke(new MethodInvoker(delegate() { setVisble(pic, visible); }));
            }
            else
            {
                pic.Visible = visible;
            }
        }

        #endregion
    }
}
