﻿namespace PicoLiteQCTester.UI
{
    partial class ucLoggerImage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picLogger = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picLogger)).BeginInit();
            this.SuspendLayout();
            // 
            // picLogger
            // 
            this.picLogger.Image = global::PicoLiteQCTester.Properties.Resources.PicoLite_V2;
            this.picLogger.Location = new System.Drawing.Point(3, 3);
            this.picLogger.Name = "picLogger";
            this.picLogger.Size = new System.Drawing.Size(250, 125);
            this.picLogger.TabIndex = 0;
            this.picLogger.TabStop = false;
            // 
            // ucLoggerImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picLogger);
            this.Name = "ucLoggerImage";
            this.Size = new System.Drawing.Size(258, 133);
            ((System.ComponentModel.ISupportInitialize)(this.picLogger)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picLogger;
    }
}
