﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester.UI
{
    public partial class ucActionsPassFail : UserControl
    {
        public ucActionsPassFail()
        {
            InitializeComponent();
        }

        #region Methods
        public void UpdateMessage(string msg)
        {
            if (this.lblMessages.InvokeRequired)
                this.lblMessages.Invoke(new Action<string>(UpdateMessage), msg);
            else
                this.lblMessages.Text = msg;
        }

        
        #endregion
        
    }
}
