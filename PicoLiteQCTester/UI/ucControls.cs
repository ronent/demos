﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester.UI
{
    public partial class ucControls : UserControl
    {
        public enum WizradSteps
        {
            Start=1,
            Test,
            Disconnect,
            Push2Run,
            ReConnect,
            Finished,
            Fail,
        }

        public WizradSteps Steps { get; set; }
        #region Events
        public event Action OnButtonStart;
        public event Action OnButtonNext;
        public event Action OnPassLEDRed;
        public event Action OnFailLEDRed;
        public event Action OnPassPush2Run;
        public event Action OnFailPush2Run;
        #endregion
        #region Constructor
        public ucControls()
        {
            InitializeComponent();
        }

        #endregion
        #region private methods
        private void failDisplay(bool bShow)
        {
            if (this.btnFail.InvokeRequired)
                this.btnFail.Invoke(new Action<bool>(failDisplay), bShow);
            else
                this.btnFail.Visible = bShow;
        }

        #endregion
        #region Methods
        internal void Reset()
        {
            ButtonDisplay(true);
            StartButtonEnable(false);
            PassFailDisplay(false);
            StartButtonText("Start");
        }

        public void StartButtonText(string text)
        {
            if (this.btnStart.InvokeRequired)
                this.btnStart.Invoke(new Action<string>(StartButtonText), text);
            else
                this.btnStart.Text = text;
        }

        public void StartButtonEnable(bool bEnable)
        {
            if (this.btnStart.InvokeRequired)
                this.btnStart.Invoke(new Action<bool>(StartButtonEnable), bEnable);
            else
                this.btnStart.Enabled = bEnable;
        }

        public void PassFailDisplay(bool bShow)
        {
            if (this.btnPass.InvokeRequired)
                this.btnPass.Invoke(new Action<bool>(PassFailDisplay), bShow);
            else
            {
                this.btnPass.Visible = bShow;
                failDisplay(bShow);
            }
        }

        public void ButtonDisplay(bool bShow)
        {
            if (this.btnStart.InvokeRequired)
                this.btnStart.Invoke(new Action<bool>(ButtonDisplay), bShow);
            else
            {
                this.btnStart.Visible = bShow;
                this.btnStart.Enabled = false;
            }
        }
        
        public void Start()
        {
            ButtonDisplay(true);
        }

        #endregion
        #region control events
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (Steps== WizradSteps.Start)
            {
                if (OnButtonStart != null)
                    OnButtonStart();
            }
            else if (Steps == WizradSteps.Test)
            {
                if (OnButtonNext != null)
                    OnButtonNext();
            }
        }

        private void btnPass_Click_1(object sender, EventArgs e)
        {
            if (Steps == WizradSteps.Disconnect)
            {
                if (OnPassLEDRed != null)
                    OnPassLEDRed();
            }
            else if(Steps == WizradSteps.Push2Run)
            {
                if (OnPassPush2Run != null)
                    OnPassPush2Run();
            }
               
        }

        private void btnFail_Click_1(object sender, EventArgs e)
        {
            if (Steps == WizradSteps.Disconnect)
            {
                if (OnFailLEDRed != null)
                    OnFailLEDRed();
            }
            else if (Steps == WizradSteps.Push2Run)
            {
                if (OnFailPush2Run != null)
                    OnFailPush2Run();
            }
        }
        #endregion
    }
}
