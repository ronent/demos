﻿namespace PicoLiteQCTester.Forms
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkList = new System.Windows.Forms.GroupBox();
            this.ucControls = new PicoLiteQCTester.UI.ucControls();
            this.ucTestPlan = new PicoLiteQCTester.UI.ucTestPlan();
            this.ucActionsPassFail1 = new PicoLiteQCTester.UI.ucActionsPassFail();
            this.ucQCStart = new PicoLiteQCTester.UI.ucQCHeader();
            this.ucLogger1 = new PicoLiteQCTester.UI.ucLogger();
            this.ucLoggerImage1 = new PicoLiteQCTester.UI.ucLoggerImage();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.checkList.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ucLogger1);
            this.groupBox1.Controls.Add(this.ucLoggerImage1);
            this.groupBox1.Location = new System.Drawing.Point(506, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 355);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Logger Data";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(881, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // checkList
            // 
            this.checkList.Controls.Add(this.ucQCStart);
            this.checkList.Controls.Add(this.ucTestPlan);
            this.checkList.Controls.Add(this.ucActionsPassFail1);
            this.checkList.Location = new System.Drawing.Point(4, 38);
            this.checkList.Name = "checkList";
            this.checkList.Size = new System.Drawing.Size(483, 355);
            this.checkList.TabIndex = 7;
            this.checkList.TabStop = false;
            this.checkList.Text = "Check List";
            // 
            // ucControls
            // 
            this.ucControls.Location = new System.Drawing.Point(305, 413);
            this.ucControls.Name = "ucControls";
            this.ucControls.Size = new System.Drawing.Size(351, 60);
            this.ucControls.TabIndex = 8;
            // 
            // ucTestPlan
            // 
            this.ucTestPlan.Location = new System.Drawing.Point(8, 26);
            this.ucTestPlan.Name = "ucTestPlan";
            this.ucTestPlan.Size = new System.Drawing.Size(469, 323);
            this.ucTestPlan.TabIndex = 3;
            this.ucTestPlan.Visible = false;
            // 
            // ucActionsPassFail1
            // 
            this.ucActionsPassFail1.Location = new System.Drawing.Point(6, 13);
            this.ucActionsPassFail1.Name = "ucActionsPassFail1";
            this.ucActionsPassFail1.Size = new System.Drawing.Size(471, 320);
            this.ucActionsPassFail1.TabIndex = 4;
            // 
            // ucQCStart
            // 
            this.ucQCStart.Location = new System.Drawing.Point(8, 13);
            this.ucQCStart.Name = "ucQCStart";
            this.ucQCStart.Size = new System.Drawing.Size(469, 336);
            this.ucQCStart.TabIndex = 1;
            this.ucQCStart.Visible = false;
            // 
            // ucLogger1
            // 
            this.ucLogger1.Location = new System.Drawing.Point(13, 177);
            this.ucLogger1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ucLogger1.Name = "ucLogger1";
            this.ucLogger1.Size = new System.Drawing.Size(307, 172);
            this.ucLogger1.TabIndex = 4;
            this.ucLogger1.Visible = false;
            // 
            // ucLoggerImage1
            // 
            this.ucLoggerImage1.Location = new System.Drawing.Point(8, 20);
            this.ucLoggerImage1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ucLoggerImage1.Name = "ucLoggerImage1";
            this.ucLoggerImage1.Size = new System.Drawing.Size(344, 120);
            this.ucLoggerImage1.TabIndex = 3;
            this.ucLoggerImage1.Visible = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 485);
            this.ControlBox = false;
            this.Controls.Add(this.ucControls);
            this.Controls.Add(this.checkList);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PicoLite QC Tester";
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.checkList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private UI.ucLogger ucLogger1;
        private UI.ucLoggerImage ucLoggerImage1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox checkList;
        private UI.ucTestPlan ucTestPlan;
        private UI.ucQCHeader ucQCStart;
        private UI.ucControls ucControls;
        private UI.ucActionsPassFail ucActionsPassFail1;
    }
}