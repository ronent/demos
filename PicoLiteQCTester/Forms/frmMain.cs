﻿using PicoLite.Devices.V2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester.Forms
{
    public partial class frmMain : Form
    {
        #region private properties
        string lastSN = string.Empty;
        Manager manager = new Manager();
        List<ReportProfile> Reports = new List<ReportProfile>();
        ReportProfile report;

        #endregion
        #region Constructor
        public frmMain()
        {
            InitializeComponent();
            initialize();
        }

        #endregion
        #region Form Events
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (Reports.Count > 0)
                if (Utilities.Instance.SaveReports(Reports))
                    MessageBox.Show(Utilities.Instance.REPORT_SAVED_MSG, "Report Generation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show(Utilities.Instance.REPORT_FAIL_SAVED_MSG, "Report Generation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            Application.Exit();
        }

        #endregion
        #region Logger Events
        void manager_OnDeviceDisconnected(PicoLiteLogger picoLite)
        {
            LoggerDisplay(false);
            LoggerImageDisplay(false);
            //if finished the Test Plan then show instructions of what to do with the logger
            if (ucControls.Steps == UI.ucControls.WizradSteps.Disconnect)
            {
                ucControls.ButtonDisplay(false);
                ucControls.PassFailDisplay(true);
                lastSN = picoLite.Status.SerialNumber;
            }
            else 
            {
                //otherwise ask for connect it again
                ucControls.Steps = UI.ucControls.WizradSteps.Start;
                ucActionsPassFail1.UpdateMessage(Utilities.Instance.CONNECT_PICOLITE_MSG);
                ActionsPassFailDisplay(true);
                LoggerDisplay(false);
                testPlanDisplay(false);
                LoggerImageDisplay(false);
                lastSN = string.Empty;
                ucControls.StartButtonEnable(false);
                ucControls.ButtonDisplay(false);
                report = null;
            }
        }

        void manager_OnDeviceConnected(PicoLiteLogger picoLite)
        {
            try
            {
                LoggerDisplay(true);
                LoggerImageDisplay(true);
                ActionsPassFailDisplay(false);
                ucControls.ButtonDisplay(true);
                ucControls.StartButtonEnable(true);

                if (ucControls.Steps == UI.ucControls.WizradSteps.Start)
                {
                    reset();

                    QCStartDisplay(true);
                    ucControls.Start();
                    ActionsPassFailDisplay(false);
                }
                else if(ucControls.Steps == UI.ucControls.WizradSteps.ReConnect)
                {
                    if (picoLite.Status.SerialNumber == lastSN)
                    {
                        ucControls.ButtonDisplay(false);
                        //download samples
                        manager.Download(picoLite.Status.SerialNumber);
                    }
                    else
                    {
                        MessageBox.Show("Error: New device was connected");

                        ucControls.Steps = UI.ucControls.WizradSteps.Start;
                        ActionsPassFailDisplay(false);
                        QCStartDisplay(true);
                        reset();

                        report = null;
                    }
                }
                else
                {
                    ucControls.Steps = UI.ucControls.WizradSteps.Start;
                    QCStartDisplay(true);
                    ActionsPassFailDisplay(false);
                    reset();

                    report = null;
                }
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in manager_OnDeviceConnected", this, ex);
            }
        }

        void manager_OnLastSample(Base.Sensors.Samples.Sample sample, string serialNumber)
        {
            try
            {
                report.TestEnded = DateTime.Now;

                if (sample != null)
                {
                    report.LoggerSampleValue = sample.Value;

                    if (sample.Value >= Utilities.Instance.Low && sample.Value <= Utilities.Instance.High)
                    {
                        //send setup to the logger
                        manager.SendSetupAndDeepSleep(serialNumber);

                        //show the message
                        ucActionsPassFail1.UpdateMessage(Utilities.Instance.QCFINISHED_MSG);
                        ActionsPassFailDisplay(true);
                        ucControls.StartButtonEnable(false);
                        ucControls.PassFailDisplay(false);

                        ucControls.Steps = UI.ucControls.WizradSteps.Start;

                    }
                    else
                    {
                        //show message of failing on sensor
                        ucActionsPassFail1.UpdateMessage(string.Format("{0}{1}{1}{2}", Utilities.Instance.TEST_FAILED_SENSOR, Environment.NewLine, Utilities.Instance.CONNECT_PICOLITE_MSG));
                        ActionsPassFailDisplay(true);

                        //fail the test
                        report.IsSensorPass = false;
                        ucControls.Steps = UI.ucControls.WizradSteps.Start;
                    }
                }

                else
                {
                    //show message of failing on sensor
                    ucActionsPassFail1.UpdateMessage(string.Format("{0}{1}{1}{2}", Utilities.Instance.TEST_FAILED_SENSOR, Environment.NewLine, Utilities.Instance.CONNECT_PICOLITE_MSG));
                    ActionsPassFailDisplay(true);

                    //fail the test
                    report.IsSensorPass = false;
                    ucControls.Steps = UI.ucControls.WizradSteps.Start;
                }

                addReport(report);

            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in manager_OnLastSample", this, ex);
            }
        }
        #endregion
        #region private methods
        private void initialize()
        {
            manager.OnDeviceConnected += manager_OnDeviceConnected;
            manager.OnDeviceDisconnected += manager_OnDeviceDisconnected;
            manager.OnLastSample += manager_OnLastSample;
            ucActionsPassFail1.UpdateMessage(Utilities.Instance.CONNECT_PICOLITE_MSG);

            ucControls.OnButtonStart += ucControls_OnStartTest;
            ucControls.OnFailLEDRed += ucControls_OnFailLEDRed;
            ucControls.OnPassLEDRed += ucControls_OnPassLEDRed;
            ucControls.OnFailPush2Run += ucControls_OnFailPush2Run;
            ucControls.OnPassPush2Run += ucControls_OnPassPush2Run;
            ucControls.OnButtonNext += ucControls_OnButtonNext;

            ucLogger1.OnValidationFail += ucLogger1_OnValidationFail;
            ucLogger1.OnValidationPass += ucLogger1_OnValidationPass;

            ucControls.Steps = UI.ucControls.WizradSteps.Start;
            ucLogger1.SetFwVersion();
            ucLogger1.SetPcb();
            ucLogger1.SetAccurecy();
            ucLogger1.SetBattery();
        }
        
        private void LoggerDisplay(bool bShow)
        {
            if (this.ucLogger1.InvokeRequired)
                this.ucLogger1.Invoke(new Action<bool>(LoggerDisplay), bShow);
            else
                this.ucLogger1.Visible = bShow;
        }

        private void LoggerImageDisplay(bool bShow)
        {
            if (this.ucLoggerImage1.InvokeRequired)
                this.ucLoggerImage1.Invoke(new Action<bool>(LoggerImageDisplay),bShow);
            else
                this.ucLoggerImage1.Visible = bShow;
        }

        private void addReport(ReportProfile report)
        {
            Reports.Add(report);
            Utilities.Instance.SaveReports(Reports);
        }
        #endregion
        #region Controls Events
        void ucControls_OnPassPush2Run()
        {
            ucActionsPassFail1.UpdateMessage(Utilities.Instance.RECONNECT_MSG);
            ucControls.Steps = UI.ucControls.WizradSteps.ReConnect;
            ucControls.PassFailDisplay(false);
            report.IsPushToRunPass = true;
        }

        void ucControls_OnFailPush2Run()
        {
            ucActionsPassFail1.UpdateMessage(string.Format("{0}{1}{1}{2}", Utilities.Instance.TEST_FAILED, Environment.NewLine, Utilities.Instance.CONNECT_PICOLITE_MSG));

            ActionsPassFailDisplay(true);
            ucControls.Steps = UI.ucControls.WizradSteps.Start;
            ucControls.StartButtonEnable(true);
            ucControls.PassFailDisplay(false);
            report.IsPushToRunPass = false;

            report.TestEnded = DateTime.Now;
            addReport(report);
        }

        void ucControls_OnPassLEDRed()
        {
            ucActionsPassFail1.UpdateMessage(Utilities.Instance.PUSH2RUN_MSG);
            ucControls.Steps = UI.ucControls.WizradSteps.Push2Run;
            report.IsDisconnectLEDRedPass = true;
        }

        void ucControls_OnFailLEDRed()
        {
            ucActionsPassFail1.UpdateMessage(string.Format("{0}{1}{1}{2}", Utilities.Instance.TEST_FAILED, Environment.NewLine, Utilities.Instance.CONNECT_PICOLITE_MSG));

            ActionsPassFailDisplay(true);
            ucControls.Steps = UI.ucControls.WizradSteps.Start;
            ucControls.StartButtonEnable(true);
            ucControls.PassFailDisplay(false);
            report.IsDisconnectLEDRedPass = false;

            report.TestEnded = DateTime.Now;
            addReport(report);
        }

        void ucControls_OnButtonNext()
        {
            testPlanDisplay(false);
            if (ucControls.Steps == UI.ucControls.WizradSteps.Test)
            {
                ucActionsPassFail1.UpdateMessage(Utilities.Instance.DISCONNECTION_RED_FLASH_MSG);
                ActionsPassFailDisplay(true);
                ucControls.ButtonDisplay(false);
                ucControls.Steps = UI.ucControls.WizradSteps.Disconnect;
            }
            else if(ucControls.Steps== UI.ucControls.WizradSteps.Fail)
            {
                ucActionsPassFail1.UpdateMessage(Utilities.Instance.DISCONNECT_PICOLITE_MSG);
                ActionsPassFailDisplay(false);
                ucControls.ButtonDisplay(false);
                ucControls.Steps = UI.ucControls.WizradSteps.Start;
            }
        }

        void ucLogger1_OnValidationPass()
        {
            ucControls.StartButtonEnable(true);
        }

        void ucLogger1_OnValidationFail()
        {
            ucControls.StartButtonEnable(false);
        }

        private void ucControls_OnStartTest()
        {
            string serialNumber = ucLogger1.GetSerialNumber();
            //initialize the report
            report = new ReportProfile();
            report.SerialNumber = serialNumber;

            if (serialNumber.Length > 0)
            {
                Log4Tech.Log.Instance.Write("PicoLite Serial Number is {0}", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber);

                var batteryLevel = Utilities.Instance.Battery;
                //try to send setup to the logger
                if (manager.SendSetupAndDeepSleepForTest(serialNumber))
                {
                    Log4Tech.Log.Instance.Write("Setup for #{0} was OK, try now to get the status from the picolite", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber);
                    //if success then get the status of the logger
                    var picoLite = manager.GetStatus(serialNumber);

                    Log4Tech.Log.Instance.Write("Sends #{0} to Test Plan for checking if pass the test", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber);
                    //check if pass the test of battery and firmware
                    if (ucTestPlan.Check(serialNumber, picoLite.Battery.BatteryLevel, picoLite.Status.FirmwareVersion, picoLite.Status.PCBAssembly))
                    {
                        //show the test plan with the results
                        testPlanDisplay(true);
                        ucLogger1.SetSerialNumberEnable(false);
                        ucControls.StartButtonText("Next");
                        ucControls.Steps = UI.ucControls.WizradSteps.Test;
                        report.FirmwareVersion = picoLite.Status.FirmwareVersion;
                        report.IsFirmwarePass = true;
                        report.IsPCBPass = true;
                        report.PCBAssembly = picoLite.Status.PCBAssembly;
                        report.IsBatteryPass = true;
                        report.BatteryValue = picoLite.Status.BatteryLevel;
                    }
                    else
                    {
                        //show the test plan with the results
                        ucTestPlan.ShowMessage(true, string.Format("{0}{1}{1}{2}", Utilities.Instance.TEST_FAILED, Environment.NewLine, Utilities.Instance.CONNECT_PICOLITE_MSG));
                        testPlanDisplay(true);
                        ucControls.ButtonDisplay(false);                   
                        
                        // update the report
                        report.IsBatteryPass = ucTestPlan.IsBatteryPass();
                        report.IsPCBPass = ucTestPlan.IsPCBPass();
                        report.IsFirmwarePass = ucTestPlan.IsFWPass();

                        report.BatteryValue = picoLite.Battery.BatteryLevel;
                        report.FirmwareVersion = picoLite.Status.FirmwareVersion;
                        report.PCBAssembly = picoLite.Status.PCBAssembly;

                        report.TestEnded = DateTime.Now;
                        addReport(report);

                        ucControls.Steps = UI.ucControls.WizradSteps.Start;
                    }
                }
            }
            else
            {
                ucLogger1.SerialNumberFocus();
                MessageBox.Show("Serial Number is empty! \r\n Please fill in Serial Number.", "PicoLiteQCTester Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void testPlanDisplay(bool bShow)
        {
            if (this.ucTestPlan.InvokeRequired)
                this.ucTestPlan.Invoke(new Action<bool>(testPlanDisplay), bShow);
            else
            {
                this.ucTestPlan.Visible = bShow;
                QCStartDisplay(false);
            }
            
        }

        private void ActionsPassFailDisplay(bool bShow)
        {
            if (this.ucActionsPassFail1.InvokeRequired)
                this.ucActionsPassFail1.Invoke(new Action<bool>(ActionsPassFailDisplay), bShow);
            else
                this.ucActionsPassFail1.Visible = bShow;
               
        }

        private void QCStartDisplay(bool bShow)
        {
            if (this.ucQCStart.InvokeRequired)
                this.ucQCStart.Invoke(new Action<bool>(QCStartDisplay), bShow);
            else
                this.ucQCStart.Visible = bShow;
        }
        #endregion

        private void reset()
        {
            ucControls.Reset();
            ucLogger1.Reset();
            ucTestPlan.Reset();
        }
    }
}
