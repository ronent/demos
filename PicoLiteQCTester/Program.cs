﻿using PicoLiteQCTester.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Log4Tech.Log.Instance.IsWrite2Cloud = false;
            Log4Tech.Log.Instance.IsWrite2Console = true;
            Log4Tech.Log.Instance.IsWrite2File = true;

            Application.Run(new frmMain());
        }
    }
}
