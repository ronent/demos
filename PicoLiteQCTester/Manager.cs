﻿using Base.Devices.Management;
using Base.Firmware;
using Base.OpCodes;
using Base.Sensors.Samples;
using PicoLite.Devices.V2;
using PicoLiteQCTester.Forms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoLiteQCTester
{
    class Manager
    {
        #region delegation
        public delegate void DeviceConnectionDelegate(PicoLiteLogger picoLite);
        public delegate void LastSampleDelegate(Sample sample,string serialNumber);
        #endregion
        #region Public Events
        public event DeviceConnectionDelegate OnDeviceConnected;
        public event DeviceConnectionDelegate OnDeviceDisconnected;
        public event LastSampleDelegate OnLastSample;
        #endregion
        #region Constructor
        public Manager()
        {
            setFirmwareVersions();
            start();
        }
        #endregion
        #region private
        private void setFirmwareVersions()
        {
            var firmware = FirmwareFactory.GetInstance().Get(typeof(PicoLiteLogger));
            Utilities.Instance.PCB_Version = firmware.PCBVersion;
            Utilities.Instance.PicoLiteFirmwareVersion = firmware.Version;
        }

        private void start()
        {
            try
            {
                SuperDeviceManager.OnDeviceConnected += SuperDeviceManager_OnDeviceConnected;
                SuperDeviceManager.OnDeviceRemoved += SuperDeviceManager_OnDeviceRemoved;
                SuperDeviceManager.Start();
            }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in init DeviceAPI", this, ex);
            }
        }
        #endregion
        #region Events
        void SuperDeviceManager_OnDeviceRemoved(object sender, Base.Devices.Management.EventsAndExceptions.ConnectionEventArgs<Base.Devices.GenericDevice> e)
        {
            if (e.Device is PicoLiteLogger)
                if (OnDeviceDisconnected != null)
                    OnDeviceDisconnected(e.Device as PicoLiteLogger);
        }

        void SuperDeviceManager_OnDeviceConnected(object sender, Base.Devices.Management.EventsAndExceptions.ConnectionEventArgs<Base.Devices.GenericDevice> e)
        {
            if (e.Device is PicoLiteLogger)
                if (OnDeviceConnected != null)
                    OnDeviceConnected(e.Device as PicoLiteLogger);
        }
        #endregion
        #region Methods
        public PicoLiteLogger GetStatus(string serialNumber)
        {
            //try to locate the device and get his status
            var genericDevice = SuperDeviceManager.GetDevice(serialNumber);
            //if found and it is picolite logger then return it
            if (genericDevice != null)
                if (genericDevice is PicoLiteLogger)
                    return genericDevice as PicoLiteLogger;

            return null;
        }

        public void Download(string serialNumber)
        {
            try
            {
                var picolite = (SuperDeviceManager.GetDevice(serialNumber) as PicoLite.Devices.V2.PicoLiteLogger);
                if (picolite.Functions.IsRunning)
                    picolite.Functions.Stop().Wait();

                picolite.Functions.OnDownloadCompleted += Functions_OnDownloadCompleted;
                picolite.Functions.Download();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in Download", this, ex);
            }
        }

        void Functions_OnDownloadCompleted(object sender, Base.OpCodes.Helpers.DownloadCompletedArgs args)
        {
            //get the last sample that is not time stamp
            if (args.EnabledSensors.Count() == 1)
            {
                var lastSample = (from i in args.EnabledSensors.ToArray()[0].Samples
                                  where i.IsTimeStamp == false
                                  orderby i.Date descending
                                  select i).FirstOrDefault();

                if (OnLastSample != null)
                    OnLastSample(lastSample, args.SerialNumber);
            }
        }

        public bool SendSetupAndDeepSleep(string serialNumber)
        {
            Log4Tech.Log.Instance.Write("Sending default setup to picolite #{0}", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber);
            //checks first if the logger is running
            try
            {
                var picolite = (SuperDeviceManager.GetDevice(serialNumber) as PicoLite.Devices.V2.PicoLiteLogger);
                if (picolite.Functions.IsRunning)
                    picolite.Functions.Stop().Wait();

                var picoliteSetup = new PicoLite.DataStructures.V2.PicoLiteV2SetupConfiguration();

                picoliteSetup.SerialNumber = serialNumber;
                picoliteSetup.BoomerangEnabled = false;
                picoliteSetup.Comment = ConfigurationManager.AppSettings["comment"];
                picoliteSetup.CyclicMode = Convert.ToBoolean(ConfigurationManager.AppSettings["cyclicMode"]);
                picoliteSetup.FahrenheitMode = Convert.ToBoolean(ConfigurationManager.AppSettings["fahrenheitMode"]);
                picoliteSetup.Interval = new TimeSpan(0, Convert.ToInt32(ConfigurationManager.AppSettings["interval"]), 0);
                picoliteSetup.TimerRunEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["timerRunEnabled"]);
                picoliteSetup.StopOnKeyPress = Convert.ToBoolean(ConfigurationManager.AppSettings["stopOnKeyPress"]);
                picoliteSetup.PushToRunMode = Convert.ToBoolean(ConfigurationManager.AppSettings["pushToRunMode"]);
                picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = Convert.ToBoolean(ConfigurationManager.AppSettings["alarmEnabled"]);
                picoliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(ConfigurationManager.AppSettings["alarmLow"]);
                picoliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(ConfigurationManager.AppSettings["alarmHigh"]);
                picoliteSetup.RunDelay = Convert.ToUInt16(ConfigurationManager.AppSettings["runDelay"]);

                Result result = Result.ERROR;
                if (picolite != null)
                {
                    result = picolite.Functions.SendSetup(picoliteSetup).Result;

                    if (result.IsOK)
                        result = picolite.Functions.DeepSleep().Result;

                }

                if (!result.IsOK)
                    MessageBox.Show(result.ToString());

                return result.IsOK;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendSetup", this, ex);
            }
            return false;
        }

        public bool SendSetupAndDeepSleepForTest(string serialNumber)
        {
            Log4Tech.Log.Instance.Write("Sending default setup to picolite #{0}", this, Log4Tech.Log.LogSeverity.DEBUG, serialNumber);
            //checks first if the logger is running
            try
            {
                var picolite = (SuperDeviceManager.GetDevices().First() as PicoLite.Devices.V2.PicoLiteLogger);
                if (picolite.Functions.IsRunning)
                    picolite.Functions.Stop().Wait();

                var picoliteSetup = new PicoLite.DataStructures.V2.PicoLiteV2SetupConfiguration();

                picoliteSetup.SerialNumber = serialNumber;
                picoliteSetup.BoomerangEnabled = false;
                picoliteSetup.Comment = Utilities.Instance.PICOLITE_DEFAULT_COMMENT;
                picoliteSetup.CyclicMode = false;
                picoliteSetup.FahrenheitMode = false;
                picoliteSetup.Interval = new TimeSpan(0, 1, 0);//one minute sampling rate by default
                picoliteSetup.TimerRunEnabled = false;
                picoliteSetup.StopOnKeyPress = false;
                picoliteSetup.PushToRunMode = true;
                picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = false;
                picoliteSetup.RunDelay = 0;

                if (picolite != null && picolite.Functions.SendSetup(picoliteSetup).Result.IsOK)
                {
                    var result = picolite.Functions.DeepSleep().Result;
                    return result.IsOK;
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in SendSetup", this, ex);
            }
            return false;
        }
        #endregion
    }
}
