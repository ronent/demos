﻿using LicenseAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LicenseAPITester
{
    class Program
    {
        static Manager manager = new Manager();
        static List<Modules> modules = new List<Modules>();
        static string licensePath = string.Empty;
        static void Main(string[] args)
        {
            ConsoleKeyInfo cki;
            Console.TreatControlCAsInput = true;

            Console.WriteLine("Options:");
            Console.WriteLine("*********");
            Console.WriteLine("1. Create license");
            Console.WriteLine("2. Read license");
            Console.WriteLine("3. Check if license expired");
            Console.WriteLine("4. Extend license by 1 year");
            Console.WriteLine("5. Create Simple Expiry License");
            Console.WriteLine("********************************");
            Console.WriteLine("Press the Escape (Esc) key to quit: \n");

            do
            {
                cki = Console.ReadKey(true);
                //Console.Write(" --- You pressed ");
                if ((cki.Modifiers & ConsoleModifiers.Alt) != 0)
                    Console.Write("ALT+");
                if ((cki.Modifiers & ConsoleModifiers.Shift) != 0)
                    Console.Write("SHIFT+");
                if ((cki.Modifiers & ConsoleModifiers.Control) != 0)
                    Console.Write("CTL+");
                //Console.WriteLine(cki.Key.ToString());
                switch(cki.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        createLicense();
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        readLicense();
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        isLicenseExpired();
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        extendLicense();
                        break;
                    case ConsoleKey.D5:
                    case ConsoleKey.NumPad5:
                        createSimpleLicense();
                        break;
                }
            } while (cki.Key != ConsoleKey.Escape);

            
        }

        private static void extendLicense()
        {
            var extendDate = DateTime.Now.AddYears(1);

            Console.WriteLine("Try to extend the license with date {0}", extendDate);

            manager.ExtendLicense(licensePath, extendDate);

            Console.WriteLine("The new date of the license is {0}", manager.ReadLicense(licensePath).ExpiredDate);
        }

        private static void isLicenseExpired()
        {
            Console.WriteLine("Checks if license expired");

            Console.WriteLine("license is {0}", manager.IsLicenseExpired(licensePath) == false ? "valid" : "expired");
        }

        private static void readLicense()
        {
            Console.WriteLine("Now... try to decrypt it...");

            var licDef = manager.ReadLicense(licensePath);

            var enu = licDef.RegisteredModules.GetEnumerator();
            StringBuilder sb = new StringBuilder();
            while (enu.MoveNext())
            {
                sb.Append(string.Format("{0} ,", System.Enum.GetName(typeof(Modules), enu.Current)));
            }
            if (sb.Length > 0)
            {
                sb = sb.Remove(sb.Length - 1, 1);
            }

            Console.WriteLine("license after decrypt : {0}, {1}, {2}, {3}", licDef.ExpiredDate, licDef.MaxUsers, sb, licDef.UseCFR);
        }

        private static void createSimpleLicense()
        {
            Console.WriteLine("Please type expiry by days (press Enter when done): \r\n");

            ConsoleKeyInfo ck;
            var days = string.Empty;
            do
            {
                ck = Console.ReadKey();
                if (ck.Key != ConsoleKey.Enter)
                    days += ck.KeyChar;
            } while (ck.Key != ConsoleKey.Enter);

            var dt = DateTime.Now.AddDays(Convert.ToInt32(days));

            licensePath = manager.CreateLicense(new LicenseDefintion
            {
                ExpiredDate = dt
            });

            Console.WriteLine("License file was created at -> {0}", licensePath);
        }

        private static void createLicense()
        {
            Console.WriteLine("Please type in the customer IP (press Enter when done): \r\n");

            ConsoleKeyInfo ck;
            string ip = string.Empty;
            do
            {
                ck = Console.ReadKey();
                if (ck.Key!= ConsoleKey.Enter)
                    ip += ck.KeyChar;
            } while (ck.Key != ConsoleKey.Enter);
           
            IPAddress validIP;

            if (IPAddress.TryParse(ip, out validIP))
            {

                modules.Add(Modules.USBLoggers);
                modules.Add(Modules.DataNet);

                var enu = modules.GetEnumerator();
                StringBuilder sb = new StringBuilder();
                while (enu.MoveNext())
                {
                    sb.Append(string.Format("{0} ,", Enum.GetName(typeof(Modules), enu.Current)));
                }
                if (sb.Length > 0)
                {
                    sb = sb.Remove(sb.Length - 1, 1);
                }

                var dt = DateTime.Now.AddYears(1);

                Console.WriteLine("Create license file with parameters: ");
                Console.WriteLine("Expired at : {0}", dt);
                Console.WriteLine("Max users: {0}", 100);
                Console.WriteLine("Modules: {0}", sb);
                Console.WriteLine("Use CFR : {0}", true);
                Console.WriteLine("Client IP : {0}", ip);

                licensePath = manager.CreateLicense(new LicenseDefintion()
                {
                    ExpiredDate = dt,
                    MaxUsers = 100,
                    RegisteredModules = modules,
                    UseCFR = true,
                    ClientIP = ip
                });

                Console.WriteLine("License file was created at -> {0}", licensePath);
            }
            else
                Console.WriteLine("Not a valid IP !");
        }
    }
}
