﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFClient.Devices.Optimizations
{
    public class FinishedLoopThroughAsLongEventArgs : EventArgs
    {
        public List<byte[]> Packets { get; private set; }
        public Exception Exception { get; set; }
        public bool IsOK { get { return Exception == null; } }
        public bool isFinished { get; private set; }

        public FinishedLoopThroughAsLongEventArgs(List<byte[]> packets, bool isFinished = false)
        {
            this.Packets = packets;
            this.isFinished = isFinished;
        }
    }
}
