﻿using HidLibrary;
using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using WCFClient.Communications;
using WCFClient.Utilities;

namespace WCFClient
{
    public delegate void EnabledChangedDelegate(bool isEnable);

    public class CommunicationManager : IDisposable
    {
        #region Members
        public static TimeSpan DISCONNECTION_TIMER = new TimeSpan(0, 0, 10);

        protected internal DeviceManager deviceManager { get; set; }
        protected internal ClientAPI client { get; set; }
        protected internal ServerAPI server { get; set; }

        private CheckApplicationInstances dataSuiteInstanceChecker;
        private volatile bool isEnabled;
        private System.Timers.Timer timer;

        private int _UserID;

        #endregion
        #region Events
        public event EnabledChangedDelegate OnEnableChanged;

        public event DeviceConnectionChangedDelegate OnDeviceConnectionChanged
        {
            add { deviceManager.OnDeviceConnectionChanged += value; }
            remove { deviceManager.OnDeviceConnectionChanged -= value; }
        }

        public event InstanceChangedDelegate OnDataSuiteInstanceChanged
        {
            add { dataSuiteInstanceChecker.OnInstanceChanged += value; }
            remove { dataSuiteInstanceChecker.OnInstanceChanged -= value; }
        }

        public event ConnectionStateChangedDelegate OnConnectionStateChanged
        {
            add { server.OnConnectionStateChanged += value; }
            remove { server.OnConnectionStateChanged -= value; }
        }

        #endregion
        #region Constructor
        public CommunicationManager(string uri, int userId)
        {
            IsDisposed = false;
            ServerURI = uri;
            _UserID = userId;

            initializeDeviceManager();

            deviceManager.OnDeviceConnected += deviceManager_OnDeviceConnected;
            deviceManager.OnDeviceRemoved += deviceManager_OnDeviceRemoved;

            initializeClientAPI();
            initializeServerAPI();

            server.OnConnectionStateChanged += server_OnConnectionStateChanged;

            dataSuiteInstanceChecker = new CheckApplicationInstances("DataSuite"); //
            dataSuiteInstanceChecker.OnInstanceChanged += (isConnected) =>
            {
                dataSuiteInstanceChanged(isConnected);
            };

            dataSuiteInstanceChecker.Start();

            isEnabled = true;
        }

        protected virtual void initializeDeviceManager()
        {
            deviceManager = new DeviceManager(this);
        }

        protected virtual void initializeServerAPI()
        {
            server = new ServerAPI(this);
        }

        protected virtual void initializeClientAPI()
        {
            client = new ClientAPI(this);
        }

        #endregion
        #region Properties
        public string ServerURI { get; private set; }

        public bool IsDisposed { get; private set; }

        public CommunicationState State
        {
            get { return server.State; }
        }

        protected bool CanDisconnect
        {
            get { return !deviceManager.HasConnectedDevices && deviceManager.IsCommunicationRegistered && server.IsConnected; }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                enableDisablenChanged(value);
            }
        }

        public bool IsDataSuiteRunning
        {
            get { return dataSuiteInstanceChecker.IsDataSuiteRunning; }
        }

        public int ConnectedDevicesCount
        {
            get { return deviceManager.ConnectedDevices; }
        }

        #endregion
        #region Events
        private void deviceManager_OnDeviceConnected(object sender, ConnectionEventArgs e)
        {
            e.UserId = _UserID;
            deviceConnected(e);
        }

        private void deviceConnected(ConnectionEventArgs e)
        {
            if (server.IsConnected)
            {
                if (!IsEnabled)
                    return;

                server.OnDeviceConnected(e);
                if (timer != null && timer.Enabled)
                    timer.Stop();
            }
            else
            {
                Start();
            }
        }

        private void deviceManager_OnDeviceRemoved(object sender, ConnectionEventArgs e)
        {
            deviceRemoved(e);
        }

        private void deviceRemoved(ConnectionEventArgs e)
        {
            if (server.IsConnected)
            {
                server.OnDeviceRemoved(e);
                if (!deviceManager.HasConnectedDevices && server.IsConnected)
                    startDisconnectionTimer();
            }
        }

        private object locker = new object();
        private void startDisconnectionTimer()
        {
            lock (locker)
            {
                if (timer != null && timer.Enabled)
                    return;

                timer = new System.Timers.Timer(DISCONNECTION_TIMER.TotalMilliseconds);

                timer.Elapsed += (sender2, e2) =>
                {
                    timer.Stop();
                    if (CanDisconnect)
                    {
                        server.Disconnect();
                    }
                    else
                    {
                        Log.Instance.Write("[WCF Client]  Disconnection canceled.", this);
                    }
                };

                if (CanDisconnect)
                {
                    Log.Instance.Write("[WCF Client]  No devices found, disconnecting in {0} seconds.", this, Log.LogSeverity.DEBUG, DISCONNECTION_TIMER.TotalSeconds);
                    timer.Start();
                }
            }
        }

        private void server_OnConnectionStateChanged(CommunicationState state)
        {
            connectionStateChanged(state);
        }

        private void connectionStateChanged(CommunicationState state)
        {
            switch (state)
            {
                case CommunicationState.Closed:
                    handleDisconnection();
                    break;
                case CommunicationState.Opened:
                    handleConnection();
                    break;
                default:
                    break;
            }
        }

        private void handleConnection()
        {
            deviceManager.Shutdown();
            deviceManager.ScanForDevices();

            Task.Factory.StartNew(() =>
            {
                TaskDelay.Delay(1000).Wait();
                startDisconnectionTimer();
            }, TaskCreationOptions.PreferFairness);
        }

        private void handleDisconnection()
        {
            
        }

        #endregion
        #region Methods
        public void Start()
        {
            if (server.IsConnected || server.IsConnecting || IsDataSuiteRunning || !IsEnabled)
                return;

            server.Connect();
        }

        public void Stop()
        {
            try
            {
                if (server.IsConnected)
                    server.Disconnect();

                deviceManager.Shutdown();
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[WCF Client]  On Stop()", this, ex);
            }
        }

        private void dataSuiteInstanceChanged(bool isConnected)
        {
            if (!isConnected)
            {
                Log.Instance.Write("[WCF Client]  DataSuite is closed.", this);
                Start();
            }
            else
            {
                Log.Instance.Write("[WCF Client]  DataSuite is open.", this);
                Stop();
            }
        }

        private void enableDisablenChanged(bool isEnabled)
        {
            if (isEnabled)
            {
                Log.Instance.Write("[WCF Client]  Connection is now enabled.", this);
                Start();
            }
            else
            {
                Log.Instance.Write("[WCF Client]  Connection is now disabled.", this);
                Stop();
            }

            Auxiliary.Tools.Utilities.InvokeEvent(OnEnableChanged, this, isEnabled);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                deviceManager.Dispose();
                server.Dispose();
                dataSuiteInstanceChecker.Dispose();
                timer.Dispose();

                OnEnableChanged = null;
                deviceManager = null;
                client = null;
                server = null;
                dataSuiteInstanceChecker = null;
                timer = null;
            }
        }

        #endregion
    }
}