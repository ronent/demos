﻿using System;
using WcfClient.WcfService;
namespace WCFClient.Service
{
    public interface IServiceManager
    {
        void Close();
        event EventHandler Faulted;
        void Open();
        global::System.ServiceModel.CommunicationState State { get; }
    }
}
