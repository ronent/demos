﻿using System;
using System.ServiceModel;
using WcfClient.WcfService;
using WCFClient.Communications;

namespace WCFClient.Service
{
    public class ServiceManager : WcfClient.WcfService.IServer, IServiceManager, IDisposable
    {
        #region Fields
        protected ServerClient server;
        protected ClientAPI client;

        #endregion
        #region Constructor
        public ServiceManager(string uri, ClientAPI client)
        {
            IsDisposed = false;
            this.client = client;
            URI = uri;
        }

        #endregion
        #region Properties
        protected static string URI { get; set; }
        public bool IsDisposed { get; private set; }

        #endregion
        #region Methods
        protected virtual void initializeServer()
        {
            server = new ServerClient(new InstanceContext(client), instantiateBinding(), instantiateAddress());
        }

        protected static EndpointAddress instantiateAddress()
        {
            var address = new EndpointAddress("net.tcp://" + URI);

            return address;
        }

        protected static NetTcpBinding instantiateBinding()
        {
            var binding = new NetTcpBinding();
            binding.Name = "netTCPEndPoint";
            binding.OpenTimeout = new TimeSpan(0, 0, 30);

            binding.Security.Mode = SecurityMode.None;
            binding.ReliableSession.Enabled = false;
            binding.ReliableSession.Ordered = true;

            return binding;
        }

        #endregion
        #region IServiceManager
        public virtual void Close()
        {
            if (server != null && server.State != CommunicationState.Closed)
            {
                try
                {
                    server.Close();
                }
                catch 
                {
                    if(server!=null)
                        server.Abort();
                }
                finally
                {
                    server = null;
                }
            }
        }

        public virtual event EventHandler Faulted
        {
            add { server.InnerChannel.Faulted += value; }
            remove { server.InnerChannel.Faulted -= value; }
        }

        public virtual void Open()
        {
            if (server == null)
            {
                initializeServer();
            }

            server.Open();
        }


        public virtual CommunicationState State
        {
            get
            {
                if (server != null)
                    return server.State;
                else
                    return CommunicationState.Closed;
            }
        }

        public bool IsOpen { get { return State == CommunicationState.Opened; } }

        #endregion
        #region IServer
        public virtual bool KeepAlive()
        {
            try
            {
                if(server!=null)
                    return server.KeepAlive();
            }
            catch 
            {
                Close();
            }

            return false;
        }

        public virtual void OnBundleReceived(string id, byte[][] data)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnBundleReceived(id, data);
            }
            catch
            {
                Close();
            }
        }

        public virtual void OnDataReceived(string id, byte[] data)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnDataReceived(id, data);
            }
            catch
            {
                Close();
            }
        }

        public virtual void OnDeviceConnected(Infrastructure.Communication.ConnectionEventArgs args)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnDeviceConnected(args);
            }
            catch
            {
                Close();
            }
        }

        public virtual void OnDeviceRemoved(Infrastructure.Communication.ConnectionEventArgs args)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnDeviceRemoved(args);
            }
            catch
            {
                Close();
            }
        }

        public virtual bool OpenSession()
        {
            try
            {
                return server.OpenSession();
            }
            catch 
            {
                Close();
            }

            return false;
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                Close();

                server = null;
                client = null;
            }
        }

        #endregion
    }
}