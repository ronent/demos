﻿using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using WCFClient.Communications;

namespace WCFClient.Utilities
{
    public class KeepAlive : IDisposable
    {
        #region Members
        private ServerAPI parent;

        private Timer timer;
        private static TimeSpan INTERVAL = new TimeSpan(0, 0, 10);
        private bool writeToLog = false;

        #endregion
        #region Constructor
        public KeepAlive(ServerAPI serverAPI)
        {
            IsDisposed = false;
            parent = serverAPI;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        public bool IsRunning { get; private set; }

        #endregion
        #region Methods
        public void Start()
        {
            if (IsRunning)
                return;

            IsRunning = true;

            if (timer == null)
            {
                timer = new Timer(INTERVAL.TotalMilliseconds);
                timer.Elapsed += timer_Elapsed;
            }

            timer.Start();

            if (writeToLog)
                Log.Instance.Write("[WCF Client]  Started pinging", this);
        }

        public void Stop()
        {
            if (!IsRunning)
                return;

            IsRunning = false;

            if (timer != null)
                timer.Stop();

            if (writeToLog)
                Log.Instance.Write("[WCF Client]  Stopped pinging", this);
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            parent.KeepAlive();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                Stop();

                timer = null;
                parent = null;
            }
        }

        #endregion
    }
}
