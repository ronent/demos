﻿using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Base.Sensors.Samples;
using MicroXBase.Devices.Types;
using Server.Infrastructure;

namespace AgentClient.Communication
{
    public delegate void DeviceActivityDelegate(GenericDevice device);

    public delegate void DeviceOnlineDelegate(SampleAddedEventArgs args);

    internal class DeviceApiClient
    {
        private readonly ConnectionHandler _connectionHandler;

        public event DeviceActivityDelegate OnConnected;
        public event DeviceActivityDelegate OnDisConnected;
        public event DeviceActivityDelegate OnDownloadCompleted;
        public event DeviceOnlineDelegate OnOnlineSample;

        public DeviceApiClient(int userId)
        {
            SuperDeviceManager.OnDeviceConnected += SuperDeviceManager_OnDeviceConnected;
            SuperDeviceManager.OnDeviceRemoved += SuperDeviceManager_OnDeviceRemoved;
            SuperDeviceManager.Start();

            _connectionHandler = new ConnectionHandler(userId);
            _connectionHandler.Connect();
        }

        void SuperDeviceManager_OnDeviceRemoved(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            
            Log4Tech.Log.Instance.Write("Device #{0} is removed", this, Log4Tech.Log.LogSeverity.DEBUG, e.Device.Status.SerialNumber);
            //remove events for this device
            unregisterMicroXDevice((MicroXLogger)e.Device);
            var data = Common.ObjectToByteArray(e);
            _connectionHandler.DeviceRemoved(data);
            if (OnDisConnected != null)
                OnDisConnected(e.Device);
        }

        void SuperDeviceManager_OnDeviceConnected(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            Log4Tech.Log.Instance.Write("Device #{0} is connected", this, Log4Tech.Log.LogSeverity.DEBUG, e.Device.Status.SerialNumber);
            //register events for this device
            registerMicroXDevice((MicroXLogger)e.Device);
            //_socketClient.SendMessageToServer(e);
            var data = Common.ObjectToByteArray(e);
            _connectionHandler.DeviceConnected(data);
            if (OnConnected != null)
                OnConnected(e.Device);
        }

        void microxDevice_OnDownloadCompleted(object sender, Base.OpCodes.Helpers.DownloadCompletedArgs args)
        {
            Log4Tech.Log.Instance.Write("Download completed for device #{0} ", this, Log4Tech.Log.LogSeverity.DEBUG, args.SerialNumber);
            var data = Common.ObjectToByteArray(args);
            _connectionHandler.DownloadCompleted(data);
            if (OnDownloadCompleted != null)
                OnDownloadCompleted(args.Logger);
        }

        void microxDevice_OnOnlineSampleAdded(object sender, SampleAddedEventArgs e)
        {
            Log4Tech.Log.Instance.Write("Online Sample for #{0} ({1}) , {2} at {3} [dummy - {4}]", this, Log4Tech.Log.LogSeverity.DEBUG, e.SerialNumber, e.Type, e.Sample.Value, e.Sample.Date, e.Sample.IsDummy);
            var data = Common.ObjectToByteArray(e);
            _connectionHandler.OnlineSample(data);
//            if (OnOnlineSample != null)
//                OnOnlineSample(e);
        }

        void microxDevice_OnDownloadProgressReported(object sender, ProgressReportEventArgs e)
        {
            var data = Common.ObjectToByteArray(e);
            _connectionHandler.DownloadProgress(data);
        }

        void microxDevice_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            Log4Tech.Log.Instance.Write("Status on #{0} is recieved", this, Log4Tech.Log.LogSeverity.DEBUG, e.Device.Status.SerialNumber);
            var data = Common.ObjectToByteArray(e);
            //_connectionHandler.DeviceStatus(data);
        }

        void Functions_OnFirmwareUpdateProgressReported(object sender, ProgressReportEventArgs e)
        {
            var data = Common.ObjectToByteArray(e);
            _connectionHandler.FirmwareProgress(data);
        }

        #region methods
        private void registerMicroXDevice(MicroXLogger microxDevice)
        {
            microxDevice.Functions.OnDownloadProgressReported += microxDevice_OnDownloadProgressReported;
            microxDevice.Functions.OnFirmwareUpdateProgressReported += Functions_OnFirmwareUpdateProgressReported;
            microxDevice.Functions.OnOnlineSampleAdded += microxDevice_OnOnlineSampleAdded;
            microxDevice.Functions.OnStatusReceived += microxDevice_OnStatusReceived;
            microxDevice.Functions.OnDownloadCompleted += microxDevice_OnDownloadCompleted;
        }

        private void unregisterMicroXDevice(MicroXLogger microxDevice)
        {
            microxDevice.Functions.OnDownloadProgressReported -= microxDevice_OnDownloadProgressReported;
            //microxDevice.Functions.OnFirmwareUpdateProgressReported -= microxDevice_OnFirmwareUpdateProgressReported;
            microxDevice.Functions.OnOnlineSampleAdded -= microxDevice_OnOnlineSampleAdded;
            microxDevice.Functions.OnStatusReceived -= microxDevice_OnStatusReceived;
            microxDevice.Functions.OnDownloadCompleted -= microxDevice_OnDownloadCompleted;
        }
        #endregion
    }

}
