﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading;
using System.Threading.Tasks;
using AgentClient.ServiceReference;
using Base.Devices;
using Base.Devices.Management;
using Base.Misc;
using Base.OpCodes;
using Log4Tech;
using Server.Infrastructure;

namespace AgentClient.Communication
{
    public class ConnectionHandler : WcfServer
    {
        #region Members

        protected IServer PipeProxy;
        protected DuplexChannelFactory<IServer> pipeFactory;
        private bool IsDisposed;
        private bool _startRecover;
        private System.Timers.Timer _timerAction;
        private ConcurrentDictionary<int, Result> LastActionResult;

        private const int Tolerance = 5;//in seconds
        private const int ActionTolerance = 10 * 1000; //in miliseconds


        #endregion

        #region Constructor

        public ConnectionHandler(int userId) : base(userId)
        {
            IsDisposed = false;
            _timerAction = new System.Timers.Timer
            {
                Interval = ActionTolerance
            };
            _timerAction.Elapsed += _timerAction_Elapsed;
            _timerAction.Start();

            LastActionResult =  new ConcurrentDictionary<int, Result>();
        }

       
        #endregion

        #region Overrides of WcfServer

        public override void DeviceConnected(byte[] e)
        {
            resetTimer();
            Log.Instance.Write("[WCF Client EVENT] Connected", this, Log.LogSeverity.INFO);
            if(IsServerConnected())
               PipeProxy.DeviceConnected(e);
        }

        public override void DeviceRemoved(byte[] e)
        {
            if(SuperDeviceManager.OnlineDevicesCount>0)
                resetTimer();

            Log.Instance.Write("[WCF Client EVENT] Removed", this, Log.LogSeverity.INFO);
            if (IsServerConnected())
                PipeProxy.DeviceRemoved(e); 
        }

        public override void DownloadCompleted(byte[] e)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client EVENT] Download completed", this, Log.LogSeverity.INFO);
            if (IsServerConnected())
                PipeProxy.DownloadCompleted(e);
        }

        public override void OnlineSample(byte[] e)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client EVENT] Online sample", this, Log.LogSeverity.INFO);
            if (IsServerConnected())
                PipeProxy.OnlineSample(e);
        }

        public override void DownloadProgress(byte[] e)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client EVENT] Download progress", this, Log.LogSeverity.INFO);
            if (IsServerConnected())
                PipeProxy.DownloadProgress(e);
        }

        public override void DeviceStatus(byte[] e)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client EVENT] Status", this, Log.LogSeverity.INFO);
            if (IsServerConnected())
                PipeProxy.Status(e);
        }

        public override void FirmwareProgress(byte[] e)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client EVENT] Firmware progress", this, Log.LogSeverity.INFO);
            if (IsServerConnected())
                PipeProxy.FirmwareProgress(e);
        }

        public override void Ping(string serialNumber)
        {
            resetTimer();
        }

        public override void Status(byte[] bytes)
        {
            resetTimer();
        }

        public override void UnitTest(byte[] e)
        {
            Log.Instance.Write("[WCF Client] Sending to server #{0} of bytes", this, Log.LogSeverity.DEBUG, e.Length);
            if (IsServerConnected())
                PipeProxy.UnitTest(e);
        }


        public override void Stop(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Stop device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.Stop(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void Run(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Run device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.Run(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void Download(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Download from device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.DownloadFromDevice(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void ResetCalibration(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Reset Calibration for device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.ResetCalibration(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void UpdateFirmware(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Update Firmware for device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.UploadFirmware(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void SendCalibration(byte[] bytes)
        {
            resetTimer();

            var deviceAction = (DeviceAction)Common.ByteArrayToObject(bytes);
            if (deviceAction != null)
            {
                Log.Instance.Write("[WCF Client] Send Calibration for device #{0}", this, Log.LogSeverity.INFO,
                    deviceAction.SerialNumber);
                var result = deviceCommunication.SendCalibration(deviceAction);
                sendStatus(deviceAction.SerialNumber.ToString(), result);
            }
        }

        public override void MarktimeStamp(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Mark TimeStamp for device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.MarkTimeStamp(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void RestoreDefaultCalibration(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Restore Default Calibration for device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.RestoreDeviceCalibration(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void SaveDefaultCalibration(string serialNumber)
        {
            resetTimer();

            Log.Instance.Write("[WCF Client] Save Default Calibration for device #{0}", this, Log.LogSeverity.INFO, serialNumber);
            var result = deviceCommunication.SaveDefaultCalibration(serialNumber);
            sendStatus(serialNumber, result);
        }

        public override void Setup(byte[] bytes)
        {
            resetTimer();

            var deviceAction = (DeviceAction)Common.ByteArrayToObject(bytes);
            if (deviceAction == null) return;

            Log.Instance.Write("[WCF Client] Save Default Calibration for device #{0}", this,
                Log.LogSeverity.INFO, deviceAction.SerialNumber);
            var result = deviceCommunication.SetupDevice(deviceAction);
            sendStatus(deviceAction.SerialNumber.ToString(), result);
        }

        #endregion

        #region Methods

        public void Connect()
        {
            createPipeProxy();
        }

        public bool IsConnected { get; private set; }

        public IServer Server
        {
            get { return IsServerConnected() ? PipeProxy : null; }
        }

        public void Dispose()
        {
            try
            {
                if (pipeFactory.State == CommunicationState.Opened)
                    pipeFactory.Close();
            }
            catch
            {
                //ignore
            }
        }

        #endregion

        #region Events

        private void _timerAction_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //only if has connected devices then
            var device = SuperDeviceManager.GetDevices().FirstOrDefault();
            if (device != null)
            {
                var serialNumberToCheck = device.Status.SerialNumber;
                //checks if the server is online
                if (IsServerConnected(serialNumberToCheck))
                    resetTimer();
            }
        }

        #endregion

        #region Functions

        private void resetTimer()
        {
            _timerAction.Stop();
            _timerAction.Start();
        }

        private void sendStatus(string serialNumber, Result result)
        {
            var device = deviceCommunication.GetDevice(serialNumber);
            var args = new Tuple<GenericDevice, int, Result>(device, _userId, result);
            if (!IsServerConnected()) return;

            Log.Instance.Write("[WCF Client EVENT] Send Status for device #{0}", this, Log.LogSeverity.INFO,
                serialNumber);
            PipeProxy.Status(Common.ObjectToByteArray(args));
        }

        protected void recoverConnection(string serialNumber)
        {
            Task.Factory.StartNew(() =>
            {
                var seriNumberToCheck = serialNumber;
                _startRecover = true;
                var waitGrowing = Tolerance;
                while (!IsDisposed)
                {
                    //try to connect to the server by sending ping
                    if (IsServerConnected(seriNumberToCheck))
                    {
                        //if success then
                        //start the hid and this will send all connected devices to the server
                        SuperDeviceManager.ReScanForDevices();
                        //and exit the loop and report back (raise event)
                        break;
                    }
                    //re-create a proxy to the server
                    createPipeProxy();
                    //if fail then continue trying in regression (every 'tolerance' seconds and add to the wait another 'tolerance' seconds)
                    Thread.Sleep(TimeSpan.FromSeconds(waitGrowing));
                    //advance the wait by the tolerance seconds
                    //waitGrowing += Tolerance;
                    waitGrowing = Tolerance;
                }
                _startRecover = false;
            });
        }

        protected bool IsServerConnected(string serialNumber="")
        {
            var serialNumberToCheck = serialNumber;
            try
            {
                if (!string.IsNullOrEmpty(serialNumberToCheck))
                {
                    //get first connected device
                    var device = SuperDeviceManager.GetDevices().FirstOrDefault();
                    if (device != null)
                    {
                        serialNumberToCheck = device.Status.SerialNumber;
                    }
                }
                //try to send ping to the server
                PipeProxy.Ping(serialNumberToCheck);
                IsConnected = true;
                return true;
            }
            catch (CommunicationException)
            {
                IsConnected = false;
                if (!_startRecover)
                    recoverConnection(serialNumberToCheck);
            }
            return false;
        }

        protected void createPipeProxy()
        {
            try
            {
                // init wcf client
                pipeFactory = new DuplexChannelFactory<IServer>(
                    new InstanceContext(this),
                    instantiateBinding(),
                    new EndpointAddress(string.Format("net.tcp://{0}", ConfigurationManager.AppSettings["API"])));

                pipeFactory.Open();
                PipeProxy = pipeFactory.CreateChannel();
            }
            catch (CommunicationException)
            {

            }
            catch (Exception)
            {
                //ignore
            }

            // PipeProxy = new ServerClient(new InstanceContext(this), instantiateBinding(), instantiateAddress());
        }

        protected NetTcpBinding instantiateBinding()
        {
            var binding = new NetTcpBinding
            {
                Name = "netTCPEndPoint",
                OpenTimeout = TimeSpan.FromHours(24f),
                Security =
                    new NetTcpSecurity
                    {
                        Mode = SecurityMode.None,
                        Message =
                            new MessageSecurityOverTcp()
                            {
                                ClientCredentialType = MessageCredentialType.UserName,
                                AlgorithmSuite = SecurityAlgorithmSuite.Default
                            }
                        ,
                        Transport =
                            new TcpTransportSecurity()
                            {
                                ClientCredentialType = TcpClientCredentialType.None,
                                ProtectionLevel = ProtectionLevel.None
                            }
                    }
                ,
                MaxReceivedMessageSize = 2147483647,
                ReceiveTimeout = TimeSpan.FromHours(24f),
                //                ReliableSession = new OptionalReliableSession()
                //                {
                //                    InactivityTimeout = TimeSpan.FromHours(1f),
                //                    Ordered = true,
                //                    Enabled = true
                //                   
                //                },
                CloseTimeout = TimeSpan.FromHours(24f),
                SendTimeout = TimeSpan.FromHours(24f),

            };


            return binding;
        }

        #endregion

       
    }
}
