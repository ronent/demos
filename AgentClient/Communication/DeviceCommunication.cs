﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Devices;
using Base.Devices.Management;
using Base.Misc;
using Base.OpCodes;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Log4Tech;
using MicroLite2E.DataStructures;
using MicroLite2T.DataStructures;
using MicroLite2TH.DataStructures;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogPro2T.DataStructures;
using MicroLogPro2T.Devices;
using MicroLogPro2TH.DataStructures;
using MicroLogPro2TH.Devices;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using MicroXBase.Sensors.Management;
using Newtonsoft.Json;
using PicoLite.DataStructures.V1;
using PicoLite.Devices;

namespace AgentClient.Communication
{
    class DeviceCommunication
    {
        #region device op codes

        public Result SetupDevice(DeviceAction device)
        {
            var result = Result.ERROR;
            device.Setup = JsonConvert.DeserializeObject<dynamic>(device.Setup);

            Stop(device.SerialNumber.ToString());

            var genericDevice = SuperDeviceManager.GetDevice(device.SerialNumber.ToString());

            if (genericDevice is PicoLiteLogger)
            {
                Log.Instance.Write("Send setup to Picolite", "");
                result = sendPicoLiteSetup(genericDevice, device);
            }
            if (genericDevice is MicroLiteLogger)
            {
                Log.Instance.Write("Send setup to Microlite", "");
                result = sendMicroLiteDeviceSetup(genericDevice, device);
            }
            if (genericDevice is MicroLog16BitLogger)
            {
                Log.Instance.Write("Send setup to Microlog", "");
                result = sendMicroLogDeviceSetup(genericDevice, device);
            }
            if (result.IsOK)
                if (device.Setup.LoggerActivation != null && device.Setup.LoggerActivation.ToString() == "1") //run
                    result = Run(device.SerialNumber.ToString());
         
            return result;
        }

        private  Result sendMicroLogDeviceSetup(GenericDevice device, DeviceAction deviceAction)
        {

            try
            {
                if (device is EC850B16Logger)
                {
                    Log.Instance.Write("Send setup to EC850B16Device", "");
                    return sendEC850ESetup(device, deviceAction.Setup);
                }
                if (device is EC800B16Logger)
                {
                    Log.Instance.Write("Send setup to EC800B16Device", "");
                    return sendEC800ESetup(device, deviceAction.Setup);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendMicroLogDeviceSetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private  ExternalSensorUnitEnum getUnit(string Unit)
        {
            try
            {
                ExternalSensorUnitEnum enumParse = (ExternalSensorUnitEnum)Enum.Parse(typeof(ExternalSensorUnitEnum), Unit);

                if (Enum.IsDefined(typeof(ExternalSensorUnitEnum), enumParse))
                    return enumParse;
            }
            catch { }

            return ExternalSensorUnitEnum.Custom;
        }

        private  Result sendEC850ESetup(GenericDevice device, dynamic setup)
        {
            var micrologSetup = new EC850B16SetupConfiguration();
            try
            {
                micrologSetup.MemorySize = ((EC850B16Logger)device).Status.MemorySize;
                micrologSetup.DeepSleepMode = setup.DeepSleepMode == "1" ? true : false;
                micrologSetup = microliteSetupFields(setup, micrologSetup);

                dynamic sensors = (dynamic)setup.Sensors;
                foreach (var sensor in sensors)
                {
                    if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.ExternalNTC ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.PT100 ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.UserDefined ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Voltage0_10V ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Current4_20mA)
                    {
                        if (sensor.IsActive == "1")
                        {
                            micrologSetup.ExternalSensor.Type = (eSensorType)sensor.TypeID;
                            if ((eSensorType)sensor.TypeID == eSensorType.UserDefined)
                            {
                                var Configuration = new MicroLogUDSConfiguration
                                {
                                    BaseType = (eSensorType)sensor.BaseSensorType,
                                    Name = sensor.SensorName.ToString(),
                                    SignificantFigures = Convert.ToByte(sensor.DecimalDigits.ToString()),
                                    Unit = getUnit(sensor.Unit.ToString())
                                };

                                if (Configuration.Unit == ExternalSensorUnitEnum.Custom)
                                    Configuration.CustomUnit = sensor.Unit.ToString();

                                micrologSetup.ExternalSensor.UserDefinedSensor = Configuration;
                            }
                            if (sensor.IsAlarmEnabled.ToString() == "1")
                            {
                                micrologSetup.ExternalSensor.Alarm.Enabled = true;
                                micrologSetup.ExternalSensor.Alarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                                micrologSetup.ExternalSensor.Alarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                            }
                        }
                        else
                        {
                            micrologSetup.ExternalSensor.Type = eSensorType.None;
                        }
                    }
                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DewPoint)
                    {
                        micrologSetup.FixedSensors.DewPointEnabled = sensor.IsActive == "1";
                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            micrologSetup.FixedSensors.DewPointAlarm.Enabled = true;
                            micrologSetup.FixedSensors.DewPointAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            micrologSetup.FixedSensors.DewPointAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Humidity)
                    {
                        micrologSetup.FixedSensors.HumidityEnabled = sensor.IsActive == "1";
                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            micrologSetup.FixedSensors.HumidityAlarm.Enabled = true;
                            micrologSetup.FixedSensors.HumidityAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            micrologSetup.FixedSensors.HumidityAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DigitalTemperature)
                    {
                        micrologSetup.FixedSensors.TemperatureEnabled = true;
                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            micrologSetup.FixedSensors.TemperatureAlarm.Enabled = true;
                            micrologSetup.FixedSensors.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            micrologSetup.FixedSensors.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                }
                if (micrologSetup.BoomerangEnabled)
                {
                    Log.Instance.Write("Boomerang setup for microlog EC850", "");
                    micrologSetup = boomerangSetup(setup, micrologSetup);
                }
                Log.Instance.Write("Finally, Send setup for microlog EC850", "");
                var ec850B16Logger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as EC850B16Logger;
                if (ec850B16Logger != null)
                    return ec850B16Logger.Functions.SendSetup(micrologSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendEC850ESetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private  Result sendEC800ESetup(GenericDevice device, dynamic setup)
        {
            EC800B16SetupConfiguration micrologSetup = new EC800B16SetupConfiguration();
            try
            {
                micrologSetup.MemorySize = ((EC800B16Logger)device).Status.MemorySize;
                micrologSetup.DeepSleepMode = setup.DeepSleepMode == "1";
                micrologSetup = microliteSetupFields(setup, micrologSetup);
                var sensorWasActivated = false;
                dynamic sensors = setup.Sensors;
                foreach (var sensor in sensors)
                {
                    if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.ExternalNTC ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.PT100 ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.UserDefined ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Voltage0_10V ||
                        Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Current4_20mA)
                    {

                        if (sensor.IsActive == "1")
                        {
                            Log.Instance.Write("#{0} external sensor {1} is been activated", "", Log.LogSeverity.DEBUG, setup.SerialNumber, sensor.TypeID);
                            micrologSetup.ExternalSensor.Type = (eSensorType)sensor.TypeID;
                            sensorWasActivated = true;
                            if ((eSensorType)sensor.TypeID == eSensorType.UserDefined)
                            {
                                MicroLogUDSConfiguration Configuration = new MicroLogUDSConfiguration
                                {
                                    BaseType = (eSensorType)sensor.BaseSensorType,
                                    Name = sensor.SensorName.ToString(),
                                    SignificantFigures = Convert.ToByte(sensor.DecimalDigits.ToString()),
                                    Unit = getUnit(sensor.Unit.ToString())
                                };

                                if (Configuration.Unit == ExternalSensorUnitEnum.Custom)
                                    Configuration.CustomUnit = sensor.CustomUnit.ToString();

                                micrologSetup.ExternalSensor.UserDefinedSensor = Configuration;
                            }
                            if (sensor.IsAlarmEnabled.ToString() == "1")
                            {
                                Log.Instance.Write("#{0} external sensor {1} setting an alarm {2} - {3}", "", Log.LogSeverity.DEBUG, setup.SerialNumber, sensor.TypeID, sensor.LowValue, sensor.HighValue);
                                micrologSetup.ExternalSensor.Alarm.Enabled = true;
                                micrologSetup.ExternalSensor.Alarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                                micrologSetup.ExternalSensor.Alarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                            }
                        }
                        else
                        {
                            if (!sensorWasActivated)
                            {
                                Log4Tech.Log.Instance.Write("#{0} external sensor {1} is been de-activated", "", Log.LogSeverity.DEBUG, setup.SerialNumber, sensor.TypeID);
                                micrologSetup.ExternalSensor.Type = eSensorType.None;
                            }
                        }
                    }
                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.InternalNTC)
                    {
                        micrologSetup.TemperatureSensor.TemperatureEnabled = true;
                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            micrologSetup.TemperatureSensor.TemperatureAlarm.Enabled = true;
                            micrologSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            micrologSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                }
                if (micrologSetup.BoomerangEnabled)
                {
                    Log.Instance.Write("Boomerang setup for microlog EC800", "");
                    micrologSetup = boomerangSetup(setup, micrologSetup);
                }
                Log.Instance.Write("Finally, Send setup for microlog EC800", "");
                var ec800B16Logger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as EC800B16Logger;
                if (ec800B16Logger != null)
                    return ec800B16Logger.Functions.SendSetup(micrologSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendEC800ESetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private  Result sendMicroLiteDeviceSetup(GenericDevice genericDevice, DeviceAction deviceAction)
        {
            try
            {
                if (genericDevice is MicroLite2E.Devices.MicroLite2ELogger)
                {
                    Log.Instance.Write("Send setup to MicroLite2EDevice", "");
                    return sendMicroLite2ESetup(genericDevice, deviceAction.Setup);
                }
                if (genericDevice is MicroLite2T.Devices.MicroLite2TLogger)
                {
                    Log.Instance.Write("Send setup to MicroLite2TDevice", "");
                    return sendMicroLite2TSetup(genericDevice, deviceAction.Setup);
                }
                if (genericDevice is MicroLite2TH.Devices.MicroLite2THLogger)
                {
                    Log.Instance.Write("Send setup to MicroLite2THDevice", "");
                    return sendMicroLite2THSetup(genericDevice, deviceAction.Setup);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendMicroLiteDeviceSetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private  Result sendMicroLite2THSetup(GenericDevice device, dynamic setup)
        {
            MicroLite2THSetupConfiguration microliteSetup = new MicroLite2THSetupConfiguration();
            try
            {
                microliteSetup.MemorySize = ((MicroLiteLogger)device).Status.MemorySize;
                microliteSetup = microliteSetupFields(setup, microliteSetup);
                dynamic sensors = setup.Sensors;
                Log.Instance.Write("Iterate sensors of microlite 2TH", "");
                foreach (var sensor in sensors)
                {
                    if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DewPoint)
                    {
                        microliteSetup.FixedSensors.DewPointEnabled = sensor.IsActive == "1";
                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            microliteSetup.FixedSensors.DewPointAlarm.Enabled = true;
                            microliteSetup.FixedSensors.DewPointAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            microliteSetup.FixedSensors.DewPointAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.Humidity)
                    {
                        microliteSetup.FixedSensors.HumidityEnabled = sensor.IsActive == "1";

                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            microliteSetup.FixedSensors.HumidityAlarm.Enabled = true;
                            microliteSetup.FixedSensors.HumidityAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            microliteSetup.FixedSensors.HumidityAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                    else if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.DigitalTemperature)
                    {
                        microliteSetup.FixedSensors.TemperatureEnabled = true;
                        if (sensor.IsAlarmEnabled.ToString() == "1")
                        {
                            microliteSetup.FixedSensors.TemperatureAlarm.Enabled = true;
                            microliteSetup.FixedSensors.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            microliteSetup.FixedSensors.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                }

                if (microliteSetup.BoomerangEnabled)
                {
                    Log4Tech.Log.Instance.Write("Boomerang setup for microlite 2TH", "");
                    microliteSetup = boomerangSetup(setup, microliteSetup);
                }
                Log4Tech.Log.Instance.Write("Finally, Send setup for microlite 2TH", "");
                var microLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as MicroLiteLogger;
                if (
                    microLiteLogger !=
                    null)
                    return microLiteLogger.Functions.SendSetup(microliteSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendMicroLite2THSetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private string normalizeEmail(string email)
        {
            return email.Replace("\r", "").Replace("[", "").Replace("]", "").Replace("\n", "").Replace("\"", "").Trim();
        }

        private  MicroXSetupConfiguration boomerangSetup(dynamic setup, MicroXSetupConfiguration microliteSetup)
        {
            var contactList = new List<BasicContact>();
            try
            {
                //iterate through contact list
                //assume that the emails came in this format: a@a.com,b@b.com
                string[] arr = setup.BoomerangContacts.ToString().Split(';');

                foreach (var t in arr)
                {
                    var email = normalizeEmail(t);
                    Log.Instance.Write("Add {0} to contact list for boomerang", "", Log.LogSeverity.DEBUG, email);
                    contactList.Add(new BasicContact() { EMail = email });
                }

                string generatedBy = setup.BoomerangGeneratedBy.ToString();
                if (generatedBy.Length > 0)
                {
                    if (generatedBy.Contains(@"\"))
                    {
                        var firstPart = generatedBy.Substring(0, generatedBy.IndexOf(@"\"));
                        var secondPart = generatedBy.Substring(generatedBy.LastIndexOf(@"\") + 1);
                        generatedBy = string.Format("{0}/{1}", firstPart, secondPart);
                    }
                }

                microliteSetup.Boomerang.Author = generatedBy;
                microliteSetup.Boomerang.CelsiusMode = setup.BoomerangCelsiusMode.ToString() == "1";
                microliteSetup.Boomerang.Contacts = contactList;
                return microliteSetup;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in boomerangSetup", "", ex);
            }
            return null;
        }

        private  Result sendMicroLite2TSetup(GenericDevice device, dynamic setup)
        {
            MicroLite2TSetupConfiguration microliteSetup = new MicroLite2TSetupConfiguration();
            try
            {
                microliteSetup.MemorySize = ((MicroLiteLogger)device).Status.MemorySize;
                microliteSetup = microliteSetupFields(setup, microliteSetup);

                Log.Instance.Write("Iterate sensors of microlite 2T", "");

                dynamic sensors = setup.Sensors;
                foreach (var sensor in sensors)
                {
                    microliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = sensor.IsAlarmEnabled.ToString() == "1";

                    if (microliteSetup.TemperatureSensor.TemperatureAlarm.Enabled)
                    {
                        microliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                        microliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                    }
                }
                if (microliteSetup.BoomerangEnabled)
                {
                    Log.Instance.Write("Boomerang setup for microlite 2T", "");
                    microliteSetup = boomerangSetup(setup, microliteSetup);
                }
                Log.Instance.Write("Finally, Send setup for microlite 2T", "");
                var microLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as MicroLiteLogger;
                if (
                    microLiteLogger !=
                    null)
                    return microLiteLogger.Functions.SendSetup(microliteSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendMicroLite2TSetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private  MicroLogSetupConfiguration microliteSetupFields(dynamic setup, MicroLogSetupConfiguration microliteSetup)
        {
            try
            {
                Log.Instance.Write("Setup fields for microlite", "");

                microliteSetup.SerialNumber = setup.SerialNumber.ToString();
                microliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1";
                microliteSetup.Comment = setup.Name.ToString();
                microliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1";
                microliteSetup.FahrenheitMode = setup.CelsiusMode.ToString() != "1";
                microliteSetup.Interval = new TimeSpan(0, 0, Convert.ToInt32(setup.SampleRateInSec.ToString()));//Convert.ToUInt16(setup.SampleRate.ToString());
                microliteSetup.AveragePoints = Convert.ToByte(setup.SampleAvgPoints.ToString());
                microliteSetup.EnableLEDOnAlarm = setup.EnableLEDOnAlarm.ToString() == "1";
                microliteSetup.LCDConfiguration = (LCDConfigurationEnum)setup.LEDConfig;
                microliteSetup.UtcOffset = Convert.ToSByte(setup.LoggerUTC);

                if (microliteSetup is MicroLite2ESetupConfiguration)
                {
                    (microliteSetup as MicroLite2ESetupConfiguration).StopOnDisconnect = setup.StopOnCapRemoval.ToString() == "1";
                    microliteSetup = (MicroLite2ESetupConfiguration)microliteSetup;
                }

                if (setup.ShowMinMaxSamplesOnLCD != null && setup.ShowMinMaxSamplesOnLCD.ToString() == "2")
                    microliteSetup.ShowMinMax = true;
                else if (setup.ShowMinMaxSamplesOnLCD != null && setup.ShowMinMaxSamplesOnLCD.ToString() == "1")
                    microliteSetup.ShowPast24HMinMax = true;

                int keyPressActivation = 0;
                if (setup.KeyPressActivation != null)
                    keyPressActivation = Convert.ToInt32(setup.KeyPressActivation.ToString());
                else if (setup.MagnetActivation != null)
                    keyPressActivation = Convert.ToInt32(setup.MagnetActivation.ToString());

                if (keyPressActivation == 1)
                {
                    microliteSetup.StopOnKeyPress = true;
                    microliteSetup.PushToRunMode = true;
                }
                else if (keyPressActivation == 2)
                {
                    microliteSetup.StopOnKeyPress = false;
                    microliteSetup.PushToRunMode = true;
                }
                else if (keyPressActivation == 3)
                {
                    microliteSetup.StopOnKeyPress = true;
                    microliteSetup.PushToRunMode = false;
                }
                else
                {
                    microliteSetup.StopOnKeyPress = false;
                    microliteSetup.PushToRunMode = false;
                }
                //if (setup.TimerRunEnabled.ToString() == "1")
                if (setup.LoggerActivation != null && setup.LoggerActivation == 3)//timer run enabled
                {
                    if (setup.LoggerTimerStart != null)
                    {
                        Log.Instance.Write("#{0} got Timer Start {1}", "", Log.LogSeverity.DEBUG, microliteSetup.SerialNumber, setup.LoggerTimerStart);
                        microliteSetup.TimerStart = getTimerStart(Convert.ToInt32(setup.LoggerTimerStart), Convert.ToInt32(setup.LoggerUTC));
                        microliteSetup.TimerRunEnabled = true;
                        Log.Instance.Write("#{0} set Timer Start {1}", "", Log.LogSeverity.DEBUG, microliteSetup.SerialNumber, microliteSetup.TimerStart);
                    }
                }
                return microliteSetup;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in microliteSetupFields", "", ex);
            }
            return null;
        }

        private  Result sendMicroLite2ESetup(GenericDevice device, dynamic setup)
        {

            try
            {
                var microliteSetup = new MicroLite2ESetupConfiguration
                {
                    MemorySize = ((MicroLiteLogger)device).Status.MemorySize
                };

                microliteSetup = microliteSetupFields(setup, microliteSetup);
                microliteSetup.StopOnDisconnect = setup.StopOnCapRemoval.ToString() == "1" ? true : false;

                dynamic sensors = setup.Sensors;
                Log4Tech.Log.Instance.Write("Iterate sensors of microlite 2E", "");
                foreach (var sensor in sensors)
                {
                    //set the type of the sensor
                    if (sensor.IsActive.ToString() == "1")
                    {
                        Log4Tech.Log.Instance.Write("sensor {0} is enabled (id={1})", "", Log4Tech.Log.LogSeverity.DEBUG, sensor.SensorType, sensor.TypeID);
                        microliteSetup.ExternalSensor.Type = (eSensorType)sensor.TypeID;
                        if (Convert.ToInt16(sensor.TypeID.ToString()) == (int)eSensorType.UserDefined)
                        {
                            var configuration = new MicroLogUDSConfiguration
                            {
                                BaseType = (eSensorType)sensor.BaseSensorID,
                                Name = sensor.SensorName.ToString(),
                                SignificantFigures = Convert.ToByte(sensor.DecimalDigits.ToString()),
                                Unit = getUnit(sensor.MeasurementUnit.ToString()),
                                CustomUnit = sensor.MeasurementUnit.ToString(),
                                Gain = 0,
                                Offset = 0
                            };

                            var referencePoints = new LoggerReference[2];
                            referencePoints[0].Logger = sensor.Calibration.Log1;
                            referencePoints[0].Reference = sensor.Calibration.Ref1;
                            referencePoints[1].Logger = sensor.Calibration.Log2;
                            referencePoints[1].Reference = sensor.Calibration.Ref2;
                            configuration.Set(referencePoints);


                            microliteSetup.ExternalSensor.UserDefinedSensor = configuration;
                        }
                        //enable the external sensor if needed
                        microliteSetup.ExternalSensor.Alarm.Enabled = sensor.IsAlarmEnabled.ToString() == "1";
                        //set the alarm values if the alarm is enabled
                        if (microliteSetup.ExternalSensor.Alarm.Enabled)
                        {
                            Log4Tech.Log.Instance.Write("sensor {0} has alarm , low-{1} high-{2}", "", Log4Tech.Log.LogSeverity.DEBUG, sensor.Type, sensor.AlarmLowValue, sensor.AlarmHighValue);
                            microliteSetup.ExternalSensor.Alarm.Low = Convert.ToDecimal(sensor.LowValue.ToString());
                            microliteSetup.ExternalSensor.Alarm.High = Convert.ToDecimal(sensor.HighValue.ToString());
                        }
                    }
                }
                //if boomerang enabled then set the boomerang fields:
                if (microliteSetup.BoomerangEnabled)
                {
                    Log.Instance.Write("Boomerang setup for microlite 2E", "");
                    microliteSetup = boomerangSetup(setup, microliteSetup);
                }
                Log.Instance.Write("Finally, Send setup for microlite 2E", "");
                var microLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as MicroLogAPI.Devices.MicroLiteLogger;
                if (
                    microLiteLogger !=
                    null)
                    return microLiteLogger.Functions.SendSetup(microliteSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendMicroLite2ESetup", "", ex);
            }
            return new Result(eResult.ERROR);
        }

        private  Result sendPicoLiteSetup(GenericDevice device, DeviceAction deviceSetupAction)
        {
            if (device is PicoLite.Devices.V2.PicoLiteLogger)
                return sendPicoLiteV2Setup(deviceSetupAction.Setup);
            return sendPicoLiteV1Setup(deviceSetupAction.Setup);
        }

        private void setPicoLiteSetupFields(dynamic setup, PicoLiteV1SetupConfiguration picoliteSetup)
        {
            picoliteSetup.SerialNumber = setup.SerialNumber.ToString();
            picoliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1";
            picoliteSetup.Comment = setup.Name.ToString();
            picoliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1";
            picoliteSetup.FahrenheitMode = setup.CelsiusMode.ToString() != "1";
            picoliteSetup.Interval = new TimeSpan(0, 0, Convert.ToUInt16(setup.SampleRateInSec.ToString()));
            picoliteSetup.TimerRunEnabled = (setup.LoggerActivation != null && setup.LoggerActivation == 3);
            picoliteSetup.UtcOffset = Convert.ToSByte(setup.LoggerUTC);
        }

        private Result sendPicoLiteV2Setup(dynamic setup)
        {
            Log.Instance.Write("Prepare PicoLite V2 setup", this);

            var picoliteSetup = new PicoLite.DataStructures.V2.PicoLiteV2SetupConfiguration();

            try
            {
//                picoliteSetup.SerialNumber = setup.SerialNumber.ToString();
//                picoliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1";
//                picoliteSetup.Comment = setup.Name.ToString();
//                picoliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1";
//                picoliteSetup.FahrenheitMode = setup.CelsiusMode.ToString() != "1";
//                picoliteSetup.Interval = new TimeSpan(0, 0, Convert.ToUInt16(setup.SampleRateInSec.ToString()));
//                picoliteSetup.TimerRunEnabled = (setup.LoggerActivation != null && setup.LoggerActivation == 3);
//                picoliteSetup.UtcOffset = Convert.ToSByte(setup.LoggerUTC);

                setPicoLiteSetupFields(setup,picoliteSetup);

                if (picoliteSetup.TimerRunEnabled)
                    picoliteSetup.TimerStart = getTimerStart(Convert.ToInt32(setup.LoggerTimerStart), Convert.ToInt32(setup.LoggerUTC));

                var keyPressActivation = Convert.ToInt32(setup.KeyPressActivation.ToString());

                if (keyPressActivation == 1)
                {
                    picoliteSetup.StopOnKeyPress = true;
                    picoliteSetup.PushToRunMode = true;
                }
                else if (keyPressActivation == 2)
                {
                    picoliteSetup.StopOnKeyPress = false;
                    picoliteSetup.PushToRunMode = true;
                }
                else if (keyPressActivation == 3)
                {
                    picoliteSetup.StopOnKeyPress = true;
                    picoliteSetup.PushToRunMode = false;
                }
                else
                {
                    picoliteSetup.StopOnKeyPress = false;
                    picoliteSetup.PushToRunMode = false;
                }

                if (setup.Sensors[0].IsAlarmEnabled != null)
                {
                    if (setup.Sensors[0].IsAlarmEnabled.ToString() == "1")
                    {
                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = true;
                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(setup.Sensors[0].LowValue.ToString());
                        picoliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(setup.Sensors[0].HighValue.ToString());
                    }
                }
                picoliteSetup.RunDelay = Convert.ToUInt16(Convert.ToInt32(setup.RunDelay) / 60);
                //if boomerang enabled then set the boomerang fields:
                if (picoliteSetup.BoomerangEnabled)
                {
                    var contactList = new List<BasicContact>();

                    string[] arr = setup.BoomerangContacts.ToString().Split(';');

                    foreach (var t in arr)
                    {
                        var email = normalizeEmail(t);
                        Log.Instance.Write("Add {0} to contact list for boomerang", "", Log.LogSeverity.DEBUG, email);
                        contactList.Add(new BasicContact() { EMail = email });
                    }

//                    foreach (var boomerangContact in setup.BoomerangContacts)
//                    {
//                        contactList.Add(new BasicContact { EMail = boomerangContact });
//                    }

                    if (contactList.Count > 0)
                    {
                        picoliteSetup.Boomerang.Author = setup.BoomerangGeneratedBy.ToString();
                        picoliteSetup.Boomerang.CelsiusMode = setup.BoomerangCelsiusMode.ToString() == "1";
                        picoliteSetup.Boomerang.Contacts = contactList;
                    }
                    else
                        picoliteSetup.BoomerangEnabled = false;
                }
                var picoLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as PicoLite.Devices.V2.PicoLiteLogger;
                if (picoLiteLogger !=
                    null)
                    return picoLiteLogger.Functions.SendSetup(picoliteSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendPicoLiteV2Setup", this, ex);
            }
            return new Result(eResult.ERROR);
        }

        private DateTime getTimerStart(int loggerTimerStart, int loggerUTC)
        {
            var d = new DateTime(1970, 1, 1, 0, 0, 0);
            var seconds = loggerTimerStart + (loggerUTC * 60 * 60);
            return d.AddSeconds(seconds);
        }

        private Result sendPicoLiteV1Setup(dynamic setup)
        {
            Log.Instance.Write("Prepare PicoLite V1 setup", this);

            var picoliteSetup = new PicoLiteV1SetupConfiguration();

            try
            {
//                picoliteSetup.SerialNumber = setup.SerialNumber.ToString();
//                picoliteSetup.BoomerangEnabled = setup.BoomerangEnabled.ToString() == "1";
//                picoliteSetup.Comment = setup.Name.ToString();
//                picoliteSetup.CyclicMode = setup.CyclicMode.ToString() == "1";
//                picoliteSetup.FahrenheitMode = setup.CelsiusMode.ToString() != "1";
//                picoliteSetup.Interval = new TimeSpan(0, 0, Convert.ToUInt16(setup.SampleRateInSec.ToString()));
//                picoliteSetup.TimerRunEnabled = (setup.LoggerActivation != null && setup.LoggerActivation == 3);
//                picoliteSetup.UtcOffset = Convert.ToSByte(setup.LoggerUTC);

                setPicoLiteSetupFields(setup, picoliteSetup);

                if (picoliteSetup.TimerRunEnabled)
                    picoliteSetup.TimerStart = getTimerStart(Convert.ToInt32(setup.LoggerTimerStart), Convert.ToInt32(setup.LoggerUTC));

                //push to run needs to be underneath the key press activation
                //in case of v1 of picolite the key press should have only push to run
                picoliteSetup.PushToRunMode = setup.KeyPressActivation.ToString() == "2";

                if (setup.Sensors[0].IsAlarmEnabled != null)
                {
                    if (setup.Sensors[0].IsAlarmEnabled.ToString() == "1")
                    {
                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Enabled = true;
                        picoliteSetup.TemperatureSensor.TemperatureAlarm.Low = Convert.ToDecimal(setup.Sensors[0].LowValue.ToString());
                        picoliteSetup.TemperatureSensor.TemperatureAlarm.High = Convert.ToDecimal(setup.Sensors[0].HighValue.ToString());
                    }
                }

                //if boomerang enabled then set the boomerang fields:
                if (picoliteSetup.BoomerangEnabled)
                {
                    List<BasicContact> contactList = new List<BasicContact>();
                    //iterate through contact list
                    //assume that the emails came in this format: a@a.com;b@b.com
                    string[] arr = setup.BoomerangContacts.ToString().Split(';');

                    foreach (var t in arr)
                    {
                        var email = normalizeEmail(t);
                        Log.Instance.Write("Add {0} to contact list for boomerang", "", Log.LogSeverity.DEBUG, email);
                        contactList.Add(new BasicContact { EMail = email });
                    }

                    if (contactList.Count > 0)
                    {
                        picoliteSetup.Boomerang.Author = setup.BoomerangGeneratedBy.ToString();
                        picoliteSetup.Boomerang.CelsiusMode = setup.BoomerangCelsiusMode.ToString() == "1";
                        picoliteSetup.Boomerang.Contacts = contactList;
                    }
                }
                var picoLiteLogger = SuperDeviceManager.GetDevice(setup.SerialNumber.ToString()) as PicoLite.Devices.V1.PicoLiteLogger;
                if (picoLiteLogger !=
                    null)
                    return picoLiteLogger.Functions.SendSetup(picoliteSetup).Result;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in sendPicoLiteV1Setup", this, ex);
            }
            return new Result(eResult.ERROR);
        }

        public  Result SaveDefaultCalibration(string serialNumber)
        {
            var genericMicroLogLogger = SuperDeviceManager.GetDevice(serialNumber) as GenericMicroLogLogger;
            return genericMicroLogLogger != null ? genericMicroLogLogger.Functions.SaveDefaultCalibration().Result : Result.ERROR;
        }

        public  Result RestoreDeviceCalibration(string serialNumber)
        {
            var genericMicroLogLogger = SuperDeviceManager.GetDevice(serialNumber) as GenericMicroLogLogger;
            return genericMicroLogLogger != null ? genericMicroLogLogger.Functions.RestoreDefaultCalibration().Result : Result.ERROR;
        }

        public  Result MarkTimeStamp(string serialNumber)
        {
            var picoliteLogger = SuperDeviceManager.GetDevice(serialNumber) as PicoLiteLogger;
            return picoliteLogger != null ? picoliteLogger.Functions.MarkTimeStamp().Result : Result.ERROR;
        }

        public  Result SendCalibration(DeviceAction deviceAction)
        {
            var microXLogger = SuperDeviceManager.GetDevice(deviceAction.SerialNumber.ToString()) as GenericLogger;
            if (microXLogger != null)
            {
                var sensorManager = microXLogger.Sensors;
                var sensor = sensorManager[(eSensorIndex)deviceAction.CalibrationSensorIndex];
                //create dynamically the amount of pairs
                var pairList = new List<LoggerReference>();
                //go over the list of pairs 
                //assuming first item is logger and second item is reference
                var enu = deviceAction.CalibrationPairs.GetEnumerator();

                while (enu.MoveNext())
                {
                    if (enu.Current != null)
                        pairList.Add(new LoggerReference()
                        {
                            Logger = enu.Current.Item1,
                            Reference = enu.Current.Item2
                        });
                }
                //create the configuration for the calibration
                var config = new CalibrationConfiguration(sensorManager as MicroXSensorManager);
                //generate the calibration with the pair list 
                var coefficients = sensor.GenerateCalibration(pairList.ToArray());
                //add the calibration to the config before send
                config.Add(sensor.Index, coefficients);

                return microXLogger.Functions.SendCalibration(config).Result;

            }
           return  Result.ERROR;
        }

        public  Result UploadFirmware(string serialNumber)
        {
            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
            return microXLogger != null ? microXLogger.Functions.UploadFirmware().Result : Result.ERROR;
        }

        public  Result ResetCalibration(string serialNumber)
        {
            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
            return microXLogger != null ? microXLogger.Functions.ResetCalibration().Result : Result.ERROR;
        }

        public  Result DownloadFromDevice(string serialNumber)
        {
            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
            return microXLogger != null ? microXLogger.Functions.Download().Result : Result.ERROR;
        }

        public  GenericDevice GetDevice(string serialNumber)
        {
            return SuperDeviceManager.GetDevice(serialNumber);
        }

        public  Result Run(string serialNumber)
        {
            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
            return microXLogger != null ? microXLogger.Functions.Run().Result : Result.ERROR;
        }

        public  Result Stop(string serialNumber)
        {
            var microXLogger = SuperDeviceManager.GetDevice(serialNumber) as MicroXLogger;
            return microXLogger != null ? microXLogger.Functions.Stop().Result : Result.ERROR;
        }

        public GenericDevice GetFirstConnectedDevice()
        {
            return SuperDeviceManager.GetDevices().FirstOrDefault();
        }
        #endregion
    }
}
