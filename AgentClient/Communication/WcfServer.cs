﻿using System;
using AgentClient.ServiceReference;
using Auxiliary.Tools;
using Log4Tech;

namespace AgentClient.Communication
{
    public abstract class WcfServer : IServer, IServerCallback
    {
        #region Members

        internal DeviceCommunication deviceCommunication;
        internal int _userId;

        #endregion
        
        #region UNIT TEST
        /**************************/
        private const int MSG_LENGTH = 6000;
        private int goodMessage = 0;
        private int badMessage = 0;
        /**************************/
        #endregion
       
        #region Constructor

        protected WcfServer(int userId)
        {
            _userId = userId;
            deviceCommunication = new DeviceCommunication();

            #region UNIT TEST

            /******************************/
//            Parallel.For(0, 100,
//                UNIT_TEST
//                );
//            for(int i=0;i<100;i++)
//                UNIT_TEST(i);

            // UNIT_TEST();
            /******************************/

            #endregion
        }
        #endregion

        #region Device Events

        public abstract void DeviceConnected(byte[] e);

        public abstract void DeviceRemoved(byte[] e);

        public abstract void DownloadCompleted(byte[] e);

        public abstract void OnlineSample(byte[] e);

        public abstract void DownloadProgress(byte[] e);

        public abstract void DeviceStatus(byte[] e);

        public abstract void FirmwareProgress(byte[] e);

        public abstract void Ping(string serialNumber);

        public abstract void Status(byte[] bytes);

        #endregion

        #region Device Actions

        public void Pong(){}

        public abstract void Stop(string serialNumber);

        public abstract void Run(string serialNumber);

        public abstract void Download(string serialNumber);

        public abstract void ResetCalibration(string serialNumber);

        public abstract void UpdateFirmware(string serialNumber);
        
        public abstract void SendCalibration(byte[] bytes);

        public abstract void MarktimeStamp(string serialNumber);

        public abstract void RestoreDefaultCalibration(string serialNumber);

        public abstract void SaveDefaultCalibration(string serialNumber);

        public abstract void Setup(byte[] bytes);

        public void Ack()
        {
            Log.Instance.Write("[WCF Client] ACK", this, Log.LogSeverity.INFO);
        }

        #endregion

        #region UNIT TEST
        public void UnitTestCallBack(byte[] bytes)
        {
            Log.Instance.Write("[WCF Client] Got from server #{0} of bytes", this, Log.LogSeverity.DEBUG, bytes.Length);

            if (checkCRC(bytes))
                goodMessage++;
            else
                badMessage++;

            Log.Instance.Write("[WCF Client] Good message: #{0} Bad messages: #{1}", this, Log.LogSeverity.DEBUG, goodMessage, badMessage);

            //UNIT_TEST();
        }

        public abstract void UnitTest(byte[] e);

        #endregion

        #region Functions

        private void UNIT_TEST(int i)
        {
            //             var arr = new byte[MSG_LENGTH + 4];
            //             var random = new Random();
            //            for (var i = 0; i < MSG_LENGTH; i++)
            //            {
            //                var randomNumber = (byte)( random.Next(0, 255));
            //                arr[i] = randomNumber;
            //            }
            //            var crc = BitConverter.GetBytes(CRC32.Compute(arr,0,MSG_LENGTH));
            //            Buffer.BlockCopy(crc,0,arr,MSG_LENGTH,4);
            //            PipeProxy.UnitTest(arr);
            //            ProgressReportEventArgs args = new ProgressReportEventArgs("1234", i);
            //            PipeProxy.DownloadProgress(Common.ObjectToByteArray(args));
        }

        private bool checkCRC(byte[] data)
        {
            int received = BitConverter.ToInt32(data, MSG_LENGTH);
            int computed = (CRC32.Compute(data, 0, MSG_LENGTH));

            return received == computed;
        }
        #endregion
       
    }
}
