﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgentClient.Manage;


namespace AgentClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {

            Log4Tech.Log.Instance.IsWrite2File = true;
            Log4Tech.Log.Instance.IsWrite2Console = true;
            Log4Tech.Log.Instance.IsWrite2Cloud = false;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Manager());


//            var _wcfServer = new WcfServer();
//            while (true)
//            {
//                Thread.Sleep(10);
//            }
        }
    }
}
