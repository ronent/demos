﻿using System;
using System.Drawing;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgentClient.Communication;
using AgentClient.UI;
using Base.Devices.Management;
using Newtonsoft.Json;

namespace AgentClient.Manage
{
    class Manager : ApplicationContext
    {
         //Component declarations
        private NotifyIcon TrayIcon;
        private ContextMenuStrip TrayIconContextMenu;
        private ToolStripMenuItem CloseMenuItem;
        private ToolStripMenuItem LogMenuItem;
        private Logs logs;

        public Manager()
        {
            Application.ApplicationExit += OnApplicationExit;
            
            start();
        }

        private void start()
        {
            try
            {
                var login = new Login();
                var result = login.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //try to send login with credentials provided
                    var response = tryLogin(login.connectionString, login.userName, login.userPassword).Result;
                    if (response.Result == "OK")
                    {
                        var userId = Convert.ToInt32(response.UserID);
                        var apiClient = new DeviceApiClient(userId);
                        apiClient.OnOnlineSample += apiClient_OnOnlineSample;
                        apiClient.OnConnected += apiClient_OnConnected;
                        apiClient.OnDisConnected += apiClient_OnDisConnected;
                        apiClient.OnDownloadCompleted += apiClient_OnDownloadCompleted;

                        InitializeComponent();
                        TrayIcon.Visible = true;
                        TrayIcon.ShowBalloonTip(100, "Login", "You are now logged-in to SiMiO", ToolTipIcon.Info);
                    }
                    else
                    {
                        MessageBox.Show("Login has failed.\r\nPlease check your credentials and try again.", "SiMiO Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        start();
                    }
                }
                else
                    Application.Exit();    
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in starting the agent", this, ex);
                MessageBox.Show("Login failed!", "SiMiO Login", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                start();
            }
        }

        void apiClient_OnDownloadCompleted(Base.Devices.GenericDevice device)
        {
            TrayIcon.ShowBalloonTip(1,"Download Completed",string.Format("Download completed for {0} #{1}", device.DeviceTypeName,device.Status.SerialNumber), ToolTipIcon.Info);
        }

        void apiClient_OnDisConnected(Base.Devices.GenericDevice device)
        {
            TrayIcon.ShowBalloonTip(1, "Device Disconnected", string.Format("Device {0} #{1} hes been disconnected", device.DeviceTypeName, device.Status.SerialNumber), ToolTipIcon.Warning);
        }

        void apiClient_OnConnected(Base.Devices.GenericDevice device)
        {
            TrayIcon.ShowBalloonTip(1, "Device Connected", string.Format("Device {0} #{1} connected", device.DeviceTypeName, device.Status.SerialNumber), ToolTipIcon.Info);
        }

        void apiClient_OnOnlineSample(Base.Sensors.Samples.SampleAddedEventArgs args)
        {
            //TrayIcon.ShowBalloonTip(1, "Online Sample", string.Format("Online Sample for {0} #{1} , {2} {3}, Alarm State:{4} , on {5}", args.Name, args.SerialNumber,args.Sample.Value,args.Unit,args.Sample.AlarmStatus, args.Sample.Date), ToolTipIcon.Info);
        }

        private void InitializeComponent()
        {
            TrayIcon = new NotifyIcon
            {
                BalloonTipIcon = ToolTipIcon.Info,
                BalloonTipText = "",
                BalloonTipTitle = "",
                Text = "Simio Agent",
                Icon = Properties.Resources.fourtec_symbol_3_icons,
                
            };

            //The icon is added to the project resources.
            //Optional - handle doubleclicks on the icon:
            TrayIcon.DoubleClick += TrayIcon_DoubleClick;
            TrayIcon.Click += TrayIcon_Click;

            //Optional - Add a context menu to the TrayIcon:
            TrayIconContextMenu = new ContextMenuStrip();
            CloseMenuItem = new ToolStripMenuItem();
            LogMenuItem = new ToolStripMenuItem();
            TrayIconContextMenu.SuspendLayout();

            // 
            // TrayIconContextMenu
            // 
            TrayIconContextMenu.Items.AddRange(new ToolStripItem[] {
            CloseMenuItem,LogMenuItem});
            TrayIconContextMenu.Name = "TrayIconContextMenu";
            TrayIconContextMenu.Size = new Size(153, 70);
            // 
            // CloseMenuItem
            // 
            CloseMenuItem.Name = "CloseMenuItem";
            CloseMenuItem.Size = new Size(152, 22);
            CloseMenuItem.Text = "Exit the application";
            CloseMenuItem.Click += CloseMenuItem_Click;

            // 
            // LogMenuItem
            // 
            LogMenuItem.Name = "LogMenuItem";
            //LogMenuItem.Size = new Size(152, 22);
            LogMenuItem.Text = "Show logs";
            LogMenuItem.Click += LogMenuItem_Click;

            TrayIconContextMenu.ResumeLayout(false);
            TrayIcon.ContextMenuStrip = TrayIconContextMenu;
        }

        private async Task<dynamic> tryLogin(string server, string user, string pwd)
        {
            dynamic result = new ExpandoObject();
            result.Result = "Error";
            try
            {
                var requestUri = string.Format("{0}/api/", server);
                dynamic body = new ExpandoObject();
                body.RequestID = "";
                body.UserID = 0;
                body.SiteID = 0;
                body.Command = "validate";
                body.Entity = "login";
                body.Data = new ExpandoObject();
                body.UserName = user;
                using (var md5Hash = MD5.Create())
                {
                    var hash = GetMd5Hash(md5Hash, pwd);
                    body.Password = hash;
                }
                var json = JsonConvert.SerializeObject(body);

                var bodyContent = new StringContent(json, Encoding.UTF8, "application/json");

                ServicePointManager
                    .ServerCertificateValidationCallback +=
                    (sender, cert, chain, sslPolicyErrors) => true;


                using (var client = new HttpClient())
                {
                    
                    client.BaseAddress = new Uri(requestUri);
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var request = new HttpRequestMessage(HttpMethod.Post, "Login")
                    {
                        Content = bodyContent
                    };
                    using (var response = await client.SendAsync(request))
                    using (var content = response.Content)
                    {
                        var responseString = await content.ReadAsStringAsync();
                        if (string.IsNullOrEmpty(responseString)) return false;

                        result = JsonConvert.DeserializeObject<dynamic>(responseString);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("Fail in tryLogin", this, ex);
            }
            return result;
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #region Events

        private void LogMenuItem_Click(object sender, EventArgs e)
        {
            if (logs == null)
                logs = new Logs();
            if (logs.IsDisposed)
                logs = new Logs();

            logs.Start();
            logs.Show();

        }

        void TrayIcon_Click(object sender, EventArgs e)
        {
            //show the menu of the app
            TrayIcon.ContextMenuStrip.Show(Cursor.Position.X, Cursor.Position.Y - 20);
        }

        private void OnApplicationExit(object sender, EventArgs e)
        {
            //Cleanup so that the icon will be removed when the application is closed
            if(TrayIcon!=null)
             TrayIcon.Visible = false;

            //send disconnection of devices when exit the application
            SuperDeviceManager.Shutdown();

            Environment.Exit(0);
        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e)
        {
            //Here you can do stuff if the tray icon is doubleclicked
           // TrayIcon.ShowBalloonTip(10000);
        }

        private static void CloseMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to exit? \n(YOU WILL NOT BE ABLE TO IDENTIFY LOGGERS)", "Are you sure?", MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                Application.Exit();
        }

        #endregion
    }
}
