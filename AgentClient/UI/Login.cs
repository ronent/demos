﻿using System;
using System.Configuration;
using System.Windows.Forms;

namespace AgentClient.UI
{
    public partial class Login : Form
    {

        public string connectionString { private set; get; }
        public string userName { private set; get; }
        public string userPassword { private set; get; }

        public Login()
        {
            InitializeComponent();
            init();
        }

        // Content item for the combo box
        private class Item
        {
            public string Name;
            public int Value;
            public Item(string name, int value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        private void init()
        {
//            cmbServer.Items.Add(new Item("SiMiO Cloud", 1));
//            cmbServer.Items.Add(new Item("Localhost", 2));
//            cmbServer.SelectedIndex = 0;
//            txtUserName.Text = "uv2015";
//            txtPassword.Text = "Q2w3e4r5";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            userName = txtUserName.Text;
            userPassword = txtPassword.Text;
            connectionString = ConfigurationManager.AppSettings["SiMiO"];

//            var item = (Item)cmbServer.SelectedItem;
//            switch (item.Value)
//            {
//                case 1://simio cloud
//                    connectionString = "simio.cloudapp.net:80";
//                    break;
//                case 2://localhost
//                    connectionString = "localhost:443";
//                    break;
//            }
        }
       
    }
}
