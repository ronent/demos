﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgentClient.UI
{
    public partial class Logs : Form
    {
        delegate void SetTextCallback(string text);
        delegate void ClearTextCallback();

        private readonly string _logPath = Directory.GetCurrentDirectory() + @"\Logs";

        public Logs()
        {
            InitializeComponent();
        }

        public void Start()
        {
            LoadLogFileContent();
           // ListenForChanges();
        }

        private void LoadLogFileContent()
        {
            ThreadStart action = () =>
            {
                var fs = new FileStream(_logPath + @"\log.txt", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (var reader = new StreamReader(fs))
                {
                    while (!IsDisposed)
                    {
                        var line = reader.ReadLine();

                        if (!string.IsNullOrWhiteSpace(line))

                            SetText(line);

                        Thread.Sleep(10);
                    }
                }
            };
            var thread = new Thread(action) { IsBackground = true };
            thread.Start();
        }

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (lstLogs.InvokeRequired)
            {
                var d = new SetTextCallback(SetText);
                Invoke(d, text);
            }
            else
            {
                lstLogs.Items.Insert(0,text);
            }
        }
    }
}
